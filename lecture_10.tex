\setcounter{chapter}{9}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Tuesday 4th February}

\section{Connectedness in finite examples}

\begin{example} Let $X = \{ a,b \}$ be a set with two elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{ b \}, X \right\}. \] The only way to express $X$ as a disjoint union of subsets which are not empty is: \[ X = \{a\} \sqcup \{b\}. \] However, $\{ a \}$ does not belong to $\curlyo_{X}$. We conclude that $(X,\curlyo_{X})$ is connected. \end{example}

\begin{example} Let $X = \{ a,b,c,d,e \}$ be a set with five elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a\}, \{a,b\},\{c,d\},\{a,c,d\},\{c,d,e\},\{a,b,c,d\},\{a,c,d,e\},X \right\}. \] The following hold.

\begin{itemize}

\item[(1)] We have that $X = \{a,b\} \sqcup \{c,d,e\}$.

\item[(2)] Both $\{a,b\}$ and $\{c,d,e\}$ belong to $\curlyo_{X}$. 

\end{itemize} %
%
We conclude that $(X,\curlyo_{X})$ is not connected. \end{example} 

\section{\texorpdfstring{$(\mathbb{Q},\curlyo_{\mathbb{Q}})$ is not connected}{The rationals are not connected}}

\begin{example} Let $\mathbb{Q}$ denote the rational numbers. Let $\curlyo_{\mathbb{Q}}$ denote the subspace topology on $\mathbb{Q}$ with respect to $(\reals,\streals)$. Let $x \in \reals$ be irrational. For instance, we can take $x$ to be $\sqrt{2}$. The following hold.

\begin{itemize}

\item[(1)] Since $x$ is irrational, we have that \[ \mathbb{Q} = \left( \mathbb{Q} \cap \opint{-\infty,x} \right) \sqcup \left( \mathbb{Q} \cap \opint{x,\infty} \right). \] 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$x$};

\draw[green] (2,0.25) node {$[$} -- (-1,0.25);
\draw[green] (2,0.75) node {$[$} -- (5,0.75);

\end{tikzpicture}

\end{figuretikz} %

\item[(2)] By Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, we have that $\opint{-\infty,x}$ belongs to $\streals$. By definition of $\curlyo_{\mathbb{Q}}$, we deduce that $\mathbb{Q} \cap \opint{-\infty,x}$ belongs to $\curlyo_{\mathbb{Q}}$. 

\item[(3)] By Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, we have that $\opint{x,\infty}$ belongs to $\streals$. By definition of $\curlyo_{\mathbb{Q}}$, we thus have that $\mathbb{Q} \cap \opint{x,\infty}$ belongs to $\curlyo_{\mathbb{Q}}$. 

\end{itemize} %
%
We conclude that $(\mathbb{Q},\curlyo_{\mathbb{Q}})$ is not connected. 

\end{example}

\section{A characterisation of connectedness}

\begin{prpn} \label{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace} Let $(X,\curlyo_{X})$ be a topological space. Let $\left\{0,1\right\}$ be equipped with the discrete topology. A topological space $(X,\curlyo_{X})$ is connected if and only if there does not exist a surjective, continuous map \ar{X,{\{0,1\}.}} \end{prpn} 

\begin{proof} Suppose that there exists a surjective continuous map \ar{X,{\{0,1\}.},f} The following hold.

\begin{itemize}

\item[(1)] Both $\{0\}$ and $\{1\}$ belong to the discrete topology on $\{0,1\}$. Since $f$ is continuous, we thus have that both $f^{-1}\left( \{0\} \right)$ and $f^{-1}\left( \{1\} \right)$ belong to $\curlyo_{X}$.

\item[(2)] Since $f$ is surjective, neither $f^{-1}\left( \{0\} \right)$ nor $f^{-1}\left( \{1\} \right)$ is empty.

\item[(3)] We have that \begin{align*} f^{-1}\left( \{0\} \right) \cup f^{-1}\left( \{1\} \right) &= f^{-1}\left( \{0,1\} \right) \\ &= X. \end{align*} 

\item[(4)] We have that \[ f^{-1}\left( \{0\} \right) \cap f^{-1}\left( \{1\} \right) = \left\{ x \in X \mid \text{$f(x)=0$ and $f(x) = 1$} \right\}. \] Since $f$ is a well-defined map, the set \[ \left\{ x \in X \mid \text{$f(x)=0$ and $f(x) = 1$} \right\} \] is empty. We deduce that \[ f^{-1}\left( \{0\} \right) \cap f^{-1}\left( \{1\} \right) \] is empty. 

\end{itemize} % 
%
By (3) and (4), we have that \[ X = f^{-1}\left( \{0\} \right) \sqcup f^{-1}\left( \{1\} \right). \] We conclude, by (1) and (2), that $(X,\curlyo_{X})$ is not connected.

Conversely, suppose that $(X,\curlyo_{X})$ is not connected. Then there are subsets $X_{0}$ and $X_{1}$ of $X$ with the following properties.

\begin{itemize}

\item[(1)] Neither $X_{0}$ nor $X_{1}$ is empty, and both belong to $\curlyo_{X}$.

\item[(2)] We have that $X = X_{0} \sqcup X_{1}$.

\end{itemize} %
%
Let \ar{X,{\{0,1\}},f} be the map given by \[ \begin{cases} x \mapsto 0 & \text{if $x \in X_{0}$,} \\ x \mapsto 1 & \text{if $x \in X_{1}$.} \end{cases} \] By (2), we have that $f$ is well-defined. Since neither $X_{0}$ nor $X_{1}$ is empty, we have that $f$ is surjective. Moreover we have that $f^{-1}\left( \{0\} \right) = X_{0}$, and that $f^{-1}\left( \{1\} \right) = X_{1}$. Since both $X_{0}$ and $X_{1}$ belong to $\curlyo_{X}$, we deduce that $f$ is continuous. 
\end{proof} 

\begin{rmk} For theoretical purposes, it is often very powerful to have a characterisation of a mathematical concept in terms of maps. We shall see that Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace} is very useful for carrying out proofs involving connected topological spaces. \end{rmk}
 
\section{\texorpdfstring{$(\reals,\curlyo_{\reals})$ is connected}{The reals are connected}}

\begin{prpn} \label{PropositionRealsAreConnected} The topological space $(\reals,\curlyo_{\reals})$ is connected. \end{prpn}

\begin{rmk} This is one of the most important facts in the course! It is a `low-level' result, which relies fundamentally on the completeness of $\reals$. Task \ref{TaskRealsAreConnected} guides you through a proof. 

To put it another way, Proposition \ref{PropositionRealsAreConnected} is the bridge between set theory and topology upon which connectedness rests. After we have proven it, we shall not need again to work in a `low-level' way with $(\reals,\streals)$ in matters concerning connectedness. We shall be able to argue entirely topologically.        
\end{rmk}

\begin{rmk} Nevertheless Proposition \ref{PropositionRealsAreConnected} is intuitively clear. Something would be wrong with our notion of a connected topological space if it did not hold! It is for this very reason that Proposition \ref{PropositionRealsAreConnected} requires a `low-level' proof. We have to think very carefully about how our intuitive understanding that $(\reals,\streals)$ is connected can be captured rigorously within the framework in which we are working. \end{rmk}

\section{Continuous surjections with a connected source}

\begin{prpn} \label{PropositionTargetOfContinuousSurjectionWithConnectedSourceIsConnected} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Suppose that $(X,\curlyo_{X})$ is connected. Suppose that there exists a continuous, surjective map \ar{X,{Y.},f} Then $(Y,\curlyo_{Y})$ is connected. \end{prpn}

\begin{proof} Let $\{ 0,1 \}$ be equipped with the discrete topology. Suppose that \ar{Y,{\{0,1\}},g} is a continuous, surjective map. Since $f$ is continuous, we have by Proposition \ref{PropositionCompositionOfContinuousMapsIsContinuous} that \ar{X,{\{0,1\}},g \circ f} is continuous. Since $f$ is surjective, we moreover have that $g \circ f$ is surjective. By Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace}, this contradicts our hypothesis that $(X,\curlyo_{X})$ is connected.  

We deduce there does not exist a continuous, surjective map \ar{Y,{\{0,1\}.},g} By Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace}, we conclude that $(Y,\curlyo_{Y})$ is connected. 
 
\end{proof} 

\begin{cor} \label{CorollaryConnectednessPreservedByHomeomorphisms} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let \ar{X,Y,f} be a homeomorphism. Suppose that $(X,\curlyo_{X})$ is connected. Then $(Y,\curlyo_{Y})$ is connected. \end{cor}

\begin{proof} Since $f$ is a homeomorphism, $f$ is in particular a continuous bijection. By Task \ref{TaskBijectionIffInjectiveAndSurjective}, a bijection in the sense of Definition \ref{DefinitionBijection} is in particular surjective. By Proposition \ref{PropositionTargetOfContinuousSurjectionWithConnectedSourceIsConnected}, we deduce that $(Y,\curlyo_{Y})$ is connected. \end{proof}

\begin{cor} \label{CorollaryQuotientOfAConnectedTopologicalSpaceIsConnected} Let $(X,\curlyo_{X})$ be a connected topological space. Let $\sim$ be an equivalence relation on $X$. Then $(\quotientset{X},\curlyo_{\quotientset{X}})$ is connected. \end{cor}

\begin{proof} Let \ar{X,\quotientset{X},\pi} denote the quotient map with respect to $\sim$. By Remark \ref{RemarkQuotientMapIsContinuous}, we have that $\pi$ is continuous. Moreover $\pi$ is surjective. By Proposition \ref{PropositionTargetOfContinuousSurjectionWithConnectedSourceIsConnected}, we deduce that $(\quotientset{X},\curlyo_{\quotientset{X}})$ is connected. \end{proof}

\section{Geometric examples of connected topological spaces}

\begin{example} \label{ExampleOpenIntervalIsConnected} Let $\opint{a,b}$ be an open interval. Let $\curlyo_{\opint{a,b}}$ denote the subspace topology on $\opint{a,b}$ with respect to $(\reals,\streals)$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
By Example \ref{ExampleHomeomorphismFromOpenIntervalToReals}, we have that $(\opint{a,b},\curlyo_{\opint{a,b}})$ is homeomorphic to $(\reals,\streals)$. By Corollary \ref{CorollaryConnectednessPreservedByHomeomorphisms}, we deduce that $(\opint{a,b},\curlyo_{\opint{a,b}})$ is connected. \end{example}

\begin{example} \label{ExampleClosedIntervalIsConnected} Let $\clint{a,b}$ be a closed interval, where $a < b$. Let $\curlyo_{\clint{a,b}}$ denote the subspace topology on $\clint{a,b}$ with respect to $(\reals,\streals)$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$[$} -- (3,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
We have that $\closure{\opint{a,b}}{(\reals,\streals)}$ is $\clint{a,b}$. By Example \ref{ExampleOpenIntervalIsConnected} and Corollary \ref{CorollaryClosureOfAConnectedSubsetIsConnected}, we deduce that $(\clint{a,b},\curlyo_{\clint{a,b}})$ is connected.

\end{example}

\begin{rmk} We can go beyond Example \ref{ExampleOpenIntervalIsConnected} and Example \ref{ExampleClosedIntervalIsConnected}. Let $X$ be a subset of $\reals$, and let $\curlyo_{X}$ be equipped with the subspace topology with respect to $(\reals,\streals)$. Then $(X,\curlyo_{X})$ is connected if and only if $X$ is an interval. To prove this is the topic of Task \ref{TaskSubsetOfRealsIsConnectedIffItIsAnInterval}. \end{rmk}

\begin{example} As in Example \ref{ExampleCircleAsAQuotientSpace}, let $\sim$ be the equivalence relation on $I$ generated by $0 \sim 1$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (2,0);

\fill[red] (0,0) circle[radius=0.05]
           (2,0) circle[radius=0.05];

\node at (0,-0.5) {$0$};
\node at (2,-0.5) {$1$};

\end{tikzpicture}

\end{figuretikz} %
%
By Example \ref{ExampleClosedIntervalIsConnected}, we have that $(I,\curlyo_{I})$ is connected. By Corollary \ref{CorollaryQuotientOfAConnectedTopologicalSpaceIsConnected}, we deduce that $(\quotientset{I},\curlyo_{\quotientset{I}})$ is connected.  

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) circle[radius=1.5];

\fill[red] (0,1.5) circle[radius=0.05] node[anchor=south,black] {$[0]=[1]$};

\end{tikzpicture}

\end{figuretikz} %
%
By Task \ref{TaskCircleHomeomorphicToQuotientOfUnitIntervalWhichIdentifiesEndpoints}, there is a homeomorphism \ar{\quotientset{I},{S^{1}.}} By Corollary \ref{CorollaryConnectednessPreservedByHomeomorphisms}, we deduce that $(S^{1},\curlyo_{S^{1}})$ is connected. \end{example}

\section{Products of connected topological spaces}

\begin{prpn} \label{PropositionProductOfConnectedTopologicalSpacesIsConnected} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be connected topological spaces. Then $(X \times Y, \curlyo_{X \times Y})$ is connected. \end{prpn}

\begin{proof} Let $\{0,1\}$ be equipped with the discrete topology. Let \ar{X \times Y,{\{0,1\}},f} be a continuous map. Our argument has two principal steps.

\begin{itemize}

\item[(1)] Suppose that $x$ belongs to $X$. By Task \ref{TaskConstantMapsAreContinuous}, we have that the map \ar{Y,X,c_{x}} given by $y \mapsto x_{0}$ for all $y$ which belong to $Y$ is continuous. By Task \ref{TaskIdentityMapIsContinuous}, we also have that the map \ar{Y,Y,id} is continuous. By Task \ref{TaskProductOfContinuousMapsIsContinuous}, we deduce that the map \ar[5]{Y,X \times Y,c_{x} \times id} given by $y \mapsto (x,y)$ for all $y$ which belong to $Y$ is continuous. %
%
By Proposition \ref{PropositionCompositionOfContinuousMapsIsContinuous}, we deduce that the map \ar[7]{Y,{\{0,1\}},{f \circ (c_{x} \times id)}} given by $y \mapsto f(x,y)$ for all $y$ which belong to $Y$ is continuous. %
%
Since $(Y,\curlyo_{Y})$ is connected, we deduce, by Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace}, that $f \circ (c_{x} \times id)$ is not surjective. Since $\left\{ 0,1 \right\}$ has only two elements, we deduce that $f \circ (c_{x} \times id)$ is constant. In other words, we have that \[ f(x,y_{0}) = f(x,y_{1}) \] for all $y_{0}$ and $y_{1}$ which belong to $Y$.


\item[(2)] Suppose that $y$ belongs to $Y$. Let \ar{X,Y,c_{y}} denote the map given by $x \mapsto y$ for all $x$ which belong to $X$. Arguing as in (1), we have that the map \ar[7]{X,{\left\{0,1\right\}},{f \circ (id \times c_{y})}} given by $x \mapsto f(x,y)$ for all $x$ which belong to $X$ is continuous. To carry out this argument is the topic of Task \ref{TaskSecondStepInProofThatProductOfConnectedSpacesIsConnected}. %
%
Since $(X,\curlyo_{X})$ is connected, we deduce, by Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace}, that $f \circ (id \times c_{y})$ is not surjective. Since $\{0,1\}$ has only two elements, we deduce that $f \circ (id \times c_{y})$ is constant. In other words, we have that \[ f(x_{0},y) = f(x_{1},y) \] for all $x_{0}$ and $x_{1}$ which belong to $X$.

\end{itemize} %
%
Suppose now that $x_{0}$ and $x_{1}$ belong to $X$, and that $y_{0}$ and $y_{1}$ belong to $Y$. By (1), taking $x$ to be $x_{0}$, we have that \[ f(x_{0},y_{0}) = f(x_{0},y_{1}). \] By (2), taking $y$ to be $y_{1}$, we have that \[ f(x_{0},y_{1}) = f(x_{1},y_{1}). \] We deduce that \[ f(x_{0},y_{0}) = f(x_{1},y_{1}). \] Thus $f$ is constant. In particular, $f$ is not surjective. We have thus demonstrated that there does not exist a continuous surjection \ar{X \times Y,{\{0,1\}.}} By Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace}, we conclude that $(X \times Y,\curlyo_{X \times Y})$ is connected.  

\end{proof}

\begin{rmk} Suppose that $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ are topological spaces. The converse to Proposition \ref{PropositionProductOfConnectedTopologicalSpacesIsConnected} holds: if $(X \times Y, \curlyo_{X \times Y})$ is connected, then both $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ are connected. To prove this is the topic of Task \ref{TaskIfProductIsConnectedThenComponentsAreConnected}. \end{rmk}

\section{Further geometric examples of connected topological spaces}

\begin{example} By Proposition \ref{PropositionRealsAreConnected}, we have that $(\reals,\curlyo_{\reals})$ is connected. Applying Proposition \ref{PropositionProductOfConnectedTopologicalSpacesIsConnected} repeatedly, we deduce that $(\reals^{n},\curlyo_{\reals^{n}})$ is connected, for any $n \in \mathbb{N}$. \end{example} 

\begin{example} \label{ExampleUnitSquareIsConnected} By Example \ref{ExampleClosedIntervalIsConnected}, we have that $(I,\curlyo_{I})$ is connected. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (2,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
By Proposition \ref{PropositionProductOfConnectedTopologicalSpacesIsConnected}, we deduce that $(I^{2},\curlyo_{I^{2}})$ is connected.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\filldraw[fill=green, draw=black] (0,0) rectangle (2,2);

\end{tikzpicture}

\end{figuretikz} %

\end{example}

\begin{example} By Example \ref{ExampleUnitSquareIsConnected}, we have that $(I^{2},\curlyo_{I^{2}})$ is connected. By Corollary \ref{CorollaryQuotientOfAConnectedTopologicalSpaceIsConnected}, we deduce that $(T^{2},\curlyo_{T^{2}})$ is connected.

 \begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (-1.5,0.15) rectangle (1.5,-0.60);

\draw (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (0,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}

\end{tikzpicture}

\end{figuretikz} %

\end{example}

\begin{rmk} By a similar argument, $(M^{2},\curlyo_{M^{2}})$ and $(K^{2},\curlyo_{K^{2}})$ are connected. To check that you understand how we have built up to being able to prove this is the topic of Task \ref{TaskKleinBottleIsConnected}. \end{rmk}

\begin{example} \label{ExampleUnitDiscIsConnected} By Example \ref{ExampleUnitSquareIsConnected}, we have that $(I^{2},\curlyo_{I^{2}})$ is connected. By Task \ref{TaskUnitDiscHomeomorphicToUnitSquare}, there is a homeomorphism \ar{I^{2},{D^{2}.}} By Corollary \ref{CorollaryConnectednessPreservedByHomeomorphisms}, we deduce that $(D^{2},\curlyo_{D^{2}})$ is connected.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\filldraw[fill=green, draw=black] (0,0) circle[radius=1];

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} By Example \ref{ExampleUnitDiscIsConnected}, we have that $(D^{2},\curlyo_{D^{2}})$ is connected. By Corollary \ref{CorollaryQuotientOfAConnectedTopologicalSpaceIsConnected}, we deduce that $(S^{2},\curlyo_{S^{2}})$ is connected.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\begin{scope}
\clip (-1,0) rectangle (1,1);
\draw[dashed] (0,0) ellipse[x radius=1,y radius=0.25];
\end{scope}

\begin{scope}
\clip (-2,0) rectangle (2,-1);
\draw (0,0) ellipse[x radius=1,y radius=0.25];
\end{scope}

\end{tikzpicture}

\end{figuretikz} 

\end{example}
