\part{Point-set foundations} 

\chapter{Monday 6th January}

\section{Definition of a topological space}

\begin{defn} \label{DefinitionTopologicalSpace} Let $X$ be a set, and let $\curlyo$ be a set of subsets of $X$. Then $(X,\curlyo)$ is a {\em topological space} if the following hold.

\begin{itemize} 

\item[(1)] The empty set $\emptyset$ belongs to $\curlyo$. 

\item[(2)] The set $X$ belongs to $\curlyo$.

\item[(3)] Let $U$ be a union of (possibly infinitely many) subsets of $X$ which belong to $\curlyo$. Then $U$ belongs to $\curlyo$.

\item[(4)] Let $U$ and $U'$ be subsets of $X$ which belong to $\curlyo$. Then $U \cap U'$ belongs to $\curlyo$. 

\end{itemize}

\end{defn}

\begin{rmk} By induction, the following holds if and only if (4) holds.

\begin{itemize}

\item[(4')] Let $J$ be a finite set, and let $\{U_{j}\}_{j \in J}$ be a set of subsets of $X$ such that $U_{j}$ belongs to $\curlyo$ for all $j \in J$. Then $\bigcap_{j} U_{j}$ belongs to $\curlyo$.

\end{itemize}

\end{rmk}

\begin{terminology} Let $(X,\curlyo)$ be a topological space. We refer to $\curlyo$ as a {\em topology} on $X$. \end{terminology}

\begin{caution} A set may be able to be equipped with many different topologies! See \S\ref{SectionFiniteExamplesOfTopologicalSpaces}. \end{caution}

\section{Open and closed subsets}

\begin{notn} Let $X$ be a set. By $A \subset X$ we shall mean that $A$ is a subset of $X$, allowing that $A$ may be equal to $X$. In the past, you may instead have written $A \subseteq X$.
\end{notn}

\begin{terminology} Let $(X,\curlyo)$ be a topological space. 

\begin{itemize}

\item[(1)] Let $U$ be a subset of $X$. Then $U$ is {\em open} with respect to $\curlyo$ if $U$ belongs to $\curlyo$.

\item[(2)] Let $V$ be a subset of $X$. Then $V$ is {\em closed} with respect to $\curlyo$ if $X \setminus V$ is an open subset of $X$ with respect to $\curlyo$.

\end{itemize}
\end{terminology}

\section{Discrete and indiscrete topologies}
 
\begin{example} \label{ExamplesTopologies}  We can equip any set $X$ with the following two topologies.

\begin{itemize}

\item[(1)] The {\em discrete topology}, consisting of all subsets of $X$. In other words, the power set of $X$. 

\item[(2)] The {\em indiscrete topology}, given by $\{ \emptyset, X \}$. 

\end{itemize}

\end{example}

\begin{rmk} By (1) and (2) of Definition \ref{DefinitionTopologicalSpace}, every topology on a set $X$ must contain both $\emptyset$ and $X$. Thus the indiscrete topology is the smallest topology with which $X$ may be equipped. \end{rmk}

\section{Finite examples of topological spaces} \label{SectionFiniteExamplesOfTopologicalSpaces} 

\begin{example} \label{ExampleTopologyOnSetWithOneElement} Let $X = \{ a \}$ be a set with one element. Then $X$ can be equipped with exactly one topology, given by $\{ \emptyset, X \}$. In particular, the discrete topology on $X$ is the same as the indiscrete topology on $X$. \end{example}

\begin{rmk} The topological space of Example \ref{ExampleTopologyOnSetWithOneElement} is important! It is known as the {\em point}. \end{rmk}

\begin{example} \label{ExampleTopologiesOnSetWithTwoElements} Let $X = \{ a, b \}$ be a set with two elements. We can define exactly four topologies upon $X$.

\begin{itemize}  

\item[(1)] The discrete topology, given by $\big\{ \emptyset, \{a\}, \{b\}, X \big\}$.

\item[(2)] The topology given by $\big\{ \emptyset, \{a\}, X \big\}$.

\item[(3)] The topology given by $\big\{ \emptyset, \{ b \}, X \big\}$.

\item[(4)] The indiscrete topology, given by $\big\{ \emptyset, X \big\}$. 
 
\end{itemize} 

\end{example}

\begin{rmk} Up to the bijection \ar{X,X,f} given by $a \mapsto b$ and $b \mapsto a$, or in other words up to relabelling the elements of $X$, the topologies of (2) and (3) are the same. \end{rmk}

\begin{terminology} The topological space $(X,\curlyo)$, where $\curlyo$ is the topology of (2) or (3), is known as the {\em Sierpi{\'n}ski interval}, or {\em Sierpi{\'n}ski space}. \end{terminology}

\begin{rmk} In fact (1) -- (4) is a list of every possible set of subsets of $X$ which contains $\emptyset$ and $X$. In other words, every set of subsets of $X$ which contains $\emptyset$ and $X$ defines a topology on $X$. \end{rmk} 

\begin{example} \label{ExampleTopologiesOnSetWithThreeElements} Let $X = \{ a,b,c \}$ be a set with three elements. We can equip $X$ with exactly twenty nine topologies! Up to relabelling, there are exactly nine.  

\begin{itemize}

\item[(1)] The set \[ \left\{ \emptyset, \{b\}, \{a,b\}, \{b,c\}, X \right\} \] defines a topology on $X$.

\item[(2)] The set $\curlyo_{X}$ given by \[ \left\{ \emptyset, \{a\}, \{c\}, X \right\} \] does not define a topology on $X$. This is because \[ \{ a \} \cup \{ c \} = \{ a,c \} \] does not belong to $\curlyo_{X}$, so (3) of Definition \ref{DefinitionTopologicalSpace} is not satisfied. 

\item[(3)] The set $\curlyo_{X}$ given by \[ \left\{ \emptyset, \{a,b\}, \{a,c\}, X \right\} \] does not define a topology on $X$. This is because \[ \{ a,b \} \cap \{ a,c \} = \{ a \} \] does not belong to $\curlyo_{X}$, so (4) of Definition \ref{DefinitionTopologicalSpace} is not satisfied.  

\end{itemize}

\end{example}

\begin{rmk} There are quite a few more `non-topologies' on $X$. \end{rmk}

\section{Open, closed, and half open intervals} 

\begin{notn} Let $\reals$ denote the set of real numbers. \end{notn}

\begin{notn} \label{NotationOpenInterval} Let $a,b \in \reals$.

\begin{itemize} 

\item[(1)] We denote by $\opint{a,b}$ the set \[ \{ x \in \reals \mid a < x < b \}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz}

\item[(2)] We denote by $\opint{a, \infty}$ the set \[ \{ x \in \reals \mid x > a \}. \] 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25);

\end{tikzpicture}

\end{figuretikz}

\item[(3)] We denote by $\opint{-\infty,b}$ the set \[ \{ x \in \reals \mid x < b \}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (-1,0.25) -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz}

\item[(4)] We sometimes denote $\reals$ by $\opint{-\infty,\infty}$. 

\end{itemize}

\end{notn}

\begin{terminology} We shall refer to any of (1) -- (4) in Notation \ref{NotationOpenInterval} as an {\em open interval}. \end{terminology}

\begin{rmk} We shall never use the notation $(a,b)$, $(a,\infty)$, $(-\infty,b)$, or $(-\infty,\infty)$ for an open interval. In particular, for us $(a,b)$ will always mean an ordered pair of real numbers $a$ and $b$. \end{rmk}

\begin{notn} Let $a,b \in \reals$. We denote by $\clint{a,b}$ the set \[ \{ x \in \reals \mid a \leq x \leq b \}. \] 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (0,0.25) node {$[$} -- (2,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz}

\end{notn}

\begin{terminology} We shall refer to $\clint{a,b}$ as a {\em closed interval}. \end{terminology}

\begin{notn} \label{NotationHalfOpenInterval} Let $a,b \in \reals$.

\begin{itemize} 

\item[(1)] We denote by $\lhopint{a,b}$ the set \[ \{ x \in \reals \mid a \leq x < b \}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (0,0.25) node {$[$} -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz}

\item[(2)] We denote by $\rhopint{a,b}$ the set \[ \{ x \in \reals \mid a < x \leq  b \}. \] 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (2,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz}

\item[(3)] We denote by $\lhopint{a,\infty}$ the set \[ \{ x \in \reals \mid x \geq a \}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw[green] (0,0.25) node {$[$} -- (3,0.25);

\end{tikzpicture}

\end{figuretikz}

\item[(4)] We denote by $\rhopint{-\infty,b}$ the set \[ \{ x \in \reals \mid x \leq b \}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (-1,0.25) -- (2,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz}

\end{itemize}

\end{notn}

\begin{terminology} We shall refer to any of (1) -- (4) of Notation \ref{NotationHalfOpenInterval} as a {\em half open interval}. \end{terminology}

\begin{terminology} By an {\em interval} we shall mean a subset of $\reals$ which is either an open interval, a closed interval, or a half open interval. \end{terminology}

\section{\texorpdfstring{Standard topology on $\reals$}{Standard topology on the real numbers}} 

\begin{defn} \label{DefinitionStandardTopologyReals} Let $\streals$ denote the set of subsets $U$ of $\reals$ with the property that, for every $x \in U$, there is an open interval $I$ such that $x \in I$ and $I \subset U$. \end{defn}

\begin{observation} \label{ObservationRealsAndEmptySetBelongToStandardTopologyOnReals} We have that $\reals$ belongs to $\streals$. Moreover $\emptyset$ belongs to $\streals$, since the required property vacuously holds. \end{observation}

\begin{example} \label{ExampleOpenIntervalIsOpenInStandardTopology} Let $U$ be an open interval $\opint{a,b}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $U$ belongs to $\streals$. For every $x \in U$, we can take the corresponding open interval $I$ such that $x \in I$ and $I \subset U$ to be $U$ itself. 

\begin{caution} There are infinitely many other possibilities for $I$. For instance, suppose that $U$ is the open interval $\opint{-1,2}$. Let $x=1$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$-1$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$2$};

\draw (1.5,0) -- (1.5,-0.15);
\node at (1.5,-0.5) {$1$};

\draw[green] (0,0.25) node {$]$} -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} % 
%
We can take $I$ to be $\opint{-1,2}$, but also for example $\opint{0,\sqrt{2}}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (7,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$-1$};

\draw (6,0) -- (6,-0.15);
\node at (6,-0.5) {$2$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$1$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$0$};

\draw (1.414*2 + 2,0) -- (1.414*2 + 2,-0.15);
\node at (1.414*2 + 2,-0.5) {$\sqrt{2}$};

\draw[green] (0,0.25) node {$]$} -- (6,0.25) node {$[$};

\draw[purple] (2,0.75) node {$]$} -- (1.414*2 +2,0.75) node {$[$};

\node[green] at (-0.25,0.5) {$U$};

\node[purple] at (1.75,1) {$I$};
\end{tikzpicture}

\end{figuretikz} 

\end{caution}

\end{example}
