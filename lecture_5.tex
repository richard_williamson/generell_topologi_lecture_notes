\setcounter{chapter}{4}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 20th January}

\section{Geometric examples of continuous maps}

\begin{rmk} Most of our continuous maps between geometric examples of topological spaces will be constructed from polynomial maps \ar{\reals,\reals} in `canonical' ways: by restrictions, products, and quotients. Don't worry about this for now. We shall take it for granted, leaving details for the exercises, and instead focus on developing a geometric feeling for continuity. \end{rmk}   

\begin{example} \label{ExampleShrinkingDiscToAPoint} Let \ar{D^{2} \times I,D^{2},f} be given by $(x,y,t) \mapsto \left( (1-t)x, (1-t)y \right)$. Then $f$ is continuous. To prove this is Task \ref{TaskShrinkingDiscToAPointIsContinuous}. \end{example}

\begin{rmk} We may think of $f$ as `shrinking $D^{2}$ onto its centre', as $t$ moves from $0$ to $1$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];
\fill (0,0) circle[radius=0.05];

\foreach \x in {0,1,...,11} 
  \draw[->,green,shorten >=0.75em] ({360-(\x*360/12))}:1.5) -- (0,0);

\end{tikzpicture}

\end{figuretikz} %
%
We can picture the image of $D^{2} \times \{t\}$ under $f$ as follows, as $t$ moves from $0$ to $1$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (12,0);

\foreach \x/\xtext in {0,1,2,3}
  \fill[green] (\x*12/4,1.4) circle[radius=(4-\x)/4+0.05];

\foreach \x/\xtext in {0,1,2,3}
  \draw (\x*12/4,0) -- (\x*12/4,-0.2)
        (\x*12/4,1.4) circle[radius=(4-\x)/4+0.05];

\draw (12,0) -- (12,-0.2);
\fill (12,1.4) circle[radius=0.05]; 

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*12/4,-0.5) node[anchor=mid] {$\xtext$};

\end{tikzpicture}

\end{figuretikz} 

\end{rmk}

\begin{example} \label{ExampleTravellingAroundCircle} Let $k \in \reals$. There is a continuous map \ar{I,S^{1},f} which can be thought of as travelling $k$ times around a circle, starting at $(0,1)$. To construct $f$ rigorously is the topic of Task \ref{TaskTravellingAroundCircle}. \end{example}

\begin{rmk} Let us picture $f$ for a few values of $k$.

\begin{itemize}

\item[(1)] Let $k=1$. Then we travel exactly once around $S^{1}$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\draw[green,->] (0,1.1) arc (90:-265:1.1);

\end{tikzpicture}

\end{figuretikz}

\begin{caution} Don't be misled by the picture. The path really travels around the circle, not slightly outside it. \end{caution}

We may picture $f\left( \clint{0,t} \right)$ as $t$ moves from $0$ to $1$ as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (10,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*10/4,0) -- (\x*10/4,-0.2)
             (\x*10/4,1.75) circle[radius=0.8];

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*10/4,-0.5) node[anchor=mid] {$\xtext$};

\fill[green,->] (0,2.65) circle[radius=0.025];
\draw[green,->] (10/4,2.65) arc (90:0:0.9);
\draw[green,->] (5,2.65) arc (90:-90:0.9);
\draw[green,->] (30/4,2.65) arc (90:-180:0.9);
\draw[green,->] (10,2.65) arc (90:-265:0.9);

\end{tikzpicture}

\end{figuretikz} %
% 
Recall from Examples \ref{ExampleOpenArcBelongsToTopologyOnCircle} that a typical open subset $U$ of $S^{1}$ is an `open arc'.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green] (60:1.45) node[rotate=-30] {$($} arc (60:-25:1.45) node [rotate=-115] {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
We have that $f^{-1}(U)$ is an open interval as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (5,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*5/4,0) -- (\x*5/4,-0.2);

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*5/4,-0.5) node[anchor=mid] {$\xtext$};

\draw[green] (0.125*5,0.25) node {$($} -- (0.33*5,0.25) node {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
In particular, $f^{-1}(U)$ belongs to $\curlyo_{I}$. Thus, even though we have not yet rigorously constructed $f$, we can intuitively believe that it is continuous.

\item[(2)] Let $k=2$. Then we travel exactly twice around $S^{1}$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\draw[green,->]
  (0,1.1)
  \foreach \i [evaluate={\r=1.1+0.125*((90+\i)/360)}] in {-90,-80,...,630} {-- (-\i:\r)}; 

\end{tikzpicture}

\end{figuretikz} %
%
\begin{caution} Again, don't be misled by the picture. The path really travels twice around the circle, thus passing through every point on the circle twice, not in a spiral outside the circle. \end{caution}

We may picture $f\left( \clint{0,t} \right)$ as $t$ moves from $0$ to $1$ as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (10,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*10/4,0) -- (\x*10/4,-0.2)
             (\x*10/4,1.75) circle[radius=0.8];

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*10/4,-0.5) node[anchor=mid] {$\xtext$};

\fill[green,->] (0,2.65) circle[radius=0.025];
\draw[green,->] (10/4,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,90} {-- ($(10/4,1.75) + (-\i:\r)$)}; 
\draw[green,->] (10/2,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,270} {-- ($(10/2,1.75) + (-\i:\r)$)}; 
\draw[green,->] (30/4,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,450} {-- ($(30/4,1.75) + (-\i:\r)$)};
 \draw[green,->] (10,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,630} {-- ($(10,1.75) + (-\i:\r)$)}; 
\end{tikzpicture}

\end{figuretikz} % 
%
Let $U$ denote the subset of $S^{1}$ given by the `open arc' depicted below.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green] (60:1.45) node[rotate=-30] {$($} arc (60:-25:1.45) node [rotate=-115] {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $f^{-1}(U)$ is a disjoint union of open intervals as follows. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (5,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*5/4,0) -- (\x*5/4,-0.2);

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*5/4,-0.5) node[anchor=mid] {$\xtext$};

\draw[green] (0.125*2.5,0.25) node {$($} -- (0.33*2.5,0.25) node {$)$}
           (2.5+0.125*2.5,0.25) node {$($} -- (2.5+0.33*2.5,0.25) node {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
In particular, $f^{-1}(U)$ belongs to $\curlyo_{I}$. Thus, again, even though we have not rigorously constructed $f$, we can believe intuitively that it is continuous.

\item[(3] Let $k=\frac{3}{2}$. Then we travel exactly one and a half times around $S^{1}$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\draw[green,->]
  (0,1.1)
  \foreach \i [evaluate={\r=1.1+0.125*((90+\i)/360)}] in {-90,-80,...,450} {-- (-\i:\r)}; 

\end{tikzpicture}

\end{figuretikz} 

We may picture $f\left( \clint{0,t} \right)$ as $t$ moves from $0$ to $1$ as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (10,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*10/4,0) -- (\x*10/4,-0.2)
             (\x*10/4,1.75) circle[radius=0.8];

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*10/4,-0.5) node[anchor=mid] {$\xtext$};

\fill[green,->] (0,2.65) circle[radius=0.025];
\draw[green,->] (10/4,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,45} {-- ($(10/4,1.75) + (-\i:\r)$)}; 
\draw[green,->] (10/2,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,180} {-- ($(10/2,1.75) + (-\i:\r)$)}; 
\draw[green,->] (30/4,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,315} {-- ($(30/4,1.75) + (-\i:\r)$)};
 \draw[green,->] (10,2.65)
  \foreach \i [evaluate={\r=0.9+0.125*((90+\i)/360)}] in {-90,-80,...,450} {-- ($(10,1.75) + (-\i:\r)$)}; 
\end{tikzpicture}

\end{figuretikz} % 
%
Let $U$ denote the subset of $S^{1}$ given by the `open arc' depicted below.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green] (60:1.45) node[rotate=-30] {$($} arc (60:-25:1.45) node [rotate=-115] {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $f^{-1}(U)$ is a disjoint union of open intervals as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (5,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*5/4,0) -- (\x*5/4,-0.2);

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*5/4,-0.5) node[anchor=mid] {$\xtext$};

\draw[green] (0.125*3.75,0.25) node {$($} -- (0.33*3.75,0.25) node {$)$}
           (10/3 + 0.125*3.75,0.25) node {$($} -- (10/3 + 0.33*3.75,0.25) node {$)$};
\end{tikzpicture}

\end{figuretikz} %
%
In particular, $f^{-1}(U)$ belongs to $\curlyo_{I}$. Thus, once more, even though we have not rigorously constructed $f$, we can believe intuitively that it is continuous.

\end{itemize} 

\end{rmk}

\begin{example} \label{ExampleReverseMapFromUnitIntervalToItselfIsContinuous} Let \ar{I,I,f} be given by $t \mapsto 1-t$. Then $f$ is continuous, by Task \ref{TaskPolynomialMapsAreContinuous}. \end{example}

\begin{rmk} We may picture $f$ as follows. 
 
\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0)  -- (2,0)
              (0,-2) -- (5,-2);

\foreach \x in {0,5}
  \draw (\x,0)  -- (\x,-0.2)
        (\x,-2) -- (\x,-2.2);

\foreach \x in {0,1}
  \draw
    (5*\x,-0.5) node[anchor=mid] {$\x$}
    (5*\x,-2.5) node[anchor=mid] {$\x$};

\foreach \x in {0,1,...,5}
  \draw[green,->] (\x,0) -- (5-\x,-2);

\end{tikzpicture}

\end{figuretikz} %
%
Let $U$ denote the subset of $I$ given by the following open interval.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0)  -- (5,0);

\foreach \x in {0,5}
  \draw (\x,0)  -- (\x,-0.2);

\foreach \x in {0,1}
  \draw
    (5*\x,-0.5) node[anchor=mid] {$\x$};

\draw[green] (3,0.25) node {$($} -- (4,0.25) node {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $f^{-1}(U)$ is the following open interval.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0)  -- (5,0);

\foreach \x in {0,5}
  \draw (\x,0)  -- (\x,-0.2);

\foreach \x in {0,1}
  \draw
    (5*\x,-0.5) node[anchor=mid] {$\x$};

\draw[green] (1,0.25) node {$($} -- (2,0.25) node {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
In particular, $f^{-1}(U)$ belongs to $\curlyo_{I}$. Thus, even though this is not quite a proof yet, we can intuitively believe that $f$ is continuous. 

\end{rmk}

\begin{example} \label{ExampleTravellingAroundCircleIncludingAJumpIsNotContinuous} There is a map \ar{I,S^{1},f} travels around the circle at half speed from $(0,1)$ to $(1,0)$ for $0 \leq t \leq \tfrac{1}{2}$, and at normal speed from $(0,-1)$ to $(0,1)$ for $\tfrac{1}{2} < t \leq 1$. It is not continuous. To construct $f$ rigorously, and to prove that it is not continuous, is the topic of Task \ref{TaskTravellingAroundCircleIncludingAJumpIsNotContinuous}. \end{example}
 
\begin{rmk} We may picture $f$ as follows. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\draw[green,->] (0,1.1) arc (90:0:1.1);

\draw[green,->] (0,-1.1) arc (-90:-265:1.1);

\end{tikzpicture}

\end{figuretikz} % 
%
Let $U$ denote the subset of $S^{1}$ given by the `open arc' depicted below.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\draw[green] (60:1.25) node [rotate=-30] {$($} arc (60:-25:1.25) node [rotate=-115] {$)$}; 

\end{tikzpicture}

\end{figuretikz} % 
%
Then $f^{-1}(U)$ is a half open interval as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (5,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*5/4,0) -- (\x*5/4,-0.2);

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*5/4,-0.5) node[anchor=mid] {$\xtext$};

\draw[green] (0.125*5,0.25) node {$($} -- (0.25*5,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
In particular, $f^{-1}(U)$ does not belong to $\curlyo_{I}$. Thus we can see intuitively that $f$ is not continuous. 

\end{rmk}

\begin{example} \label{ExampleTravellingInTheUnitDiscIncludingAJumpIsNotContinuous} There is a map \ar{I,D^{2},f} which begins at $\left(-\frac{1}{2},\frac{\sqrt{3}}{2}\right)$, travels around an arc of radius $\frac{1}{4}$ centred at $\left(-\frac{3}{4},0\right)$ to $\left(-\frac{1}{2},-\frac{\sqrt{3}}{2}\right)$, jumps to $\left(\frac{1}{2},\frac{\sqrt{3}}{2}\right)$, and then travels around an arc of radius $\frac{1}{4}$ centred at $\left(\frac{3}{4},0\right)$ to $\left(\frac{1}{2},-\frac{\sqrt{3}}{2}\right)$. It is not continuous. To construct $f$ rigorously, and to prove that it is not continuous, is the topic of Task \ref{TaskTravellingInTheUnitDiscIncludingAJumpIsNotContinuous}. \end{example}

\begin{rmk} We may picture $f$ as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\clip (0,0) circle[radius=1.5];

\draw[green,->] (-225:1.5) arc (45:-45:1.5)
                (45:1.5) arc (-225:-135:1.5);

\end{tikzpicture}

\end{figuretikz} % 
%
Let $U$ denote the subset of $D^{2}$ given by the `open rectangle' depicted below. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\clip (0,0) circle[radius=1.5];

\draw[green,->] (-225:1.5) arc (45:-45:1.5)
                (45:1.5) arc (-225:-135:1.5);

\draw[dashed,purple] (-1.5,-0.35) rectangle (-0.25,-1.25);

\end{tikzpicture}

\end{figuretikz} % 
%
Then $f^{-1}(U)$ is a half open interval as follows. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (5,0);

\foreach \x/\xtext in {0,...,4}
  \draw      (\x*5/4,0) -- (\x*5/4,-0.2);

\foreach \x/\xtext in {0/0,1/\frac{1}{4},2/\frac{1}{2},3/\frac{3}{4},4/1}
  \draw
    (\x*5/4,-0.5) node[anchor=mid] {$\xtext$};

\draw[green] (0.35*5,0.05) node {$($} -- (0.5*5,0.05) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
In particular, $f^{-1}(U)$ does not belong to $\curlyo_{I}$. Thus we can see intuitively that $f$ is not continuous. 

\end{rmk}

\begin{rmk} Intuitively, continuous maps cannot `jump'! \end{rmk}

\begin{example} \label{ExampleMapFromRealsToUnitSquareWithASharpEdge} Let \ar{\reals,D^{2},f} be the map given by \[ x \mapsto \begin{cases} (-\tfrac{1}{2},0) & \text{for $x \leq -\tfrac{1}{2}$,} \\ (x,0) & \text{for $-\tfrac{1}{2} \leq x \leq 0$,} \\ (0,x) & \text{for $0 \leq x \leq \tfrac{1}{2}$,} \\ (0,\tfrac{1}{2}) & \text{for $x \geq \tfrac{1}{2}$.} \end{cases} \] 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[black] (0,0) circle[radius=1.5];

\draw[green] (-0.75,0) -- (0,0) -- (0,0.75); 

\end{tikzpicture}

\end{figuretikz} % 
%
Then $f$ is continuous. To prove this is the topic of Task \ref{TaskMapFromRealsToUnitSquareWithASharpEdge}. \end{example}

\begin{rmk} In particular, continuous maps can have `sharp edges'. In {\em differential topology}, maps are required to moreover be {\em smooth}: sharp edges are disallowed! The courses MA3402 Analyse p{\aa} Mangfoldigheter and TMA4190 Mangfoldigheter both lead towards differential topology. \end{rmk}

\section{Inclusion maps are continuous}

\begin{terminology} \label{TerminologyInclusionMap} Let $X$ be a set, and let $A$ be a subset of $X$. We refer to the map \ar{A,X,i} given by $x \mapsto x$ as an {\em inclusion map}. \end{terminology}

\begin{prpn} \label{PropositionInclusionMapIsContinuous} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$, and let $A$ be equipped with the subspace topology $\curlyo_{A}$ with respect to $(X,\curlyo_{X})$. The inclusion map \ar{A,X,i} is continuous. \end{prpn}

\begin{proof} Let $U$ be a subset of $X$ which belongs to $\curlyo_{X}$. Then $i^{-1}(U) = A \cap U$. By definition of $\curlyo_{A}$, we have that $A \cap U$ belongs to $\curlyo_{A}$. We conclude that $i^{-1}(U)$ belongs to $\curlyo_{A}$. \end{proof}

\begin{notn} Let $X$, $Y$, and $Z$ be sets. Let \ar{X,Y,f} and \ar{Y,Z,g} be maps. We denote by \ar[4]{X,Z,g \circ f} the {\em composition} of $f$ and $g$, given by $x \mapsto g\left(f(x)\right)$. \end{notn}

\section{Compositions of continuous maps are continuous}

\begin{prpn} \label{PropositionCompositionOfContinuousMapsIsContinuous} Let $(X,\curlyo_{X})$, $(Y,\curlyo_{Y})$, and $(Z,\curlyo_{Z})$ be topological spaces. Let \ar{X,Y,f} and \ar{Y,Z,g} be continuous maps. The map \ar[4]{X,Z,g \circ f} is continuous. \end{prpn}

\begin{proof} Let $U$ be a subset of $Z$ which belongs to $\curlyo_{Z}$. Then \begin{align*} (g \circ f)^{-1}(U) &= \left\{ x \in X \mid g\left(f(x)\right) \in U \right\} \\ &= \left\{ x \in X \mid f(x) \in g^{-1}(U) \right\} \\ &= f^{-1}\left(g^{-1}(U)\right). \end{align*} %
%
Since $g$ is continuous, we have that $g^{-1}(U)$ belongs to $\curlyo_{Y}$. We deduce, since $f$ is continuous, that $f^{-1}\left(g^{-1}(U)\right)$ belongs to $\curlyo_{X}$. Thus $(g \circ f)^{-1}(U)$ belongs to $\curlyo_{X}$. \end{proof}

\section{Projection maps are continuous}

\begin{notn} Let $X$ and $Y$ be sets. We denote by \ar{X \times Y,X,p_{1}} the map given by $(x,y) \mapsto x$. We denote by \ar{X \times Y,Y,p_{2}} the map given by $(x,y) \mapsto y$.  \end{notn} 

\begin{terminology} We refer to $p_{1}$ and $p_{2}$ as {\em projection maps}. \end{terminology}

\begin{prpn} \label{PropositionProjectionMapsAreContinuous} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let $X \times Y$ be equipped with the product topology $\curlyo_{X \times Y}$. Then \ar{X \times Y,X,p_{1}} and \ar{X \times Y,Y,p_{2}} are continuous. \end{prpn}

\begin{proof} Suppose that $U_{X}$ is a subset of $X$ which belongs to $\curlyo_{X}$. Then \[ p_{1}^{-1}(U_{X}) = U_{X} \times Y. \] We have that $U_{X} \times Y$ belongs to $\curlyo_{X \times Y}$. Thus $p_{1}$ is continuous. 

Suppose now that $U_{Y}$ is a subset of $Y$ which belongs to $\curlyo_{Y}$. Then \[ p_{2}^{-1}(U_{Y}) = X \times U_{Y}. \] We have that $X \times U_{Y}$ belongs to $\curlyo_{X \times Y}$. Thus $p_{2}$ is continuous.  
\end{proof} 

\begin{rmk} We can think of \ar{I \times I,I,p_{1}} as the map $(x,y) \mapsto (x,0)$. We can picture this as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) rectangle (2,2);

\fill (0,0) circle[radius=0.05] node[anchor=north east] {$(0,0)$}
      (2,0) circle[radius=0.05] node[anchor=north west] {$(1,0)$}
      (0,2) circle[radius=0.05] node[anchor=south east] {$(0,1)$}
      (2,2) circle[radius=0.05] node[anchor=south west] {$(1,1)$};

\foreach \x in {1,...,5}
  \draw[green,->] (\x/3,1.9) -- (\x/3,0.1);
\end{tikzpicture}

\end{figuretikz} %
%
We can think of \ar{I \times I,{I.},p_{2}} as the map $(x,y) \mapsto (0,y)$. We can picture this as follows.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) rectangle (2,2);

\fill (0,0) circle[radius=0.05] node[anchor=north east] {$(0,0)$}
      (2,0) circle[radius=0.05] node[anchor=north west] {$(1,0)$}
      (0,2) circle[radius=0.05] node[anchor=south east] {$(0,1)$}
      (2,2) circle[radius=0.05] node[anchor=south west] {$(1,1)$};

\foreach \x in {1,...,5}
  \draw[green,->] (1.9,\x/3) -- (0.1,\x/3);
\end{tikzpicture}

\end{figuretikz} 

\end{rmk} 
