\setcounter{chapter}{3}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Tuesday 14th January}

\section{Examples of product and subspace topologies}

\begin{rmk} We can combine our two `canonical' ways of constructing new topological spaces from old ones to obtain many interesting examples of topological spaces. \end{rmk}

\begin{notn} We denote by $S^{1}$ the set \[ \left\{ (x,y) \in \reals^{2} \mid \| (x,y) \| = 1 \right\}. \]

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1];

\end{tikzpicture}

\end{figuretikz} %
%
We denote by $\curlyo_{S^{1}}$ the subspace topology on $S^{1}$ with respect to $(\reals^{2},\stRtwo)$. \end{notn}

\begin{terminology} We refer to $S^{1}$ as the {\em circle}. \end{terminology}

\begin{example} \label{ExampleOpenArcBelongsToTopologyOnCircle} By definition, a subset of $S^{1}$ belongs to $\curlyo_{S^{1}}$ if and only if it is the intersection with $S^{1}$ of a subset of $\reals^{2}$ which belongs to $\stRtwo$. The generic example is an `open arc'. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green] (20:1.45) node[rotate=110] {$($} arc (20:70:1.45) node [rotate=-210] {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
This is, for instance, the intersection with $S^{1}$ of an `open rectangle' in $\reals^{2}$, which belongs to $\stRtwo$ by Example \ref{ExampleProductOfOpenIntervalsIsOpenInR2}. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green, dashed] (15:1.5) rectangle (75:1.55);

\end{tikzpicture}

\end{figuretikz} %
%
Since $\curlyo_{S^{1}}$ defines a topology on $S^{1}$, we also have that disjoint unions of (possibly infinitely many) `open arcs' belong to $\curlyo_{S^{1}}$.  

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green] (20:1.45) node[rotate=-70] {$)$} arc (20:70:1.45) node [rotate=-20] {$($};
\draw[green] (-135:1.45) node[rotate=-45] {$)$} arc (-135:-215:1.45) node [rotate=-135] {$($};

\end{tikzpicture}

\end{figuretikz} %
%
This can also be demonstrated directly. The subset of $S^{1}$ given by the two `open arcs' in the previous picture is, for instance, the intersection with $S^{1}$ with the subset of $\reals^{2}$ depicted below, which belongs to $\stRtwo$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green,dashed] (-1.5,0.7) -- (0.25,0.7) -- (0.25,1.5) -- (1.5,1.5) -- (1.5,0.25) -- (-0.5,0.25) -- (-0.5,-0.7) -- (-1.5,-0.7) -- (-1.5,0.7); 

\end{tikzpicture}

\end{figuretikz} %
%
Alternatively, it is the intersection with $S^{1}$ with the subset of $\reals^{2}$ consisting of two disjoint `open rectangles', depicted below.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw (0,0) circle[radius=1.25];

\draw[green,dashed] (-1.5,0.7) -- (-0.5,0.7) -- (-0.5,-0.7) -- (-1.5,-0.7) -- (-1.5,0.7);

\draw[green,dashed] (0.25,1.5) -- (1.5,1.5) -- (1.5,0.25) -- (0.25,0.25) -- (0.25,1.5); 

\end{tikzpicture}

\end{figuretikz} 

\end{example} 

\begin{notn} We denote by $\curlyo_{I^{2}}$ the product topology on $I^{2}$ with respect to two copies of $(I,\curlyo_{I})$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\fill[green] (-1,-1) rectangle (1,1);

\draw (-1,-1) rectangle (1,1);

\end{tikzpicture}

\end{figuretikz}

\end{notn}

\begin{terminology} We refer to $I^{2}$ as the {\em unit square}. \end{terminology}

\begin{rmk} The topology $\curlyo_{I^{2}}$ coincides with the subspace topology on $I^{2}$ with respect to $(\reals^{2},\stRtwo)$. To prove this is the topic of Task \ref{TaskProductOfSubspacesIsSubspaceOfProduct}.  \end{rmk}

\begin{example} \label{ExampleOpenSubsetsOfR2ContainedInUnitSquare} Any of the open sets pictured in Examples \ref{ExampleProductOfOpenIntervalsIsOpenInR2} -- \ref{ExampleOpenDiscOpenInStandardTopologyOnR2} and \ref{ExampleOpenStarBelongsToStandardTopologyOnR2} -- \ref{ExampleOpenBlobBelongsToStandardTopologyOnR2} which `fit inside $I^{2}$' belong to $I^{2}$. For instance, an `open star'.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\draw (-2,-2) rectangle (2,2);

\node[star, star points=6, star point height=0.6cm, minimum size=8em, fill=green, draw=black, dashed] at (0,0) {};

\end{tikzpicture}

\end{figuretikz} %
%
To see this, let $(x,y)$ be a point of a subset $U$ of $I^{2}$ of this kind.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\draw (-2,-2) rectangle (2,2);

\node[star, star points=6, star point height=0.6cm, minimum size=8em, draw=black, dashed] at (0,0) {};

\fill (0.5,-0.25) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
We have the following.

\begin{itemize}

\item[(1)] We can find an open interval $U_{X} = \opint{a,b}$ such that $0 < a < b < 1$ and $x \in U_{X}$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-2,-2) rectangle (2,2);

\node[star, star points=6, star point height=0.6cm, minimum size=8em, draw=black, dashed] at (0,0) {};

\fill (0.5,-0.25) circle[radius=0.05];

\draw (-2,-2) -- (-2,-2.15);
\node[anchor=mid] at (-2,-2.4) {$0$};

\draw (2,-2) -- (2,-2.15);
\node[anchor=mid] at (2,-2.4) {$1$};

\draw (0.2,-2) -- (0.2,-2.15);
\node[anchor=mid] at (0.2,-2.4) {$a$};

\draw (0.8,-2) -- (0.8,-2.15);
\node[anchor=mid] at (0.8,-2.4) {$b$};

\draw (0.5,-2) -- (0.5,-2.15);
\node[anchor=mid] at (0.5,-2.4) {$x$};

\draw[green] (0.2,-1.75) node {$]$} -- (0.8,-1.75) node {$[$};

\end{tikzpicture}

\end{figuretikz} 

\item[(2)] We can find an open interval $U_{Y} = \opint{a',b'}$ such that $0 < a' < b' < 1$ and $y \in U_{Y}$.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-2,-2) rectangle (2,2);

\node[star, star points=6, star point height=0.6cm, minimum size=8em, draw=black, dashed] at (0,0) {};

\fill (0.5,-0.25) circle[radius=0.05];

\draw (-2,-2) -- (-2.15,-2);
\node[anchor=mid] at (-2.4,-2) {$0$};

\draw (-2,2) -- (-2.15,2);
\node[anchor=mid] at (-2.4,2) {$1$};

\draw (-2,-0.6) -- (-2.15,-0.6);
\node[anchor=mid] at (-2.4,-0.6) {$a'$};

\draw (-2,0.1) -- (-2.15,0.1);
\node[anchor=mid] at (-2.4,0.1) {$b'$};

\draw (-2,-0.25) -- (-2.15,-0.25);
\node[anchor=mid] at (-2.4,-0.25) {$y$};

\draw[green] (-1.75,-0.6) node[rotate=90] {$]$} -- (-1.75,0.1) node[rotate=90] {$[$};

\end{tikzpicture}

\end{figuretikz} 

\item[(3)] We have that $U_{X} \times U_{Y} \subset U$.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\draw (-2,-2) rectangle (2,2);

\node[star, star points=6, star point height=0.6cm, minimum size=8em, draw=black, dashed] at (0,0) {};

\fill (0.5,-0.25) circle[radius=0.05];

\draw[green, dashed] (0.2,-0.6) rectangle (0.8,0.1);

\end{tikzpicture}

\end{figuretikz} 

\end{itemize} %
%
As we observed in Example \ref{ExampleOpenIntervalInUnitInterval}, both $U_{X}$ and $U_{Y}$ belong to $\curlyo_{I}$. Thus (1) -- (3) together demonstrate that $U$ belongs to $\curlyo_{I^2}$.

\end{example}

\begin{example} Let $U$ be the subset of $I^{2}$ given by \[ \left\{ (x,y) \in I^{2} \mid \left| (x,y) \right| < \tfrac{1}{2} \right\}. \]
 
\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\fill[green] (-1.5,-1.5) circle[radius=1.5];
\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.5,0) -- (-1.5,1.5) -- (1.5,1.5) -- (1.5,-1.5) -- (0,-1.5);
\draw[green] (0,-1.5) -- (-1.5,-1.5) -- (-1.5,0);

\end{tikzpicture}

\end{figuretikz} %
%
We have the following.

\begin{itemize}

\item[(1)] Let $(x,y)$ be a point of $U$ which does not lie on the boundary of $I^{2}$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.5,-1.5) rectangle (1.5,1.5);

\fill (-1,-1) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
As in Example \ref{ExampleOpenSubsetsOfR2ContainedInUnitSquare}, we can find an `open rectangle' around $(x,y)$ which is a subset of $U$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.5,-1.5) rectangle (1.5,1.5);

\fill (-1,-1) circle[radius=0.05];

\draw[green,dashed] (-1.25,-1.25) rectangle (-0.75,-0.75);

\end{tikzpicture}

\end{figuretikz}

\item[(2)] Let $(x,y)$ be a point of $U$ with $x=0$.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.5,-1.5) rectangle (1.5,1.5);

\fill (-1.5,-1) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
Let $\epsilon$ be a real number such that \[ 0 < \epsilon < \tfrac{1}{2} - y. \] Let $U_{X}$ denote the half open interval \[ \lhopint{0,\tfrac{\epsilon \sqrt{2}}{2}}. \] Let $U_{Y}$ denote the open interval \[ \opint{y - \tfrac{\epsilon \sqrt{2}}{2}, y + \tfrac{\epsilon \sqrt{2}}{2} }. \] %
%
We have that $(0,y) \in U_{X} \times U_{Y}$. As we saw in Example \ref{ExampleOpenLeftHalfOpenIntervalInUnitInterval}, we have that $U_{X}$ belongs to $\curlyo_{I}$. As we saw in Example \ref{ExampleOpenIntervalInUnitInterval}, we have that $U_{Y}$ belongs to $\curlyo_{I}$. Moreover, let $(x',y')$ be a point of $U_{X} \times U_{Y}$. Arguing as in Example \ref{ExampleOpenDiscOpenInStandardTopologyOnR2}, we have that \[ \left\| (x',y') \right\| < \tfrac{1}{2}. \] Thus $U_{X} \times U_{Y} \subset U$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.5,-0.5) -- (-1.5,1.5) -- (1.5,1.5) -- (1.5,-1.5) -- (-1.5,-1.5) -- (-1.5,-1.25);

\draw[green] (-1.5,-1.25) -- (-1.5,-0.5);
\draw[green, dashed] (-1.5,-1.25) -- (-1,-1.25) -- (-1,-0.5) -- (-1.5,-0.5);

\fill (-1.5,-1) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} 

\item[(3)] Let $(x,y)$ be a point of $U$ with $y=0$.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.5,-1.5) rectangle (1.5,1.5);

\fill (-1,-1.5) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
Let $\epsilon$ be a real number such that \[ 0 < \epsilon < \tfrac{1}{2} - x. \] Let $U_{X}$ denote the open interval \[ \opint{x - \tfrac{\epsilon \sqrt{2}}{2}, x + \tfrac{\epsilon \sqrt{2}}{2} }. \] Let $U_{Y}$ denote the half open interval \[ \lhopint{0,\tfrac{\epsilon \sqrt{2}}{2}}. \] %
%
We have that $(x,0) \in U_{X} \times U_{Y}$. As we saw in Example \ref{ExampleOpenIntervalInUnitInterval}, $U_{X}$ belongs to $\curlyo_{I}$. As we saw in Example \ref{ExampleOpenLeftHalfOpenIntervalInUnitInterval}, $U_{Y}$ belongs to $\curlyo_{I}$. Moreover, let $(x',y')$ be a point of $U_{X} \times U_{Y}$. Arguing as in Example \ref{ExampleOpenDiscOpenInStandardTopologyOnR2}, we have that \[ \left\| (x',y') \right\| < \tfrac{1}{2}. \] Thus $U_{X} \times U_{Y} \subset U$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\draw[dashed] (-1.5,-1.5) circle[radius=1.5];

\end{scope}

\draw (-1.25,-1.5) -- (-1.5,-1.5) -- (-1.5,1.5) -- (1.5,1.5) -- (1.5,-1.5) -- (-0.5,-1.5);

\draw[green] (-1.25,-1.5) -- (-0.5,-1.5);
\draw[green, dashed] (-1.25,-1.5) -- (-1.25,-1) -- (-0.5,-1) -- (-0.5,-1.5);

\fill (-1,-1.5) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} 

\end{itemize} %
%
We conclude that $U$ belongs to $\curlyo_{I^{2}}$. 

\end{example} 

\begin{rmk} Many more subsets of $I^{2}$ with `segments on the boundary' belong to $\curlyo_{I^{2}}$. \end{rmk}

\begin{example} A `truncated star' belongs to $\curlyo_{I^{2}}$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}

\clip (-1.5, -1.5) rectangle (1.5, 1.5);

\node[star, star points=6, star point height=1.25cm, minimum size=12em, fill=green, draw=black, dashed] at (0,0) {};

\end{scope}

\draw (-1.5,-1.5) rectangle (1.5,1.5); 

\end{tikzpicture}

\end{figuretikz} 

\end{example} 

\begin{example} A `half open ladder' belongs to $\curlyo_{I^{2}}$.   

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\draw[green] (-1, 1.5) -- (-0.5, 1.5);
\draw[green] (0.5, 1.5) -- (1, 1.5);
\draw[green] (-1, -1.5) -- (-0.5, -1.5);
\draw[green] (0.5, -1.5) -- (1, -1.5);

\fill[green] (-1, 1.5) -- (-0.5,1.5) -- (-0.5,1) -- (0.5,1) -- (0.5,1.5) -- (1,1.5) -- (1,-1.5) -- (0.5,-1.5) -- (0.5,-1) -- (-0.5,-1) -- (-0.5,-1.5) -- (-1,-1.5) -- (-1, 1.5);  

\fill[white] (-0.5,0.75) rectangle (0.5,0.45);
\draw[dashed] (-0.5,0.75) rectangle (0.5,0.45);

\fill[white] (-0.5,-0.15) rectangle (0.5,0.15);
\draw[dashed] (-0.5,-0.15) rectangle (0.5,0.15);

\fill[white] (-0.5,-0.75) rectangle (0.5,-0.45);
\draw[dashed] (-0.5,-0.75) rectangle (0.5,-0.45);

\draw (-1,-1.5) -- (-1.5,-1.5) -- (-1.5,1.5) -- (-1,1.5);
\draw (-0.5,1.5) -- (0.5,1.5);
\draw (1,1.5) -- (1.5,1.5) -- (1.5, -1.5) -- (1, -1.5);
\draw (-0.5, -1.5) -- (0.5, -1.5);

\draw[dashed] (-0.5,1.5) -- (-0.5,1) -- (0.5,1) -- (0.5,1.5);
\draw[dashed] (-0.5,-1.5) -- (-0.5,-1) -- (0.5,-1) -- (0.5,-1.5);
\draw[dashed] (-1,1.5) -- (-1,-1.5);
\draw[dashed] (1,1.5) -- (1,-1.5);

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} The following subset of $I^{2}$ belongs to $\curlyo_{I^{2}}$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\node[star, star points=6, star point height=0.4cm, minimum size=5em, fill=green, draw=black, dashed] at (0,0) {};

\draw (-1.5,-0.75) -- (-1.5,1.5) -- (1.5,1.5) -- (1.5,1);
\draw (1.5,0) -- (1.5,-1.5) -- (-0.75,-1.5);
\draw[green] (-0.75,-1.5) -- (-1.5,-1.5) -- (-1.5,-0.75);
\draw[green] (1.5,1) -- (1.5,0);

\fill[green] (1.5,1) rectangle (1.25,0);
\draw[dashed] (1.5,1) -- (1.25,1) -- (1.25,0) -- (1.5,0);

\begin{scope}

\clip (-1.5,-1.5) rectangle (1.5,1.5);

\fill[green] (-1.5,-1.5) circle[radius=0.75];

\draw[dashed] (-1.5,-1.5) circle[radius=0.75];

\end{scope}

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{rmk} We now introduce a few more important examples of product and subspace topologies. Exploring them is the topic of Tasks \ref{TaskExploringTheTopologyOnTheUnitDisc} -- \ref{TaskExploringTheTopologyOnTheCylinder}.
\end{rmk}

\begin{notn} Let $D^{2}$ denote the set \[ \left\{ (x,y) \in \reals^{2} \mid \left\| (x,y) \right\| \leq 1 \right\}. \] 

\begin{figuretikz}

\begin{tikzpicture}

\fill[green] (0,0) circle[radius=1.5];
\draw        (0,0) circle[radius=1.5];

\end{tikzpicture}

\end{figuretikz} %
%
We denote by $\curlyo_{D^{2}}$ the subspace topology on $D^{2}$ with respect to $(\reals^{2},\curlyo_{\reals^{2}})$. \end{notn}

\begin{terminology} We refer to $(D^{2}, \curlyo_{D^{2}})$ as the {\em unit disc}. \end{terminology}

\begin{notn} \label{NotationAnnulus} Let $k$ be a real number such that $0 < k < 1$. Let $A_{k}$ denote the set \[ \left\{ (x,y) \in \reals^{2} \mid k \leq \left\| (x,y) \right\| \leq 1 \right\}. \] 

\begin{figuretikz}

\begin{tikzpicture}

\fill[green] (0,0) circle[radius=1.5];
\draw        (0,0) circle[radius=1.5];

\fill[white] (0,0) circle[radius=1];
\draw (0,0) circle[radius=1];

\end{tikzpicture}

\end{figuretikz} % 
%
We denote by $\curlyo_{A_{k}}$ the subspace topology on $A_{k}$ with respect to $(\reals^{2},\curlyo_{\reals^{2}})$. \end{notn}

\begin{terminology} We refer to $(A_{k}, \curlyo_{A_{k}})$ as an {\em annulus}. \end{terminology}

\begin{notn} We denote by $\curlyo_{S^{1} \times I}$ the product topology on $S^{1} \times I$ with respect to $(S^{1},\curlyo_{S^{1}})$ and $(I,\curlyo_{I})$. 

\begin{figuretikz}

\begin{tikzpicture}

\draw (-0.75,0) -- (-0.75,1.75);
\draw (0.75,0) -- (0.75,1.75);

\draw (0,0) ellipse[x radius=0.75, y radius=0.25];
\draw (0,1.75) ellipse[x radius=0.75, y radius=0.25];

\end{tikzpicture}

\end{figuretikz} %
%
\end{notn}

\begin{caution} This cylinder is hollow! \end{caution}

\begin{terminology} We refer to $(S^{1} \times I, \curlyo_{S^{1} \times I})$ as the {\em cylinder}. \end{terminology}

\section{Definition of a continuous map}

\begin{notn} Let $X$ and $Y$ be sets. Let \ar{X,Y,f} be a map. Let $U$ be a subset of $Y$. We denote by $f^{-1}(U)$ the set \[ \left\{ x \in X \mid f(x) \in U \right\}. \] \end{notn}

\begin{terminology} We refer to $f^{-1}(U)$ as the {\em inverse image of $U$ under $f$}. \end{terminology}

\begin{defn} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. A map \ar{X,Y,f} is {\em continuous} if, for every $U \in \curlyo_{Y}$, the subset $f^{-1}(U)$ of $X$ belongs to $\curlyo_{X}$. \end{defn}

\begin{rmk} A map \ar{\reals,\reals,f} is continuous with respect to the standard topology on both copies of $\reals$ if and only if it is continuous in the $\epsilon-\delta$ sense that you have met in earlier courses. To prove this is the topic of Task \ref{TaskContinuousIffContinuousInEpsilonDeltaSense}. \end{rmk}

\section{Examples of continuous maps between finite topological spaces}

\begin{example} \label{ExampleContinuousMapBetweenFiniteSpaces} Let $X$ be a set with two elements $\{a,b\}$. Let $\curlyo_{X}$ denote the topology on $X$ given by \[ \left\{ \emptyset, \{b\}, X \right\}. \] In other words, $(X,\curlyo_{X})$ is the Sierpi{\'n}ski interval. Let $Y$ denote the set with three elements $\{a',b',c'\}$. Let $\curlyo_{Y}$ denote the topology on $Y$ given by \[ \left\{ \emptyset, \{a'\}, \{c'\}, \{ a',c'\}, \{b',c'\}, Y \right\}. \] %
%
Let \ar{X,Y,f} denote the map given by $a \mapsto b'$ and $b \mapsto c'$. We have the following.

\begin{itemize}

\item[(1)]  $f^{-1}(\emptyset) = \emptyset$.

\item[(2)]  $f^{-1}\left(\{a'\}\right) = \emptyset$.

\item[(3)]  $f^{-1}\left( \{c'\} \right) = \{ b \}$.

\item[(4)]  $f^{-1}\left( \{a',c'\} \right) = \{ b \}$.

\item[(5)]  $f^{-1}\left( \{b',c'\} \right) = X$.

\item[(6)]  $f^{-1}(Y) = X$. 

\end{itemize} %
%
We see that $f^{-1}(U) \in \curlyo_{X}$ for every $U \in \curlyo_{Y}$. Thus $f$ is continuous. 

\end{example}

\begin{example} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be as in Example \ref{ExampleContinuousMapBetweenFiniteSpaces}. Let \ar{Y,X,g} denote the map given by $a' \mapsto a$, $b '\mapsto b$, and $c' \mapsto a$. We have that \[ g^{-1}\left( \{ b \} \right) = \{ b'\}. \] Thus $g$ is not continuous, since $\{ b \}$ belongs to $\curlyo_{X}$, but $\{ b' \}$ does not belong to $\curlyo_{Y}$. 

\end{example}
