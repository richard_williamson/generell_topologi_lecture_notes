\ProvidesPackage{generell_topologi}

%%%%%%%%%%%%%
%Diagrams
%%%%%%%%%%%%%

\usepackage{tikz}
\usetikzlibrary{matrix, arrows, calc, patterns, intersections, decorations.markings, decorations.pathreplacing, shapes.geometric}

%Extracting x and y coordinates from a point. The arguments {*1}{*2}{*3} consist of a point *1, a name for a macro *2, and a name for a macro *3, where *2 will contain the x-coordinate, and *3 will contain the y-coordinate. For example: \tikzcoordinates{(1,1)}{\xcoord}{\yccord}. After this, \xcoord and \ycoord can be called.

\makeatletter
\newcommand{\tikzcoordinates}[3]{%
  \tikz@scan@one@point\pgfutil@firstofone#1\relax
  \edef#2{\the\pgf@x}%
  \edef#3{\the\pgf@y}%
}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%
%Comment environment
%%%%%%%%%%%%%%%%%%%%%%

\usepackage{comment}

%%%%%%%%
%Tables
%%%%%%%%

\usepackage{booktabs}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Symbols, 'cases' environment, etc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amssymb,amsmath}

%For 'tick' and 'cross' symbols.
\usepackage{pifont}
\newcommand{\tick}{\ding{51}}
\newcommand{\cross}{\ding{55}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Environment for 'dangerous bend' symbol
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{caution}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Environment for typesetting verse
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{verse}

%%%%%%%%%%%%%%%%%%%%%%%%
%Does not divide symbol
%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{centernot}

%%%%%%%%%%%%%%%%%%%
%List appearance
%%%%%%%%%%%%%%%%%%%

\usepackage{enumitem}
\setlist[itemize]{align=left, leftmargin=*, labelindent=\parindent}

%%%%%%%%%%%%%%%%%%
%References
%%%%%%%%%%%%%%%%%%

%Clickable references, with page references given in the bibliography. 
\usepackage{hyperref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Numbering of Exercises and Supplement chapters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{exercises}
\setcounter{exercises}{1}

\newcounter{supplement}
\setcounter{supplement}{1}

%%%%%%%%%%%%%%%%%%%%%%
%Theorem environments
%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amsthm}

\theoremstyle{definition}

\newtheorem{defn}{Definition}[section]
\newtheorem{rmk}[defn]{Remark}
\newtheorem{assum}[defn]{Assumption}
\newtheorem{notn}[defn]{Notation}
\newtheorem{terminology}[defn]{Terminology}
\newtheorem{convention}[defn]{Convention}
\newtheorem{recollection}[defn]{Recollection}
\newtheorem{prpn}[defn]{Proposition}
\newtheorem{thm}[defn]{Theorem}
\newtheorem{lem}[defn]{Lemma}
\newtheorem{cor}[defn]{Corollary}
\newtheorem{scholium}[defn]{Scholium}
\newtheorem{goal}[defn]{Goal}
\newtheorem{observation}[defn]{Observation}
\newtheorem{idea}[defn]{Idea}
\newtheorem{theme}[defn]{Theme}
\newtheorem{preview}[defn]{Preview}
\newtheorem{example}[defn]{Example}
\newtheorem{references}[defn]{References}
\newtheorem{question}[defn]{Question}
\newtheorem{answer}[defn]{Answer}
\newtheorem{synopsis}[defn]{Synopsis}
\newtheorem{examples}[defn]{Examples}
\newtheorem{summary}[defn]{Summary}
 
\newtheorem{task}{Task}[section] 
\renewcommand{\thetask}{\thesection.\arabic{task}}
\newtheorem{exrmk}[task]{Remark}
\newtheorem{exnotn}[task]{Notation}
\newtheorem{exdefn}[task]{Definition}
\newtheorem{exterminology}[task]{Terminology}
\newtheorem{exexample}[task]{Example}
\newtheorem{exassum}[task]{Assumption}
\newtheorem{exprpn}[task]{Proposition}
\newtheorem{excor}[task]{Corollary}

%%%%%%%%%%%%%
%Technical
%%%%%%%%%%%%%

%Given list #1= {a_1,...,a_n} and 1 ≤ #2=j ≤ n, outputs a_j.
\newcommand{\nth}[2]{
	\foreach [count=\i] \item in {#1} {
		\ifnum\i=#2	
			\item
		\fi
	}	
}

%For centering a commutative diagram (with space above and below)
\newenvironment{diagram}{\medskip\centering}{\medskip\noindent\ignorespacesafterend}

%For displaying an equality with the same spacing as a diagram as above.
\newenvironment{equality}{\medskip\centering}{\medskip\noindent\ignorespacesafterend}

%For centering a figure (with morespace above than for a diagram)
\newenvironment{figuretikz}{\bigskip\bigskip\centering}{\bigskip\noindent\ignorespacesafterend}

%%%%%%%%%%%%%%%
%Abbreviations
%%%%%%%%%%%%%%%

\newcommand{\curlyo}{\mathcal{O}}
\newcommand{\reals}{\mathbb{R}}
\newcommand{\naturals}{\mathbb{N}}
\newcommand{\opint}[1]{\left] \nth{#1}{1} , \nth{#1}{2} \right[}
\newcommand{\clint}[1]{\left[ \nth{#1}{1} , \nth{#1}{2} \right]}
\newcommand{\lhopint}[1]{\left[ \nth{#1}{1} , \nth{#1}{2} \right[}
\newcommand{\rhopint}[1]{\left] \nth{#1}{1} , \nth{#1}{2} \right]}
\newcommand{\equivclass}[1]{\langle #1 \rangle}
\newcommand{\quotientset}[1]{#1 / {\sim}}
\newcommand{\streals}{\curlyo_{\reals}}
\newcommand{\stRtwo}{\curlyo_{\reals^{2}}}
\newcommand{\closure}[2]{\mathsf{cl}_{#2}\left( #1 \right)}
\newcommand{\knotdiagram}[1]{D_{\mathsf{\nth{#1}{1}}_{\mathsf{\nth{#1}{2}}}}}
\newcommand{\knotcross}[1]{\mathsf{X}_{#1}}
\newcommand{\knotovercross}[1]{\mathsf{X}^{\mathsf{over}}_{#1}}
\newcommand{\knotundercross}[1]{\mathsf{X}^{\mathsf{under}}_{#1}}
\newcommand{\knot}[1]{\mathsf{\nth{#1}{1}}_{\mathsf{\nth{#1}{2}}}}
\newcommand{\link}[1]{\mathsf{\nth{#1}{1}}^{\mathsf{\nth{#1}{2}}}_{\mathsf{\nth{#1}{3}}}}
\newcommand{\linkdiagram}[1]{D_{\mathsf{\nth{#1}{1}}^{\mathsf{\nth{#1}{2}}}_{\mathsf{\nth{#1}{3}}}}}
\newcommand{\reidemeisterone}{\mathsf{R1}}

%%%%%%%%%%%%%%%%%%%%%%%%
%Diagram templates
%%%%%%%%%%%%%%%%%%%%%%%%

%All diagrams have as input a list #1={*1,*2,...,*n} of vertices and edges. The entry *j is selected via the command \nth defined above.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Arrow #2={*1,*2,*3} as below. The optional parameter #1 specifies the column sep value, with default #1 = 3.
%
%       *3
%   *1 ----> *2
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\ar}[2][3]{

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=#1 em, nodes={anchor=center}]
{ 
|(0-0)| \nth{#2}{1} \& |(1-0)| \nth{#2}{2} \\ 
};
	
\draw[->] (0-0) to node[auto] {$\nth{#2}{3}$} (1-0);
 
\end{tikzpicture} 

\end{diagram} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Pair of arrows #1={*1,*2,*3,*4} as below. The optional parameter #1 specifies the column sep value, with default #1 = 3.

%
%      *3
%     ---->
%  *1       *2
%     ---->
%      *4
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\pair}[2][3]{

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=#1 em, nodes={anchor=center}]
{ 
|(0-0)| \nth{#2}{1} \& |(1-0)| \nth{#2}{2} \\ 
};
	
\draw[->]  ([yshift=0.75em]{0-0}.east) to node[auto] {$\nth{#2}{3}$}  ([yshift=0.75em]{1-0}.west);

\draw[->] ([yshift=-0.75em]{0-0}.east) to node[auto,swap] {$\nth{#2}{4}$} ([yshift=-0.75em]{1-0}.west);
 
\end{tikzpicture} 

\end{diagram} }
