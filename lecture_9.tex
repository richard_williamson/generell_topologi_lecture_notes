\setcounter{chapter}{8}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 3rd February}

\section{A local characterisation of closed sets}

\begin{prpn} \label{PropositionClosedIfAndOnlyIfOwnClosure} Let $(X, \curlyo_{X})$ be a topological space. Let $V$ be a subset of $X$. Then $V$ is closed with respect to $\curlyo_{X}$ if and only if $\closure{V}{(X,\curlyo_{X})}$ is $V$. 
\end{prpn} 

\begin{proof} Suppose first that $V$ is closed with respect to $\curlyo_{X}$. Suppose that $x$ does not belong to $V$. We make the following observations.

\begin{itemize}

\item[(1)] By definition of $X \setminus V$, we have that $x$ belongs to $X \setminus V$. Moreover, since $V$ is closed with respect to $\curlyo_{X}$, we have that $X \setminus V$ belongs to $\curlyo_{X}$. In other words, $X \setminus V$ is a neighbourhood of $x$ in $X$ with respect to $\curlyo_{X}$. 

\item[(2)] By definition of $X \setminus V$ once more, we have that $V \cap \left( X \setminus V \right)$ is empty. 

\end{itemize} %
%
Together (1) and (2) establish that $x$ is not a limit point of $V$ in $X$ with respect to $\curlyo_{X}$, for any $x$ which does not belong to $V$. We conclude that $\closure{V}{(X,\curlyo_{X})}$ is $V$. 

Suppose now that $\closure{V}{(X,\curlyo_{X})}$ is $V$. Suppose that $x \in X$ does not belong to $V$. By definition of $\closure{V}{(X,\curlyo_{X})}$, we have that $x$ is not a limit point of $V$ in $X$ with respect to $\curlyo_{X}$. By definition of a limit point, we deduce that there is a neighbourhood $U_{x}$ of $x$ such that $V \cap U_{x}$ is empty. We make the following observations.

\begin{itemize}

\item[(1)] We have that \[ X \setminus V = \bigcup_{x \in X \setminus V} \{ x \}. \] We also have that $x$ belongs to $U_{x}$ for every $x \in X \setminus V$, or, in other words, that $\{ x \}$ is a subset of $U_{x}$ for every $x \in X \setminus V$. Thus we have that $\bigcup_{x \in X \setminus V} \{ x \}$ is a subset of $\bigcup_{x \in X \setminus V} U_{x}$. We deduce that $X \setminus V$ is a subset of $\bigcup_{x \in X \setminus V} U_{x}$.
 
\item[(2)] We have that \[ V \cap \left( \bigcup_{x \in X \setminus V} U_{x} \right) = \bigcup_{x \in X \setminus V} \left( V \cap U_{x} \right). \] Since $V \cap U_{x}$ is empty for every $x \in X \setminus V$, we have that $\bigcup_{x \in X \setminus V} \left( V \cap U_{x} \right)$ is empty. We deduce that $V \cap \left( \bigcup_{x \in X \setminus V} U_{x} \right)$ is empty. In other words, $\bigcup_{x \in X \setminus V} U_{x}$ is a subset of $X \setminus V$. 

\item[(3)] Since $U_{x}$ belongs to $\curlyo_{X}$, for every $x \in X \setminus V$, and since $\curlyo_{X}$ is a topology on $X$, we have that $\bigcup_{x \in X \setminus V} U_{x}$ belongs to $\curlyo_{X}$.

\end{itemize} %
%
By (1) and (2) together, we have that $\bigcup_{x \in X \setminus V} U_{x} = X \setminus V$. By (3), we deduce that $X \setminus V$ belongs to $\curlyo_{X}$. Thus $V$ is closed with respect to $\curlyo_{X}$.
\end{proof}

\begin{rmk} We now, by Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure}, have two ways to understand closed sets. The first is `global' in nature: that $X \setminus V$ belongs to $\curlyo_{X}$. The second is `local' in nature: that every limit point of $V$ belongs to $V$. 

For certain purposes in mathematics it can be appropriate to work `locally', whilst for others it can be appropriate to work `globally'. To know that `local' and `global' variants of a particular mathematical concept coincide allows us to move backwards and forwards between these points of view. This is often a very powerful technique. \end{rmk}

\section{Boundary}

\begin{defn} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. The {\em boundary} of $A$ in $X$ with respect to $\curlyo_{X}$ is the set of $x \in X$ such that, for every neighbourhood $U$ of $x$ in $X$ with respect to $\curlyo_{X}$, there is an $a \in U$ which belongs to $A$, and there is a $y \in U$ which belongs to $X \setminus A$. \end{defn}

\begin{notn} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. We shall denote the boundary of $A$ in $X$ with respect to $\curlyo_{X}$ by $\partial_{(X,\curlyo_{X})} A$. \end{notn}

\begin{rmk} Suppose that $x \in X$ belongs to $\partial_{(X,\curlyo_{X})} A$. Then $x$ is a limit point of $A$ in $X$ with respect to $\curlyo_{X}$. \end{rmk}   

\begin{rmk} Let $x$ be a limit point of $A$ which does not belong to $A$. Then $x$ belongs to $\partial_{(X,\curlyo_{X})} A$. \end{rmk}   

\begin{caution} However, as we shall see in Example \ref{ExampleBoundarySetWithFiveElements}, it is not necessarily the case that if $a$ belongs to $A$, then $a$ belongs to $\partial_{(X,\curlyo_{X})} A$. In particular, not every limit point of $A$ belongs to $\partial_{(X,\curlyo_{X})} A$.  \end{caution}

\section{Boundary in a finite example}

\begin{example} \label{ExampleBoundarySetWithFiveElements}, Let $X = \{ a,b,c,d,e \}$ be a set with five elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a\}, \{b\}, \{a,b\}, \{b,e\}, \{c,d\}, \{a,b,e\}, \{a,c,d\}, \{b,c,d\}, \{a,b,c,d\}, \{b,c,d,e\}, X \right\}. \] %
%
Let $A = \{ b,d \}$. The neighbourhoods in $X$ with respect to $\curlyo_{X}$ of each of the elements of $A$ are listed in a table in Example \ref{ExampleLimitPointsSetWithFiveElements}. To determine $\partial_{(X,\curlyo_{X})} A$, we check, for each element of $A$, whether each of its neighbourhoods both contain either $a$, $c$, or $e$, and contain either $b$ or $d$. We determined the limit points of $A$ in $X$ with respect to $\curlyo_{X}$ in Example \ref{ExampleLimitPointsSetWithFiveElements}, which saves us a little work.

\begin{center}
\begin{tabular}{l l p{20em}}
\toprule
Element & Belongs to $\partial_{(X,\curlyo_{X})} A$? & Reason \\ 
\midrule
$a$     & \cross & Not a limit point. \\
$b$     & \cross &  The neighbourhood $\{b\}$ does not contain any element of $X \setminus A$. \\
$c$     & \tick & Limit point which does not belong to $A$. \\
$d$     & \tick & Every neighbourhood of $d$ contains both $c$ and $d$. We have that $d$ belongs to $A$, and that $c$ belongs to $X \setminus A$. \\
$e$     & \tick & Limit point which does not belong to $A$. \\ 
\bottomrule
\end{tabular}
\end{center} %
%
Thus $\partial_{(X,\curlyo_{X})} A = \{ c,d,e \}$. 

\end{example}

\section{Geometric examples of boundary}

\begin{example} \label{ExampleBoundaryInRealsOfHalfOpenIntervalFromZeroToOne} Let $(X,\curlyo_{X})$ be $(\reals,\streals)$. Let $A=\lhopint{0,1}$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
By Example \ref{ExampleLimitPointsInRealsOfHalfOpenIntervalFromZeroToOne}, we have that $1$ is a limit point of $\lhopint{0,1}$ in $\reals$ with respect to $\streals$ which does not belong to $\lhopint{0,1}$. Thus $1$ belongs to $\partial_{(\reals,\streals)} \lhopint{0,1}$. By Example \ref{ExampleRealNumberLessThanZeroNotALimitPointInRealsOfHalfOpenIntervalFromZeroToOne}, we have that all other limit points of $\lhopint{0,1}$ in $\reals$ with respect to $\streals$ belong to $\lhopint{0,1}$. To determine $\partial_{(\reals,\streals)} \lhopint{0,1}$, it therefore remains to check which elements of $\lhopint{0,1}$ have the property that each of their neighbourhoods contains at least one element of $\reals \setminus \lhopint{0,1}$.

Let $U$ be a neighbourhood of $0$ in $\reals$ with respect to $\streals$. By definition of $\streals$, there is an open interval $\opint{a,b}$ such that $a < 0 < b$, and which is a subset of $U$.   

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-2,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (3,0.25) node {$[$};

\draw[purple] (-1,0.75) node {$]$}  -- (2,0.75) node {$[$};

\draw (-1,0) -- (-1,-0.15);
\node[anchor=mid] at (-1,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$b$};

\end{tikzpicture}

\end{figuretikz} % 
%
There is an $x \in \reals$ such that $a < x < 0$. In particular, $x$ belongs to $\reals \setminus \lhopint{0,1}$.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-2,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (3,0.25) node {$[$};

\draw[purple] (-1,0.75) node {$]$}  -- (2,0.75) node {$[$};

\draw (-1,0) -- (-1,-0.15);
\node[anchor=mid] at (-1,-0.5) {$a$};

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$b$};

\draw (-0.5,0) -- (-0.5,-0.15);
\node[anchor=mid] at (-0.5,-0.5) {$x$};

\end{tikzpicture}

\end{figuretikz} % 
% 
Since $\opint{a,0}$ is a subset of $\opint{a,b}$, and since $\opint{a,b}$ is a subset of $U$, we also have that $x$ belongs to $U$. This proves that if $U$ is a neighbourhood of $0$ in $\reals$ with respect to $\streals$, then $\left( \reals \setminus \lhopint{0,1} \right) \cap U$ is not empty. Thus $0$ belongs to $\partial_{(\reals,\streals)} \lhopint{0,1}$. 

Suppose now that $0 < x < 1$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (7,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (6,0) -- (6,-0.15);
\node[anchor=mid] at (6,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (6,0.25) node {$[$};

\draw (2.5,0) -- (2.5,-0.15);
\node[anchor=mid] at (2.5,-0.5) {$x$};

\end{tikzpicture}

\end{figuretikz} % 
%
Let $0 < \epsilon \leq  \min \{x, 1-x\}$. Then $\opint{x - \epsilon, x + \epsilon}$ is a neighbourhood of $x$ in $\reals$ with respect to $\streals$, and $\left( \reals \setminus \lhopint{0,1} \right) \cap \opint{x - \epsilon, x + \epsilon}$ is empty. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (7,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (6,0) -- (6,-0.15);
\node[anchor=mid] at (6,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (6,0.25) node {$[$};

\draw[purple] (1.25,0.75) node {$]$}  -- (3.75,0.75) node {$[$};

\draw (1.25,0) -- (1.25,-0.15);
\node[anchor=mid] at (1.25,-0.5) {$x - \epsilon$};

\draw (3.75,0) -- (3.75,-0.15);
\node[anchor=mid] at (3.75,-0.5) {$x + \epsilon$};

\draw (2.5,0) -- (2.5,-0.15);
\node[anchor=mid] at (2.5,-0.5) {$x$};

\end{tikzpicture}

\end{figuretikz} % 
%
Thus $x$ does not belong to $\partial_{(\reals,\streals)} \lhopint{0,1}$. We conclude that $\partial_{(\reals,\streals)} \lhopint{0,1}$ is $\{0,1\}$. 

\end{example}

\begin{example} \label{ExampleBoundaryD2InR2} Let $(X,\curlyo_{X})$ be $(\reals^{2},\stRtwo)$. Let $A = D^{2}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\filldraw[fill=green, draw=black] (0,0) circle[radius=1.5];

\end{tikzpicture}

\end{figuretikz} % 
%
Suppose that $(x,y)$ belongs to $\reals^{2} \setminus D^{2}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\fill (2.5,1.5) circle[radius=0.05];

\node at (2.5,1.1) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} % 
%
Let $\epsilon \in \reals$ be such that \[ 0 < \epsilon < \left\| (x,y) \right\| - 1. \] Let $U_{x}$ be the open interval given by \[ \opint{x - \tfrac{\epsilon \sqrt{2}}{\epsilon}, x + \tfrac{\epsilon \sqrt{2}}{\epsilon}}. \] Let $U_{y}$ be the open interval given by \[ \opint{y - \tfrac{\epsilon \sqrt{2}}{\epsilon}, y + \tfrac{\epsilon \sqrt{2}}{\epsilon}}. \] Then $U_{x} \times U_{y}$ is a neighbourhood of $(x,y)$ in $\reals^{2}$ whose intersection with $D^{2}$ is empty. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\draw[green, dashed] (2.1,1) rectangle (3.1,2);

\fill (30:3) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} % 
%
This can be proven by the same argument as is needed to carry out Task \ref{TaskPointsOutsideTheClosedDiscAreNotLimitPointsOfTheOpenUnitDiscInR2}. Thus $(x,y)$ is not a limit point of $D^{2}$ in $\reals^{2}$ with respect to $\stRtwo$. In particular, $(x,y)$ does not belong to $\partial_{(\reals^{2},\stRtwo)} D^{2}$.

Suppose now that $(x,y) \in \reals^{2}$ has the property that $\left\| (x,y) \right\| < 1$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\fill (0.5,0.5) circle[radius=0.05];

\node at (0.5,0.05) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $\epsilon \in \reals$ be such that \[ 0 < \epsilon < 1- \left\| (x,y) \right\|. \] Let $U_{x}$ be the open interval given by \[ \opint{x - \tfrac{\epsilon \sqrt{2}}{\epsilon}, x + \tfrac{\epsilon \sqrt{2}}{\epsilon}}. \] Let $U_{y}$ be the open interval given by \[ \opint{y - \tfrac{\epsilon \sqrt{2}}{\epsilon}, y + \tfrac{\epsilon \sqrt{2}}{\epsilon}}. \] Then $U_{x} \times U_{y}$ is a neighbourhood of $(x,y)$ in $\reals^{2}$ whose intersection with $D^{2}$ is empty. To check this is Task \ref{TaskPointInInteriorOfD2DoesNotBelongToBoundary}.  

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\fill (0.5,0.5) circle[radius=0.05];

\draw[green, dashed] (0.2,0.2) rectangle (0.8,0.8);

\end{tikzpicture}

\end{figuretikz} %
%
In other words, $\left( \reals^{2} \setminus D^{2} \right) \cap \left( U_{x} \times U_{y} \right)$ is empty. Thus $(x,y)$ does not belong to $\partial_{(\reals^{2},\stRtwo)} D^{2}$.

Suppose now that $\left\| (x,y) \right\| = 1$. In other words, we have that $(x,y)$ belongs to $S^{1}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\fill (45:1.5) circle[radius=0.05];

\node[anchor=west] at (40:1.65) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} %
%
Every neighbourhood of $(x,y)$ in $\reals^{2}$ with respect to $\curlyo_{\reals^{2}}$ contains an `open rectangle' $U$ to which $(x,y)$ belongs. Both $D^{2} \cap U$ and $\left( \reals^{2} \setminus D^{2} \right) \cap U$ are not empty. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];

\fill (45:1.5) circle[radius=0.05];

\draw[green,dashed] (45:1) rectangle (45:2);

\end{tikzpicture}

\end{figuretikz} %
%
Thus $(x,y)$ belongs to $\partial_{(\reals^{2},\stRtwo)} D^{2}$. To fill in the details of this argument is the topic of Task \ref{TaskPointOnCircleBelongsToBoundaryOfUnitDiscInR2}. We conclude that $\partial_{(\reals^{2},\stRtwo)} D^{2}$ is $S^{1}$.  

\end{example}

\begin{example} \label{ExampleBoundaryOfUnitSquareInR2} Let $(X,\curlyo_{X})$ be $(\reals^{2},\curlyo_{\reals^{2}})$. Let $A = I^{2}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\filldraw[fill=green,draw=black] (0,0) rectangle (2,2);

\end{tikzpicture}

\end{figuretikz} %
%
Then $\partial_{(\reals^{2},\stRtwo)} I^{2}$ is the `border around $I^{2}$'.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) rectangle (2,2);

\end{tikzpicture}

\end{figuretikz} %
%
In other words, $\partial_{(\reals^{2},\stRtwo)} I^{2}$ is $\left( \left\{ 0,1 \right\} \times I \right) \cup \left( I \times \left\{ 0,1 \right\} \right)$. To prove this is the topic of Task \ref{TaskBoundaryOfUnitSquareInR2}.   

\end{example}

\begin{example} \label{ExampleBoundaryOfAnnulusInR2} Let $(X,\curlyo_{X})$ be $(\reals^{2},\stRtwo)$. Let $A$ be an annulus $A_{k}$, for some $k \in \reals$ with $0 < k < 1$, as in Notation \ref{NotationAnnulus}. 

\begin{figuretikz}

\begin{tikzpicture}

\fill[green] (0,0) circle[radius=1.5];
\draw        (0,0) circle[radius=1.5];

\fill[white] (0,0) circle[radius=0.75];
\draw (0,0) circle[radius=0.75];

\end{tikzpicture}

\end{figuretikz} % 
%
Then $\partial_{(\reals^{2},\stRtwo)} A_{k}$ is the union of the outer and the inner circle of $A_{k}$. In other words, the union of the set \[ \left\{ (x,y) \in \reals^{2} \mid \left\| (x,y) \right\| = 1 \right\} \] and the set \[ \left\{ (x,y) \in \reals^{2} \mid \left\| (x,y) \right\| = k \right\}. \]  

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];
\draw (0,0) circle[radius=0.75];

\end{tikzpicture}

\end{figuretikz} % 
%
To prove this is the topic of Task \ref{TaskBoundaryOfAnnulusInR2}. Suppose, for instance, that $(x,y) \in \reals^{2}$ belongs to the inner circle of $A_{k}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];
\draw (0,0) circle[radius=0.75];

\fill (45:0.75) circle[radius=0.05];

\node at (45:0.2) {$(x,y)$};   

\end{tikzpicture}

\end{figuretikz} % 
%
Every neighourhood of $(x,y)$ contains an `open rectangle' around $(x,y)$ which overlaps both $A_{k}$ and the open disc which we can think of as having been cut out from $D^{2}$ to obtain $A_{k}$.  

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1.5];
\draw (0,0) circle[radius=0.75];

\fill (45:0.75) circle[radius=0.05];

\draw[green, dashed] (45:0.3) rectangle (45:1.2); 

\end{tikzpicture}

\end{figuretikz} % 

\end{example}

\begin{example} \label{ExampleBoundaryOfAnnulusInD2} Let $(X,\curlyo_{X})$ be $(D^{2},\curlyo_{D^{2}})$. Let $A$ be an annulus $A_{k}$ as in Example \ref{ExampleBoundaryOfAnnulusInR2}. 

\begin{figuretikz}

\begin{tikzpicture}

\fill[green] (0,0) circle[radius=1.5];
\draw        (0,0) circle[radius=1.5];

\fill[white] (0,0) circle[radius=0.75];
\draw (0,0) circle[radius=0.75];

\end{tikzpicture}

\end{figuretikz} % 
%
Then $\partial_{(D^{2},\curlyo_{D^{2}})} A_{k}$ is the inner circle of $A_{k}$.  

\begin{figuretikz}

\begin{tikzpicture}

\draw (0,0) circle[radius=0.75];

\end{tikzpicture}

\end{figuretikz} % 
%
To prove this is the topic of Task \ref{TaskBoundaryOfAnnulusInD2}. In particular if $(x,y) \in S^{1}$ then, unlike in Example \ref{ExampleBoundaryOfAnnulusInR2}, $(x,y)$ does not belong to $\partial_{(D^{2},\curlyo_{D^{2}})} A_{k}$. For there is a neighbourhood of $(x,y)$ in $D^{2}$ with respect to $\curlyo_{D^{2}}$ which does not overlap $D^{2} \setminus A_{k}$, the open disc which we can think of as having cut out of $D^{2}$ to obtain $A_{k}$.   

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=0.75];
\draw (30:1.5) arc (30:-300:1.5);
\draw[green] (30:1.5) arc (30:60:1.5);

\fill (45:1.5) circle[radius=0.05];

\path[name path=horizontal] (30:1.5) -- ++(-1,0);

\path[name path=vertical] (60:1.5) -- ++(0,-1);

\draw[green, dashed, name intersections={of={horizontal and vertical}}] (30:1.5) -- (intersection-1) -- (60:1.5);

\end{tikzpicture}

\end{figuretikz}  

\end{example}

\begin{caution} Example \ref{ExampleBoundaryOfAnnulusInR2} and Example \ref{ExampleBoundaryOfAnnulusInD2} demonstrate that given a set $A$, and a topological space $(X,\curlyo_{X})$ such that $A$ is a subset of $X$, the boundary of $A$ in $X$ with respect to $\curlyo_{X}$ depends upon $(X,\curlyo_{X})$. The next examples illustrate this further. \end{caution}

\begin{example} \label{ExampleBoundaryOfLetterTInR2} Let $(X,\curlyo_{X})$ be $(\reals^{2},\curlyo_{\reals^{2}})$. Let $\mathsf{T}$ denote the subset of $\reals^{2}$ given by the union of \[ \left\{ (0,y) \mid 0 \leq y \leq 1 \right\} \] and \[ \left\{ (x,1) \mid -1 \leq x \leq 1 \right\}. \] 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\end{tikzpicture}

\end{figuretikz} %
%
Then $\partial_{X} \mathsf{T}$ is $\mathsf{T}$. We have that $\mathsf{T}^{2}$ is closed in $\reals^{2}$. To prove this is the topic of Task \ref{TaskLetterTIsClosedInR2}. Thus every limit point of $\mathsf{T}^{2}$ belongs to $\mathsf{T}$. Suppose that $(x,y)$ belongs to $\mathsf{T}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill (-1,1.5) circle[radius=0.05];

\node at (-1,1.1) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} % 
%
Then the intersection with $\reals^{2} \setminus \mathsf{T}$ of every neighbourhood of $(x,y)$ in $\reals^{2}$ is not empty. To prove this is the topic of Task \ref{TaskBoundaryOfLetterTInR2}.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill (-1,1.5) circle[radius=0.05];

\draw[green, dashed] (-1.25,1.25) rectangle (-0.75,1.75);

\end{tikzpicture}

\end{figuretikz}

\end{example}

\begin{example} \label{ExampleBoundaryOfLetterTInACross} Let $X$ be the subset of $\reals^{2}$ given by the union of \[ \left\{ (0,y) \mid -1 \leq y \leq 2 \right\} \] and  \[ \left\{ (x,1) \mid -2 \leq x \leq 2 \right\}. \] 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,-1.5)  -- (0,3)
              (-3.75,1.5) -- (3.75,1.5); 

\end{tikzpicture}

\end{figuretikz} %
%
Let $\curlyo_{X}$ denote the subspace topology on $X$ with respect to $(\reals^{2},\stRtwo)$. Let $\mathsf{T}$ be as in Example \ref{ExampleBoundaryOfLetterTInR2}.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);
\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\end{tikzpicture}

\end{figuretikz} %
%
Then $\partial_{(X,\curlyo_{X})}\mathsf{T}$ is $\{ (-1,1), (0,1), (1,1), (0,0) \}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill[red] (0,1.5) circle[radius=0.05];
\fill[red] (0,0) circle[radius=0.05];
\fill[red] (2.25,1.5) circle[radius=0.05];
\fill[red] (-2.25,1.5) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
Every neighbourhood of each of these four points contains both a segment of $X \setminus \mathsf{T}$ and a segment of $\mathsf{T}$. A typical neighbourhood of $(-1,1)$, for instance, is the intersection of an `open rectangle' around $(-1,1)$ in $\reals^{2}$ with $\mathsf{T}$ as depicted below.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill[red] (-2.25,1.5) circle[radius=0.05];

\draw[dashed, green] (-2.75, 1) rectangle (-1.75,2); 

\end{tikzpicture}

\end{figuretikz} %
%
Let $(x,y)$ be a point of $\mathsf{T}$ which is not one of these four. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill (-1,1.5) circle[radius=0.05];

\node at (-1,1.1) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} % 
%
Then we can find a neighbourhood of $(x,y)$ whose intersection with $X \setminus \mathsf{T}$ is empty. For instance, an intersection of a sufficiently small `open rectangle' around $(x,y)$ in $\reals^{2}$ with $X$.   

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill (-1,1.5) circle[radius=0.05];

\draw[dashed, green] (-1.5, 1) rectangle (-0.5,2); 

\end{tikzpicture}

\end{figuretikz} %
%
Suppose that $(x,y) \in X$ does not belong to $\mathsf{T}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill (0,2.25) circle[radius=0.05];

\node at (0.6,2.25) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} %
%
Then we can find a neighbourhood of $(x,y)$ whose intersection with $X \setminus \mathsf{T}$ is empty. For instance, an intersection of a sufficiently small `open rectangle' around $(x,y)$ in $\reals^{2}$ with $X$.   

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,1.5)  -- (0,3)
              (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill (0,2.25) circle[radius=0.05];

\draw[dashed, green] (-0.25, 2) rectangle (0.25,2.5); 

\end{tikzpicture}

\end{figuretikz} %
%
To fill in the details of this argument is the topic of Task \ref{TaskBoundaryOfLetterTInACross}.  

\end{example}

\begin{example} \label{ExampleBoundaryOfLetterTInALargerLetterT} Let $X$ be the subset of $\reals^{2}$ given by the union of \[ \left\{ (0,y) \mid -2 \leq y \leq 1 \right\} \] and \[ \left\{ (x,1) \mid -2 \leq x \leq 2 \right\}. \] 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,-1.5)  -- (0,1.5)
              (-3.75,1.5) -- (3.75,1.5); 

\end{tikzpicture}

\end{figuretikz} % 
%
Let $\curlyo_{X}$ denote the subspace topology on $X$ with respect to $(\reals^{2},\stRtwo)$. Let $\mathsf{T}$ be as in Example \ref{ExampleBoundaryOfLetterTInR2}.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\end{tikzpicture}

\end{figuretikz} % 
%
Then $\partial_{(X,\curlyo_{X})}\mathsf{T}$ is $\{ (-1,1), (1,1), (0,0) \}$.
 
\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow]  (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
              (-2.25,1.5) -- (2.25,1.5); 

\fill[red] (0,0) circle[radius=0.05];
\fill[red] (2.25,1.5) circle[radius=0.05];
\fill[red] (-2.25,1.5) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
In particular $(0,1)$ does not belong to $\partial_{(X,\curlyo_{X})}\mathsf{T}$, unlike in Example \ref{ExampleBoundaryOfLetterTInACross}. We can find a neighbourhood of $(0,1)$ whose intersection with $X \setminus \mathsf{T}$ is empty, such as the intersection of a sufficiently small `open rectangle' around $(0,1)$ in $\reals^{2}$ with $X$.  

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0,0) -- (0,-1.5)
              (-2.25,1.5) -- (-3.75,1.5)
              (2.25,1.5) -- (3.75,1.5);

\draw[blue] (0,0)  -- (0,1.5)
            (-2.25,1.5) -- (2.25,1.5); 

\fill (0,1.5) circle[radius=0.05];

\draw[dashed, green] (-0.25, 1.25) rectangle (0.25,1.75); 

\end{tikzpicture}

\end{figuretikz} %
%
To give the details of the calculation of $\partial_{(X,\curlyo_{X})}\mathsf{T}$ is the topic of Task \ref{TaskBoundaryOfLetterTInALargerLetterT}.

\end{example}

\section{Connected topological spaces} 

\begin{terminology} \label{TerminologyDisjointUnion} Let $X$ be a set. Let $X_{0}$ and $X_{1}$ be subsets of $X$. The union $X_{0} \cup X_{1}$ of $X_{0}$ and $X_{1}$ is {\em disjoint} if $X_{0} \cap X_{1}$ is the empty set. \end{terminology}

\begin{notn} \label{NotationSetIsDisjoinUnionOfAPairOfSubsets} Let $X$ be a set. Let $X_{0}$ and $X_{1}$ be subsets of $X$. If $X = X_{0} \cup X_{1}$, and this union is disjoint, we write $X = X_{0} \sqcup X_{1}$. \end{notn} 

\begin{defn} \label{DefinitionConnectedTopologicalSpace} A topological space $(X, \curlyo_{X})$ is {\em connected} if there do not exist subsets $X_{0}$ and $X_{1}$ of $X$ such that the following hold. 

\begin{itemize}

\item[(1)] Neither $X_{0}$ nor $X_{1}$ is empty, and both belong to $\curlyo_{X}$.

\item[(2] We have that $X = X_{0} \sqcup X_{1}$.

\end{itemize}

\end{defn}

\section{An example of a topological space which is not connected}

\begin{rmk} We shall have to work quite hard to prove that any of our geometric examples of topological spaces are connected. Instead, we shall begin with some examples of topological spaces which are not connected. \end{rmk}

\begin{example} \label{ExampleDisjointUnionOfIntervalsIsNotConnected} Let $X = \clint{1,2} \cup \clint{4,7}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (8,0);

\draw (1,0) -- (1,-0.15);
\node[anchor=mid] at (1,-0.5) {$1$};

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$2$};

\draw[green] (1,0.25) node {$[$} -- (2,0.25) node {$]$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node[anchor=mid] at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$[$} -- (7,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $\curlyo_{X}$ denote the subspace topology on $X$ with respect to $(\reals,\streals)$. The following hold.

\begin{itemize}

\item[(1)] By Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, we have that $\opint{0,3}$ belongs to $\streals$. We have that $\clint{1,2} = X \cap \opint{0,3}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (8,0);

\draw (1,0) -- (1,-0.15);
\node[anchor=mid] at (1,-0.5) {$1$};

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$2$};

\draw[green] (1,0.25) node {$[$} -- (2,0.25) node {$]$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node[anchor=mid] at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$[$} -- (7,0.25) node {$]$};

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$3$};

\draw[purple] (0,0.75) node {$]$} -- (3,0.75) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
By definition of $\curlyo_{X}$, we deduce that $\clint{1,2}$ belongs to $\curlyo_{X}$. 

\item[(2)]  By Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, we have that $\opint{3,8}$ belongs to $\curlyo_{\reals}$. We have that $\clint{4,7} = X \cap \opint{3,8}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (9,0);

\draw (1,0) -- (1,-0.15);
\node[anchor=mid] at (1,-0.5) {$1$};

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$2$};

\draw[green] (1,0.25) node {$[$} -- (2,0.25) node {$]$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node[anchor=mid] at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$[$} -- (7,0.25) node {$]$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$3$};

\draw (8,0) -- (8,-0.15);
\node[anchor=mid] at (8,-0.5) {$8$};

\draw[purple] (3,0.75) node {$]$} -- (8,0.75) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
By definition of $\curlyo_{X}$, we conclude that $\clint{4,7}$ belongs to $\curlyo_{X}$. 

\item[(3)] We have that $X = \clint{1,2} \sqcup \clint{4,7}$, since $\clint{1,2} \cap \clint{4,7}$ is empty.

\end{itemize} %
%
We conclude that $(X,\curlyo_{X})$ is not connected. 

\end{example}

\begin{rmk} In (1), we could have chosen instead of $\opint{0,3}$ any subset of $\reals$ which belongs to $\streals$, which does not intersect $\clint{4,7}$, and of which $\clint{1,2}$ is a subset. In (2), we could have chosen instead of $\opint{3,8}$ any subset of $\reals$ which belongs to $\streals$, which does not intersect $\clint{1,2}$, and of which $\clint{4,7}$ is a subset. \end{rmk} 

