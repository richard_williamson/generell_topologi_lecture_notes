\setcounter{chapter}{6}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 27th January}

\section{Homeomorphisms}

\begin{defn} \label{DefinitionBijection} Let $X$ and $Y$ be sets. A map \ar{X,Y,f} is {\em bijective} if there is a map \ar{Y,X,g} such that $g \circ f = id_{X}$ and $f \circ g = id_{Y}$. \end{defn}

\begin{rmk} Here $id_{X}$ and $id_{Y}$ denote the respective identity maps, in the terminology of \ref{TerminologyIdentityMap}. \end{rmk}

\begin{notn} Let $X$ and $Y$ be sets, and let \ar{X,Y,f} be a bijective map. We often denote the corresponding map \ar{Y,X,g} by $f^{-1}$. \end{notn}

\begin{rmk} Let $X$ and $Y$ be sets. A map \ar{X,Y,f} is bijective in the sense of Definition \ref{DefinitionBijection} if and only if $f$ is both injective and surjective. To prove this is Task \ref{TaskBijectionIffInjectiveAndSurjective}. \end{rmk}

\begin{defn} \label{DefinitionHomeomorphism} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. A map \ar{X,Y,f} is a {\em homeomorphism} if the following hold. 

\begin{itemize}

\item[(1)] We have that $f$ is continuous,

\item[(2)] There is a continuous map \ar{Y,X,g} such that $g \circ f = id_{X}$ and $f \circ g = id_{Y}$.

\end{itemize}
 
\end{defn} 

\begin{rmk} An equivalent definition of a homeomorphism is the topic of Task \ref{TaskEquivalentCharacterisationOfAHomeomorphism}. \end{rmk} 

\begin{defn} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Then $(X,\curlyo_{X})$ is {\em homeomorphic} to $(Y,\curlyo_{Y})$ if there exists a homeomorphism \ar{X,{Y.}} \end{defn}

\begin{rmk} By Task \ref{TaskInverseToAHomeomorphismIsAHomeomorphism}, we have that $(X,\curlyo_{X})$ is homeomorphic to $(Y,\curlyo_{Y})$ if and only if there exists a homeomorphism \ar{Y,{X.}}\end{rmk}

\section{Examples of homeomorphisms between finite topological spaces}

\begin{example} \label{ExampleHomeomorphismBetweenFiniteSpaces} Let $X = \{ a,b,c \}$ be a set with three elements. Let \ar{X,X,f} be the bijective map given by $a \mapsto b$, $b \mapsto c$, and $c \mapsto a$. Let $\curlyo_{0}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a\}, \{b,c\}, X \right\}. \] Let $\curlyo_{1}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a,c\}, \{b\}, X \right\}. \] %
%
Let us regard the copy of $X$ in the source of $f$ as equipped with the topology $\curlyo_{0}$, and regard the copy of $X$ in the target of $f$ as equipped with the topology $\curlyo_{1}$. We have the following. \begin{align*} f^{-1}\left( \emptyset \right) &= \emptyset \\ f^{-1}\left( \{a,c\} \right) &= \{b,c\} \\ f^{-1}\left( \{b\} \right) &= \{a\} \\ f^{-1}\left( X \right) &= X. \end{align*} %
%
Thus $f$ is continuous. Let \ar{X,X,g} be the inverse to $f$, given by $a \mapsto c$, $b \mapsto a$, and $c \mapsto b$. We have the following. \begin{align*} g^{-1}\left( \emptyset \right) &= \emptyset \\ g^{-1} \left( \{a\} \right) &= \{b\} \\ g^{-1} \left( \{b,c\} \right) &= \{a,c\} \\ g^{-1} \left( X \right) &= X. \end{align*} Thus $g$ is continuous. We conclude that $f$ is a homeomorphism. In other words, we have that $(X,\curlyo_{0})$ and $(X,\curlyo_{1})$ are homeomorphic. \end{example}

\begin{example} \label{ExampleNonHomeomorphismBetweenFiniteHomeomorphicSpaces} Let $X$ be as in Example \ref{ExampleHomeomorphismBetweenFiniteSpaces}. Let $\curlyo_{2}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a,b\}, \{ c \}, X \right\}. \] Let $f$ be as in Example \ref{ExampleHomeomorphismBetweenFiniteSpaces}. Let us again regard the copy of $X$ in the source of $f$ as equipped with the topology $\curlyo_{0}$, but let us now regard the copy of $X$ in the target of $f$ as equipped with the topology $\curlyo_{2}$. Then $f$ is not continuous, since $f^{-1}\left( \{c\} \right) = \{ b \}$, and $\{ b \}$ does not belong to $\curlyo_{0}$. Thus $f$ is not a homeomorphism. \end{example}

\begin{rmk} \label{RemarkSelfInverseHomeomorphismBetweenFiniteHomeomorphicSpaces} Let \ar{X,X,h} be the bijective map given by $a \mapsto c$, $b \mapsto b$, and $c \mapsto a$. We have the following. \begin{align*} h^{-1}\left( \emptyset \right) &= \emptyset \\ h^{-1}\left( \{a,b\} \right) &= \{ b,c \} \\ h^{-1}\left( \{ c \} \right) &= \{ a \} \\ h^{-1}\left( X \right) &= X. \end{align*} Thus $h$ is continuous. Moreover we have that $h$ is its own inverse. We conclude that $h$ is a homeomorphism. In other words, we have that $(X,\curlyo_{0})$ and $(X,\curlyo_{2})$ are homeomorphic. \end{rmk}

\begin{caution} Example \ref{ExampleNonHomeomorphismBetweenFiniteHomeomorphicSpaces} and Remark \ref{RemarkSelfInverseHomeomorphismBetweenFiniteHomeomorphicSpaces} demonstrate that a pair of topological spaces can be homeomorphic, even though a particular map that we consider might not be a homeomorphism. It is very important to remember this! \end{caution} 

\begin{example} \label{ExampleContinuousBijectionBetweenFiniteSpacesWhichIsNotAHomeomorphism} Let $X$ be as in Example \ref{ExampleHomeomorphismBetweenFiniteSpaces}. Let $\curlyo_{3}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a\}, \{b\}, \{a,b\}, \{b,c\}, X \right\}. \] Let $f$ be as in Example \ref{ExampleHomeomorphismBetweenFiniteSpaces}. Let us regard the copy of $X$ in the source of $f$ as equipped with the topology $\curlyo_{3}$, and regard the copy of $X$ in the target of $f$ as equipped with the topology $\curlyo_{1}$. Since $\curlyo_{0}$ is a subset of $\curlyo_{3}$, the calculation of Example \ref{ExampleHomeomorphismBetweenFiniteSpaces} demonstrates that $f$ is continuous. The inverse of $f$ is the map $g$ of  Example \ref{ExampleHomeomorphismBetweenFiniteSpaces}. We have that $g^{-1}\left(\{b\}\right) = \{c\}$, and $\{c\}$ does not belong to $\curlyo_{1}$. Thus $g$ is not continuous. We conclude that $f$ is not a homeomorphism. \end{example}

\begin{rmk} \label{RemarkTopologiesOfHomeomorphicSpacesHaveSameCardinality} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be homeomorphic topological spaces. Then $\curlyo_{X}$ and $\curlyo_{Y}$ must have the same cardinality. To prove this is Task \ref{TaskTopologiesOfHomeomorphicSpacesHaveSameCardinality}. Thus $(X,\curlyo_{3})$ is not homeomorphic to $(X,\curlyo_{1})$. \end{rmk}  

\begin{caution} Example \ref{ExampleContinuousBijectionBetweenFiniteSpacesWhichIsNotAHomeomorphism} and Remark \ref{RemarkTopologiesOfHomeomorphicSpacesHaveSameCardinality} demonstrate that there can be a continuous bijective map from one topological space to another, and yet these topological spaces might not be homeomorphic. It is very important to remember this! This phenomenon does not occur in group theory or linear algebra, for instance. \end{caution}

\section{Geometric examples of homeomorphisms}

\begin{rmk} Two geometric examples of topological spaces are, intuitively, homeomorphic if we can bend, stretch, compress, twist, or otherwise `manipulate in a continuous manner', one to obtain the other. We can sharpen or smooth edges. We cannot cut or tear. \end{rmk}

\begin{rmk} It may help you to think of geometric examples of topological spaces as made of dough, or of clay that has not yet been fired! \end{rmk}

\begin{example} \label{ExampleOpenIntervalHomeomorphicToOpenUnitInterval} Suppose that $a,b \in \reals$, and that $a < b$.  Let the open interval $\opint{a,b}$ be equipped with the subspace topology $\curlyo_{\opint{a,b}}$ with respect to $(\reals,\curlyo_{\reals})$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (4,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Let the open interval $\opint{0,1}$ also be equipped with the subspace topology with respect to $(\reals,\curlyo_{\reals})$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$1$};

\draw[green] (0,0.25) node {$]$} -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $\left( \opint{a,b}, \curlyo_{\opint{a,b}} \right)$ is homeomorphic to $\left( \opint{0,1}, \curlyo_{\opint{0,1}} \right)$. Intuitively we can stretch or shrink, and translate, $\opint{0,1}$ to obtain $\opint{a,b}$. To be rigorous, the map \ar{\opint{0,1},\opint{a,b},f} given by $t \mapsto a(1-t) + bt$ is a homeomorphism.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (-0.1,0)  -- (2.1,0)
              (0.9,-2) -- (5.1,-2);

\foreach \x/\xtext in {0.9/a,5.1/b}
  \draw
    (\x,-2.5) node[anchor=mid] {$\xtext$};

\foreach \x in {0,1,...,4}
  \draw[green,->,shorten >=0.2em, shorten <=0.2em] (\x/2,0) -- ({1-\x/4 + 5*(\x/4)},-2);

\foreach \x in {-0.1,2.1}
  \draw (\x,0)  -- (\x,-0.2);

\foreach \x in {0.9,5.1}
  \draw (\x,-2)  -- (\x,-2.2);

\draw (-0.1,-0.5) node[anchor=mid] {$0$};
\draw (2.1,-0.5) node[anchor=mid,fill=white,text=black] {$1$};

\end{tikzpicture}

\end{figuretikz} %
%
For the following hold.

\begin{itemize}

\item[(1)] By Task \ref{TaskPolynomialMapsAreContinuous}, we have that $f$ is continuous.

\item[(2)] Let \ar{\opint{a,b},\opint{0,1},g} be the map given by $t \mapsto \frac{t-a}{b-a}$. By Task \ref{TaskPolynomialMapsAreContinuous}, we have that $g$ is continuous. Moreover we have the following, for every $t \in \opint{0,1}$. \begin{align*} %
%
g\left( f (t) \right) &= g \left( a(1-t) + bt \right) \\
                      &= \frac{a(1-t) + bt - a}{b-a} \\
                      &= \frac{t(b-a)}{b-a} \\
                      &= t.                 
\end{align*} %
%
Thus we have that $g \circ f = id_{\opint{0,1}}$. We also have the following, for every $t \in \opint{a,b}$. \begin{align*} %
%
f\left( g (t) \right) &= f \left( \frac{t-a}{b-a} \right) \\ 
                      &= a \left( 1 - \frac{t-a}{b-a} \right) + b \left( \frac{t-a}{b-a} \right) \\
                      &= \frac{ a \left(b-a\right) - a \left(t-a\right) + b \left(t-a \right) }{b-a} \\
                      &= \frac{t\left( b -a \right)}{b-a} \\
                      &= t. 
\end{align*} %
%
Thus we have that $f \circ g = id_{\opint{a,b}}$. 

\end{itemize}

\end{example}

\begin{example} \label{ExampleOpenIntervalsAreHomeomorphic} Suppose that $a,b \in \reals$, and that $a < b$. Suppose also that $a', b' \in \reals$, and that $a' < b'$. Let $\opint{a,b}$ be equipped with the subspace topology $\curlyo_{\opint{a,b}}$ with respect to $(\reals,\streals)$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} % 
%
Let $\opint{a',b'}$ be equipped with the subspace topology $\curlyo_{\opint{a',b'}}$ with respect to $(\reals,\streals)$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a'$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$b'$};

\draw[green] (0,0.25) node {$]$} -- (4,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} % 
%
By Example \ref{ExampleOpenIntervalHomeomorphicToOpenUnitInterval} and Remark \ref{RemarkTwoOutOfThreePropertyForHomeomorphisms}, we have that $\left( \opint{a,b}, \curlyo_{\opint{a,b}} \right)$ is homeomorphic to $\left( \opint{a',b'}, \curlyo_{\opint{a',b'}} \right)$. In other words, we use the homeomorphism of Example \ref{ExampleOpenIntervalHomeomorphicToOpenUnitInterval} to construct a homeomorphism from $(\opint{a,b},\curlyo_{\opint{a,b}})$ to $(\opint{a',b'},\curlyo_{\opint{a',b'}})$ in two steps. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (1.4,2)  -- (4.6,2)
              (-0.1,0)  -- (2.1,0)
              (0.9,-2) -- (5.1,-2);

\foreach \x/\xtext in {1.4/a,4.6/b}
  \draw
    (\x,1.5) node[anchor=mid] {$\xtext$};

\foreach \x/\xtext in {0.9/a',5.1/b'}
  \draw
    (\x,-2.5) node[anchor=mid] {$\xtext$};

\draw[green,->,shorten >=0.2em, shorten <=0.2em] (1.5,2)  -- (0,0);
\draw[green,->,shorten >=0.2em, shorten <=0.2em] (2.25,2) -- (0.5,0); 
\draw[green,->,shorten >=0.2em, shorten <=0.2em] (3,2)    -- (1,0);
\draw[green,->,shorten >=0.2em, shorten <=0.2em] (3.75,2) -- (1.5,0);
\draw[green,->,shorten >=0.2em, shorten <=0.2em] (4.5,2)  -- (2,0);

\foreach \x in {0,1,...,4}
  \draw[green,->,shorten >=0.2em, shorten <=0.2em] (\x/2,0) -- ({1-\x/4 + 5*(\x/4)},-2);

\foreach \x in {-0.1,2.1}
  \draw (\x,0)  -- (\x,-0.2);

\foreach \x in {0.9,5.1}
  \draw (\x,-2)  -- (\x,-2.2);

\foreach \x in {1.4,4.6}
  \draw (\x,2)  -- (\x,1.8);

\draw (-0.1,-0.5) node[anchor=mid] {$0$};
\draw (2.1,-0.5) node[anchor=mid,fill=white,text=black] {$1$};

\end{tikzpicture}

\end{figuretikz}

\end{example}

\begin{rmk} \label{RemarkConstructingHomeomorphismsWithAnIntermediateStep} The technique of Example \ref{ExampleOpenIntervalsAreHomeomorphic} and Example \ref{ExampleOpenIntervalsAreHomeomorphic} is a good one to keep in mind when trying to prove that a pair of topological spaces are homeomorphic. 

\begin{itemize}

\item[(1)] Look for an intermediate special case, which in this case is where one of the topological spaces is $\left( \opint{0,1}, \curlyo_{\opint{0,1}} \right)$, for which we can explicitly write down a homeomorphism without too much difficulty. 

\item[(2)] Apply a `machine', which in this case is the fact that we can compose and invert homeomorphisms, to achieve our original goal. 

\end{itemize}
\end{rmk}

\begin{example} \label{ExampleClosedIntervalsAreHomeomorphic} Suppose that $a,b \in \reals$, and that $a < b$. Suppose also that $a', b' \in \reals$, and that $a' < b'$. Let $\clint{a,b}$ be equipped with the subspace topology $\curlyo_{\clint{a,b}}$ with respect to $(\reals,\streals)$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$[$} -- (3,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $\clint{a',b'}$ be equipped with the subspace topology $\curlyo_{\clint{a',b'}}$ with respect to $(\reals,\streals)$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a'$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$b'$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $\left( \clint{a,b}, \curlyo_{\clint{a,b}} \right)$ is homeomorphic to $\left( \clint{a',b'}, \curlyo_{\clint{a',b'}} \right)$. Intuitively, we can stretch or shrink, and translate, $\clint{a,b}$ to obtain $\clint{a,b}$. To be rigorous, we can argue in exactly the same way as in Example \ref{ExampleOpenIntervalsAreHomeomorphic} and Example \ref{ExampleOpenIntervalsAreHomeomorphic}, with the unit interval $(I,\curlyo_{I})$ as the intermediate special case. 

\end{example}

\begin{rmk} \label{RemarkPointIsNotHomeomorphicToAClosedIntervalOfStrictlyPositiveLength} The assumption that $a<b$ and $a' < b'$ is crucial in Example \ref{ExampleClosedIntervalsAreHomeomorphic}. Let $a \in \reals$. Let $\{ a \}=\clint{a,a}$ be equipped with the subspace topology $\curlyo_{\{ a \}}$ with respect to $(\reals,\streals)$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (2,0);

\draw (1,0) -- (1,-0.15);
\node at (1,-0.5) {$a$};

\fill[green] (1,0.25) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
Suppose that $a',b' \in \reals$, and that $a' < b'$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a'$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$b'$};

\draw[green] (0,0.25) node {$[$} -- (2,0.25) node {$]$};

\end{tikzpicture} 

\end{figuretikz} %
%
We have the following.

\begin{itemize}

\item[(1)] A homeomorphism is in particular a bijection, as observed in Task \ref{TaskEquivalentCharacterisationOfAHomeomorphism}. 

\item[(2)] There is no bijective map \ar{{\{a\}},{\clint{a',b'}.}} To check that you understand this is Task \ref{TaskNoBijectiveMapBetweenAPointAndASetWithMoreThanOnePoint}.  

\end{itemize} %
%
We conclude that $\left( \{a\}, \curlyo_{\{ a \}} \right)$ is not homeomorphic to $\left( \clint{a',b'}, \curlyo_{\clint{a',b'}} \right)$. Can you see where the argument of Example \ref{ExampleOpenIntervalsAreHomeomorphic} breaks down? This is Task \ref{TaskProofThatClosedIntervalHomeomorphicToUnitIntervalBreaksDownForAPoint}.  \end{rmk}

\begin{rmk} In a nutshell, we can shrink a closed interval to a closed interval which has as small a strictly positive length as we wish, but not to a point. \end{rmk}

\begin{example} \label{ExampleOpenIntervalFromMinusOneToOneIsHomeomorphicToReals} Let the open interval $\opint{-1,1}$ be equipped with the subspace topology $\curlyo_{\opint{-1,1}}$ with respect to $(\reals,\curlyo_{\reals})$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$-1$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$1$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $\left( \opint{-1,1}, \curlyo_{\opint{-1,1}} \right)$ is homeomorphic to $(\reals,\curlyo_{\reals})$. Intuitively, think a cylindrical piece of dough. The dough can be worked in such a way that the cylinder becomes a longer and longer piece of spaghetti. We can think of open intervals in topology in a similar way! 

With dough, our piece of spaghetti would eventually snap, but the mathematical dough of which an open interval is made can be stretched as much as we like, to the end of time! If we `wait long enough', our mathematical piece of spaghetti will be longer than the distance between any pair of real numbers! A way to visualise this is depicted below.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (-1.1,0)  -- (1.1,0)
              (-5.1,-2) -- (5.1,-2);

\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0,0) -- (0,-2); 
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.2,0) -- (-1.25,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.2,0) -- (1.25,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.4,0) -- (-2.5,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.4,0) -- (2.5,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.6,0) -- (-3.75,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.6,0) -- (3.75,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.8,0) -- (-5,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.8,0) -- (5,-2);  

\node[anchor=mid] at (2.25,0) {$\opint{-1,1}$};  
\node[anchor=mid] at (5.75,-2) {$\reals$};  

\end{tikzpicture}

\end{figuretikz} %
%
To be rigorous, the map \ar{\opint{-1,1},\reals,f} given by $t \mapsto \frac{t}{1 - | t |}$ is a homeomorphism. For the following hold.

\begin{itemize}

\item[(1)] We have that $f$ is continuous. To prove this is the topic of Task \ref{TaskMapFromOpenIntervalFromMinusOneToOneToRealsIsContinuous}.

\item[(2)] Let \ar{\reals,\opint{-1,1},g} be the map given by $t \mapsto \frac{t}{1+ |t|}$. We have that $g$ is continuous. To prove this is the topic of Task \ref{TaskMapFromRealsToOpenIntervalFromMinusOneToOneIsContinuous}. Moreover we have that $g\left(f(t)\right) = t$ for all $t \in \opint{-1,1}$, so that $g \circ f = id_{\opint{-1,1}}$. In addition we have that $f\left(g(t)\right) = t$ for all $t \in \reals$, so that $f \circ g = id_{\reals}$. To prove the last two statements is the topic of Task \ref{TaskMapsBetweenOpenIntervalFromMinusOneToOneAndRealsAreContinuous}. 

\end{itemize}
 
\end{example}

\begin{example} \label{ExampleHomeomorphismFromOpenIntervalToReals} Suppose that $a$ and $b$ belong to $\reals$, and that $a < b$. Let $\curlyo_{\opint{a,b}}$ denote the subspace topology on $\opint{a,b}$ with respect to $(\reals,\streals)$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
By Example \ref{ExampleOpenIntervalsAreHomeomorphic}, Example \ref{ExampleOpenIntervalFromMinusOneToOneIsHomeomorphicToReals} and Remark \ref{RemarkTwoOutOfThreePropertyForHomeomorphisms}, we have that $(\opint{a,b},\curlyo_{\opint{a,b}})$ is homeomorphic to $(\reals,\streals)$. Following the technique described in Remark \ref{RemarkConstructingHomeomorphismsWithAnIntermediateStep}, we use the homeomorphisms of Example \ref{ExampleOpenIntervalsAreHomeomorphic} and Example \ref{ExampleOpenIntervalFromMinusOneToOneIsHomeomorphicToReals} to construct a homeomorphism from $(\opint{a,b},\curlyo_{\opint{a,b}})$ to $(\reals,\streals)$ in two steps. 
 
\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[yellow] (0.4,2)   -- (3.6,2)
              (-1.1,0)  -- (1.1,0)
              (-5.1,-2) -- (5.1,-2);

\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.5,2) -- (-1,0); 
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (1.25,2) -- (-0.5,0);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (2,2) -- (0,0);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (2.75,2) -- (0.5,0);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (3.5,2) -- (1,0);  

\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0,0) -- (0,-2); 
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.2,0) -- (-1.25,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.2,0) -- (1.25,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.4,0) -- (-2.5,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.4,0) -- (2.5,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.6,0) -- (-3.75,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.6,0) -- (3.75,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (-0.8,0) -- (-5,-2);  
\draw[->, green, shorten >= 0.25em, shorten <= 0.25em] (0.8,0) -- (5,-2);  

\node[anchor=mid] at (4.5,2) {$\opint{a,b}$};  
\node[anchor=mid] at (2.25,0) {$\opint{-1,1}$};  
\node[anchor=mid] at (5.75,-2) {$\reals$};  

\end{tikzpicture}

\end{figuretikz}

   \end{example}

\begin{example} Let $a_{0},b_{0},a_{0}',b_{0}' \in \reals$ be such that $a_{0} < b_{0}$ and $a_{0}' < b_{0}'$.  Let $X_{0}$ be the `open rectangle' given by $\opint{a_{0},b_{0}} \times \opint{a_{0}',b_{0}'}$, equipped with the subspace topology $\curlyo_{X_{0}}$ with respect to $(\reals^{2},\stRtwo)$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) rectangle (4,2);
\draw[dashed] (0,0) rectangle (4,2);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{0}$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$b_{0}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{0}'$};

\draw (0,2) -- (-0.15,2);
\node at (-0.5,2) {$b_{0}'$};

\end{tikzpicture}

\end{figuretikz} % 
%
Let $a_{1},b_{1},a_{1}',b_{1}' \in \reals$ be such that $a_{1} < b_{1}$ and $a_{1}' < b_{1}'$. Let $X_{1}$ be the `open rectangle' given by $\opint{a_{1},b_{1}} \times \opint{a_{1}',b_{1}'}$, equipped with the subspace topology $\curlyo_{X_{1}}$ with respect to $(\reals^{2},\stRtwo)$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) rectangle (3.5,5);
\draw[dashed] (0,0) rectangle (3.5,5);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{1}$};

\draw (3.5,0) -- (3.5,-0.15);
\node at (3.5,-0.5) {$b_{1}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{1}'$};

\draw (0,5) -- (-0.15,5);
\node at (-0.5,5) {$b_{1}'$};

\end{tikzpicture}

\end{figuretikz} % 
%
By Example \ref{ExampleOpenIntervalsAreHomeomorphic}, we have that $\left(\opint{a_{0},b_{0}},\curlyo_{\opint{a_{0},b_{0}}}\right)$ is homeomorphic to $\left(\opint{a_{1},b_{1}},\curlyo_{\opint{a_{1},b_{1}}}\right)$, and that $\left(\opint{a_{0}',b_{0}'},\curlyo_{\opint{a_{0}',b_{0}'}}\right)$ is homeomorphic to $\left(\opint{a_{1}',b_{1}'},\curlyo_{\opint{a_{1}',b_{1}'}}\right)$. By Task \ref{TaskProductOfHomeomorphismsIsAHomeomorphism}, we deduce that $(X_{0},\curlyo_{X_{0}})$ is homeomorphic to $(X_{1},\curlyo_{X_{1}})$. 

\end{example} 

\begin{example} \label{ExampleClosedRectanglesAreHomeomorphic} Let $a_{0},b_{0},a_{0}',b_{0}' \in \reals$ be such that $a_{0} < b_{0}$ and $a_{0}' < b_{0}'$.  Let $X_{0}$ be the `closed rectangle' given by $\clint{a_{0},b_{0}} \times \clint{a_{0}',b_{0}'}$, equipped with the subspace topology $\curlyo_{X_{0}}$ with respect to $(\reals^{2},\stRtwo)$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) rectangle (3,3);
\draw (0,0) rectangle (3,3);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{0}$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b_{0}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{0}'$};

\draw (0,3) -- (-0.15,3);
\node at (-0.5,3) {$b_{0}'$};

\end{tikzpicture}

\end{figuretikz} % 
%
Let $a_{1},b_{1},a_{1}',b_{1}' \in \reals$ be such that $a_{1} < b_{1}$ and $a_{1}' < b_{1}'$. Let $X_{1}$ be the `closed rectangle' given by $\clint{a_{1},b_{1}} \times \clint{a_{1}',b_{1}'}$, equipped with the subspace topology $\curlyo_{X_{1}}$ with respect to $(\reals^{2},\stRtwo)$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) rectangle (6,1);
\draw         (0,0) rectangle (6,1);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{1}$};

\draw (6,0) -- (6,-0.15);
\node at (6,-0.5) {$b_{1}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{1}'$};

\draw (0,1) -- (-0.15,1);
\node at (-0.5,1) {$b_{1}'$};

\end{tikzpicture}

\end{figuretikz} % 
%
By Example \ref{ExampleClosedIntervalsAreHomeomorphic}, we have that $\left(\clint{a_{0},b_{0}},\curlyo_{\clint{a_{0},b_{0}}}\right)$ is homeomorphic to $\left(\clint{a_{1},b_{1}},\curlyo_{\clint{a_{1},b_{1}}}\right)$, and that $\left(\clint{a_{0}',b_{0}'},\curlyo_{\clint{a_{0}',b_{0}'}}\right)$ is homeomorphic to $\left(\clint{a_{1}',b_{1}'},\curlyo_{\opint{a_{1}',b_{1}'}}\right)$. By Task \ref{TaskProductOfHomeomorphismsIsAHomeomorphism}, we deduce that $(X_{0},\curlyo_{X_{0}})$ is homeomorphic to $(X_{1},\curlyo_{X_{1}})$. 

\end{example}

\begin{rmk} As in Remark \ref{RemarkPointIsNotHomeomorphicToAClosedIntervalOfStrictlyPositiveLength}, it is crucial in Example \ref{ExampleClosedRectanglesAreHomeomorphic} that the inequalities are strict. For instance, let $a,a_{0}',b_{0}' \in \reals$ be such that  $a_{0}' < b_{0}'$. Let $X_{0}$ be the line $\{a\} \times \clint{a_{0}',b_{0}'}$, equipped with the subspace topology $\curlyo_{X_{0}}$ with respect to $(\reals^{2},\curlyo_{\reals^{2}})$.  

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\draw[green] (0,0) -- (0,3);

\fill (0,0) circle[radius=0.05];
\node at (0,-0.5) {$(a,a_{0}')$};

\fill (0,3) circle[radius=0.05];
\node at (0,3.5) {$(a,b_{0}')$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $a_{1},b_{1},a_{1}',b_{1}' \in \reals$ be such that $a_{1} < b_{1}$ and $a_{1}' < b_{1}'$. Let $X_{1}$ be the `closed rectangle' given by $\clint{a_{1},b_{1}} \times \clint{a_{1}',b_{1}'}$, equipped with the subspace topology $\curlyo_{X_{1}}$ with respect to $(\reals^{2},\stRtwo)$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) rectangle (3,2);
\draw         (0,0) rectangle (3,2);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{1}$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b_{1}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{1}'$};

\draw (0,2) -- (-0.15,2);
\node at (-0.5,2) {$b_{1}'$};

\end{tikzpicture}

\end{figuretikz} % 
%
Then $(X_{0},\curlyo_{X_{0}})$ is not homeomorphic to $(X_{1},\curlyo_{X_{1}})$. We cannot prove this yet, but we shall be able to soon, after we have studied {\em connectedness}. 
\end{rmk}

\begin{example} \label{ExampleCircleIsHomeomorphicToASquare} Let $X$ be the square depicted below, consisting of just the four lines, with no `inside'. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) rectangle (2.5,2.5);

\fill (2.5,2.5)  circle[radius=0.05] node[anchor=south west] {$(2,2)$}; 
\fill (2.5,0) circle[radius=0.05] node[anchor=north west] {$(2,-2)$}; 
\fill (0,2.5)  circle[radius=0.05] node[anchor=south east] {$(-2,2)$}; 
\fill (0,0) circle[radius=0.05] node[anchor=north east] {$(-2,-2)$}; 

\end{tikzpicture}

\end{figuretikz} %
%
In other words, $X$ is given by \[ \left( \{ -2,2 \} \times \clint{-2,2} \right) \cup \left( \clint{-2,2} \times \{-2,2\} \right). \]  Let $\curlyo_{X}$ denote the subspace topology on $X$ with respect to $(\reals^{2},\stRtwo)$. Then $(X,\curlyo_{X})$ is homeomorphic to the circle $(S^{1},\curlyo_{S^{1}})$.

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\end{tikzpicture}

\end{figuretikz} %
%
A way to construct a homeomorphism \ar{S^{1},X,f} is to send each $x \in S^{1}$ to the unique $y \in X$ such that $y=kx$, where $k \in \reals$ has the property that $k \geq 0$. To rigorously write down the details is the topic of Task \ref{TaskCircleIsHomeomorphicToBoundaryOfSquare}. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];
\draw (-1.5,-1.5) rectangle (1.5,1.5);

\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (45:1) -- (1.5,1.5);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (-45:1) -- (1.5,-1.5);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (135:1) -- (-1.5,1.5);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (-135:1) -- (-1.5,-1.5);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (90:1) -- (0,1.5);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (-90:1) -- (0,-1.5);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (0:1) -- (1.5,0);
\draw[green,->, shorten >= 0.2em, shorten <= 0.2em] (180:1) -- (-1.5,0);

\end{tikzpicture}

\end{figuretikz} %
%
\end{example}

\begin{rmk} Think of a circular piece of string on a table. Even without stretching it, you could manipulate it so that it becomes a square! \end{rmk}

\begin{example} \label{ExampleUnitDiscHomeomorphicToUnitSquare} A similar argument to that of Example \ref{ExampleCircleIsHomeomorphicToASquare} demonstrates that the unit disc $(D^{2},\curlyo_{D^{2}})$ 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\filldraw[green, draw=black] (0,0) circle[radius=1];

\end{tikzpicture}

\end{figuretikz} %
%
is homeomorphic to the unit square $(I^{2},\curlyo_{I^{2}})$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\filldraw[green, draw=black] (0,0) rectangle (1.75,1.75);

\end{tikzpicture}

\end{figuretikz} %
% 
To prove this is the topic of Task \ref{TaskUnitDiscHomeomorphicToUnitSquare}. 

\end{example}

\begin{example} \label{ExampleDiamondIsHomeomorphicToCircle} Let $Y$ denote the union of the set \[ \left\{ (x,y) \in \reals^{2} \mid \text{$-1 \leq x \leq 0$ and $\left| y \right| = 1 + x$} \right\} \] and the set \[ \left\{ (x,y) \in \reals^{2} \mid \text{$0 \leq x \leq 1$ and $\left| y \right| = 1 - x$} \right\}. \] Let $\curlyo_{Y}$ denote the subspace topology on $Y$ with respect to $(\reals^{2},\stRtwo)$.  

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-1,0) -- (0,1) -- (1,0) -- (0,-1) -- (-1,0);

\end{tikzpicture}

\end{figuretikz} %
%
Then $(Y,\curlyo_{Y})$ is homeomorphic to the circle $(S^{1},\curlyo_{S^{1}})$.  

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,0) circle[radius=1];

\end{tikzpicture}

\end{figuretikz} %
%
A way to construct a homeomorphism \ar{Y,S^{1},f} is to send each $(x,y_{0}) \in Y$ to the unique $(x,y_{1}) \in S^{1}$ such that $y_{1}=ky_{0}$, where $k \in \reals$ has the property that $k \geq 0$. To rigorously write down the details is the topic of Task \ref{}. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) circle[radius=1.5];

\draw (-1.5,0) -- (0,1.5) -- (1.5,0) -- (0,-1.5) -- (-1.5,0);

\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (0.5,1) -- (0.5,1.45); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (1,0.5) -- (1,1.1); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (-0.5,1) -- (-0.5,1.45); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (-1,0.5) -- (-1,1.1); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (0.5,-1) -- (0.5,-1.45); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (1,-0.5) -- (1,-1.1); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (-0.5,-1) -- (-0.5,-1.45); 
\draw[green, ->, shorten >= 0.2em, shorten <= 0.2em] (-1,-0.5) -- (-1,-1.1); 

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{rmk} By Remark \ref{RemarkTwoOutOfThreePropertyForHomeomorphisms}, we have that the topological space $(X,\curlyo_{X})$ of Example \ref{ExampleCircleIsHomeomorphicToASquare} is homeomorphic to the topological space $(Y,\curlyo_{Y})$ of Example \ref{ExampleDiamondIsHomeomorphicToCircle}, since both are homeomorphic to $(S^{1},\curlyo_{S^{1}})$. To prove this in a different way is the topic of Task \ref{ExampleDiamondIsHomeomorphicToCircle}. \end{rmk}

\begin{example} Let $X$ be a `blob' in $\reals^{2}$, equipped with the subspace topology $\curlyo_{X}$ with respect to $(\reals^{2},\curlyo_{\reals^{2}})$. 
 
\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\filldraw[green, draw=black, scale=0.75] plot [smooth cycle] coordinates {(-0.5,-1) (0.5,0.2) (1.5,-1) (3.2,-0.1) (2.5,2.5) (1.5,2) (0.5,2.75) (-0.5,1.75) (-1.4,2) (-1.2,0.75) (-1.3,-0.1)};

\end{tikzpicture}

\end{figuretikz} % 
%
Then $(X,\curlyo_{X})$ is homeomorphic to the unit square $(I^{2},\curlyo_{I^{2}})$. If $X$ were made of dough, it would be possible to knead it to obtain a square! To rigorously prove that $(X,\curlyo_{X})$ is homeomorphic to $(I^{2},\curlyo_{I^{2}})$ is the topic of Task \ref{}. 

\end{example}

\begin{rmk} In Task \ref{}, we shall not explicitly describe a subset of $\reals^{2}$ such as the `blob' above. We shall work a little more abstractly, with subsets of $\reals^{2}$ which can be `cut into star shaped pieces'. Here `star shaped' has a technical meaning, discussed before Task \ref{}. \end{rmk}
