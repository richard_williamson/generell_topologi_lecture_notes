\setcounter{chapter}{15}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Tuesday 25th February}

\section{From last time}

\begin{prpn} \label{PropositionClosedSubsetOfACompactSpaceIsCompact} Let $(X,\curlyo_{X})$ be a compact topological space, and let $A$ be a closed subset of $X$. Then $(A,\curlyo_{A})$ is compact. Here $\curlyo_{A}$ is the subspace topology on $A$ with respect to $(X,\curlyo_{X})$.  \end{prpn}

 \begin{lem} \label{LemmaSeparationOfAPointAndACompactSubset} Let $(X,\curlyo_{X})$ be a Hausdorff topological space. Let $A$ be a compact subset of $X$. Suppose that $x \in X \setminus A$. There is a pair of open subsets $U$ and $U'$ of $X$ such that:

\begin{itemize}

\item[(1)] $A \subset U$,

\item[(2)] $U'$ is a neighbourhood of $x$,

\item[(3)] $U \cap U' = \emptyset$.
\end{itemize}
\end{lem}

\begin{prpn} \label{PropositionCompactSubsetOfAHausdorffSpaceIsClosed} Let $(X,\curlyo_{X})$ be a Hausdorff topological space. Let $A$ be a compact subset of $X$. Then $A$ is closed in $(X,\curlyo_{X})$.  \end{prpn}

\begin{proof} Let $x \in X \setminus A$. By Lemma \ref{LemmaSeparationOfAPointAndACompactSubset} there is a pair of open subsets $U$ and $U'$ of $X$ such that:

\begin{itemize}

\item[(1)] $A \subset U$,

\item[(2)] $U'$ is a neighbourhood of $x$,

\item[(3)] $U \cap U' = \emptyset$.
 
\end{itemize} %
%
In particular $U' \cap A \subset U' \cap U = \emptyset$. Thus $x$ is not a limit point of $A$ in $(X,\curlyo_{X})$.

We deduce that $\overline{A}=A$. By Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure} we conclude that $A$ is closed in $X$.
\end{proof} 

\begin{rmk} \label{RemarkCompactSubsetNotNecessarilyClosedIfNotHausdorff} Proposition \ref{PropositionCompactSubsetOfAHausdorffSpaceIsClosed} does not necessarily hold if $(X,\curlyo_{X})$ is not Hausdorff. Here are two examples.

\begin{itemize}

\item[(1)] Let $X = \{ 0,1 \}$ be equipped with the topology $\curlyo = \{ \emptyset, \{1\},X \}$. In other words, $(X,\curlyo)$ is the Sierpi{\'n}ski interval. 

By Examples \ref{ExamplesT1Spaces} (1) a finite topological space is T$1$ if and only if its topology is the discrete topology. Since $\curlyo$ is not the discrete topology on $X$, the Sierpi{\'n}ski interval is not T$1$ and hence not Hausdorff. 

Since $X$ is finite every subset of $X$ is compact. In particular $\{1\}$ is a compact subset of $(X,\curlyo)$. But as we observed in Examples \ref{ExamplesLimitPoint} (1) the set $\{1 \}$ is not closed in $(X,\curlyo)$.

\item[(2)] Let $\big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big)$ be the real line with two origins of Example \ref{ExampleQuotientOfHausdorffSpaceIsNotNecessarilyHausdorff}. As in Example \ref{ExampleQuotientOfHausdorffSpaceIsNotNecessarilyHausdorff} to avoid confusion we adopt the notation $\left]a,b\right[$ for the open interval from $a$ to $b$. Similarly we denote by $\left] a,b \right]$ the half open interval from $a$ to $b$.

Let \ar{\reals \sqcup \reals, {(\reals \sqcup \reals)/ \sim},\pi} denote the quotient map. 

Let \ar{I,\reals \sqcup \reals,i} denote the map $t \mapsto (t,0)$. Here as usual we think of $\reals \sqcup \reals$ as \[ \big( \reals \times \{0\} \big) \cup \big( \reals \times \{1\} \big). \] %
%
We have that $i$ is the composition of the following two maps. 

\begin{itemize}

\item[(1)] The inclusion map \ar{I,{\reals,}} which is continuous by Proposition \ref{PropositionInclusionIsContinuous}

\item[(2)] The map \ar{\reals,\reals \sqcup \reals} given by $x \mapsto (x,0)$, which is continuous by Observation \ref{ObservationCanonicalMapsIntoCoproductAreContinuous}. 

\end{itemize} %
%
We deduce by Proposition \ref{PropositionCompositionOfContinuousMapsIsContinuous} that $i$ is continuous. Thus by Proposition \ref{PropositionUnitIntervalIsCompact} and Proposition \ref{PropositionContinuousImageOfACompactSpaceIsCompact} we have that $\pi\big(i(I)\big)$ is a compact subset of \[ \big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big). \] %
%
We claim that $\pi\big(i(I)\big)$ is not a closed subset of $\big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big)$. 

Let us prove that $\pi\big((0,1)\big)$ is a limit point of $\pi\big(i(I)\big)$ in $\big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big)$. Let $U$ be a neighbourhood of $\pi\big((0,1)\big)$ in $\big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big)$. Then $\pi^{-1}(U)$ is a neighbourhood of $(0,1)$ in $\reals \sqcup \reals$.  

As in Example \ref{ExampleQuotientOfHausdorffSpaceIsNotNecessarilyHausdorff} we have that \[ \big\{ \left]a,b\right[ \times\{ 0 \} \mid a,b \in \reals \big\} \cup \big\{  \left]a,b\right[ \times \{1\} \mid a,b \in \reals \big\} \] is a basis for $(\reals \sqcup \reals, \curlyo_{\reals \sqcup \reals})$. By Question 3 (a) of Exercise Sheet 2 there are $a,b \in \reals$ such that $0 \in \left]a,b\right[$ and $\left] a,b \right[ \times \{1\} \subset \pi^{-1}(U)$. 

We have that $\pi^{-1}\Big( \pi\big(i(I)\big) \Big) = \big( I \times \{ 0 \} \big) \times \big( \left]0,1\right] \times \{1\} \big)$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (6,0);

\node at (7,0) {$\reals \times \{1\}$};

\draw[yellow] (0,-2) -- (6,-2);

\node at (7,-2) {$\reals \times \{0\}$};

\draw[green] (1.5,-1.7) node {$[$} to node[auto] {$I \times \{0\}$} (4.5,-1.7) node {$]$};
\draw[green] (1.5,0.3) node {$($} to node[auto] {$\left]0,1\right] \times \{1\}$} (4.5,0.3) node {$]$};
\draw[purple] (0.5,1.2) node {$($} to node[auto] {$\left] a,b \right[ \times \{1\}$} (2.5,1.2) node {$)$};


\draw (1.5,0) -- (1.5,-0.15);
\node at (1.5,-0.4) {$(0,1)$};
\draw (4.5,0) -- (4.5,-0.15);
\node at (4.5,-0.4) {$(1,1)$};


\draw (1.5,-2) -- (1.5,-0.-2.15);
\node at (1.5,-2.4) {$(0,0)$};
\draw (4.5,-2) -- (4.5,-2.15);
\node at (4.5,-2.4) {$(1,0)$};

\end{tikzpicture}

\end{figuretikz} 

Then \[ \left] a,b \right[ \cap \left] 0,1 \right] = \left] 0,b \right[ \not= \emptyset. \] %
%
Hence \[ \big(\left] a,b \right[ \times \{1\}\big) \cap \big( \left] 0,1 \right] \times \{1\} \big) \not= \emptyset. \] Thus \[ \pi^{-1}\Big( U \cap \pi\big(i(I)\big) \Big) = \pi^{-1}(U) \cap \pi^{-1}\Big(\pi\big(i(I)\big)\Big) \not= \emptyset. \] We deduce that \[ U \cap \pi\big(i(I)\big) \not= \emptyset. \] This completes our proof that $\pi\big((0,1)\big)$ is a limit point of $\pi\big(i(I)\big)$ in \[\big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big). \] 

But $\pi\big((0,1)\big) \not \in \pi\big(i(I)\big)$. Thus $\pi\big(i(I)\big)$ is not closed in $\big( (\reals \sqcup \reals)/\sim, \curlyo_{(\reals \sqcup \reals)/\sim} \big)$. 

\end{itemize}
\end{rmk}

\begin{prpn} \label{PropositionHomeomorphismIffContinuousAndBijective} Let $(X,\curlyo_{X})$ be a compact topological space. Let $(Y,\curlyo_{Y})$ be a Hausdorff topological space. A map \ar{X,Y,f} is a homeomorphism if and only if $f$ is continuous and bijective. \end{prpn}
 
\begin{proof} If $f$ is a homeomorphism then by Proposition \ref{PropositionEquivalentCharacterisationOfAHomeomorphism} we have that $f$ is continuous and bijective. 

Suppose instead that $f$ is continuous and bijective. That $f$ is bijective implies that $x \mapsto f^{-1}(x)$ gives a well defined map \ar{Y,{X.},g} We have that $g$ is inverse to $f$. To prove that $f$ is a homeomorphism we shall prove that $g$ is continuous.

By Question 1 (a) of Exercise Sheet 3 we have that $g$ is continuous if and only if $g^{-1}(A)$ is a closed subset of $Y$ for any closed subset $A$ of $X$. By definition of $g$ we have that $g^{-1}(A) = f(A)$. Thus it suffices to prove that if $A$ is a closed subset of $X$ then $f(A)$ is a closed subset of $Y$.

Suppose that $A$ ia a closed subset of $X$. Then since $(X,\curlyo_{X})$ is compact we have by Proposition \ref{PropositionClosedSubsetOfACompactSpaceIsCompact} that $A$ is a compact subset of $X$. Thus by Proposition \ref{PropositionContinuousImageOfACompactSpaceIsCompact} we have that $f(A)$ is a compact subset of $Y$. Since $(Y,\curlyo_{Y})$ is Hausdorff we deduce by Proposition \ref{PropositionCompactSubsetOfAHausdorffSpaceIsClosed} that $f(A)$ is closed in $(Y,\curlyo_{Y})$ as required.

\end{proof}

\begin{prpn} \label{PropositionQuotientOfACompactSpaceIsCompact} Let $(X,\curlyo_{X})$ be a compact topological space and let $\sim$ be an equivalence relation on $X$. Then $(X / \sim, \curlyo_{X/\sim})$ is compact. \end{prpn} 

\begin{proof} The quotient map \ar{X,{X/\sim},\pi} is continuous and surjective. We deduce that $(X/\sim,\curlyo_{X/\sim})$ is compact by Proposition \ref{PropositionContinuousImageOfACompactSpaceIsCompact}. \end{proof}

\begin{example} As in Examples \ref{ExamplesQuotientSpaces} (1) let $\sim$ be the equivalence relation on $I$ generated by $0 \sim 1$. Let \ar{I,S^{1},\phi} be the continuous map constructed in Question 9 of Exercise Sheet 3. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw (0,1.75) circle[radius=0.8];

\draw[green,->] (0,2.65) arc (90:-265:0.9);

\end{tikzpicture}

\end{figuretikz} %
%
We have that $\phi(0) = \phi(1)$. Thus we obtain a map \ar{{I/\sim},S^{1},f} given by $[t] \mapsto \phi(t)$. By Question 11 (a) of Exercise Sheet 4 we have that $f$ is continuous. Moreover $f$ is a bijection.  

\begin{itemize}

\item[(1)] By Examples \ref{ExamplesHausdorffness} we have that $(\reals,\curlyo_{\reals})$ is Hausdorff. Thus by Proposition \ref{PropositionProductHausdorffSpacesIsHausdorff} we have that $(\reals^{2},\curlyo_{\reals^{2}})$ is Hausdorff. By Proposition \ref{PropositionSubspaceHausdorffSpaceIsHausdorff} we deduce that $(S^{1},\curlyo_{S^{1}})$ is Hausdorff.

\item[(2)] By Proposition \ref{PropositionUnitIntervalIsCompact} we have that $(I,\curlyo_{I})$ is compact. Thus by Proposition \ref{PropositionQuotientOfACompactSpaceIsCompact} we have that $(I/\sim,\curlyo_{I/\sim})$ is compact.  

\end{itemize} %
%
We conclude by Proposition \ref{PropositionHomeomorphismIffContinuousAndBijective} that $f$ is a homeomorphism. This gives a rigorous affirmative answer to Question \ref{QuestionCircleHomeomorphicToQuotientOfUnitInterval}. 

\end{example}

\section{A product of compact topological spaces is compact}

\begin{prpn} \label{PropositionTubeLemma} Let $(X,\curlyo_{X})$ be a topological space and let $(X',\curlyo_{X'})$ be a compact topological space. Let $x \in X$ and let $W$ be a subset of $X \times X'$ satisfying the following conditions.

\begin{itemize}

\item[(1)] $W \in \curlyo_{X \times X'}$.

\item[(2)] $\{x\} \times X' \subset W$. 

\end{itemize} %
%
Then there is a neighbourhood $U$ of $x$ in $(X,\curlyo_{X})$ such that $U \times X' \subset W$. 
 \end{prpn}

\begin{proof} Let $x' \in X'$. By (2) we have that $(x,x') \in W$. By (1) and the definition of $\curlyo_{X \times X'}$ we deduce that there is a neighbourhood $U_{x'}$ of $x$ in $(X,\curlyo_{X})$ and a neighbourhood $U'_{x'}$ of $x'$ in $(X',\curlyo_{X'})$ such that $U_{x'} \times U'_{x'} \subset W$. We have that \begin{align*} X' &= \bigcup_{x' \in X'} \{ x' \} \\ &\subset \bigcup_{x' \in X'} U'_{x'}. \end{align*} Thus $X=\bigcup_{x' \in X'} U'_{x'}$ and we have that $\{ U'_{x'} \}_{x' \in X}$ is an open covering of $X'$. 

Since $(X',\curlyo_{X'})$ is compact there is a finite subset $J$ of $X'$ such that $\{ U'_{x'} \}_{x' \in J}$ is an open covering of $X'$. Let $U = \bigcap_{x' \in J} U_{x'}$. We make the following observations.

\begin{itemize}

\item[(1)]  Since $J$ is finite we have that $U \in \curlyo_{X}$. 

\item[(2)] Since $x \in U_{x'}$ for all $x' \in X'$, we in particular have that $x \in U_{x'}$ for all $x' \in J$. Thus $x \in U$.

\item[(3)] For any $x' \in J$ we have that $U \times U'_{x'} \subset U_{x'} \times U'_{x'} \subset W$. Thus we have that \begin{align*} U \times X' &= U \times \Big( \bigcup_{x' \in J} U'_{x'} \Big) \\ &= \bigcup_{x' \in J} \big(U \times U'_{x'}\big) \\ &\subset \bigcup_{x' \in J} W \\&= W. \end{align*}

\end{itemize} 
\end{proof}

\begin{rmk} Proposition \ref{PropositionTubeLemma} is sometimes known as the {\em tube lemma}. It does not necessarily hold if $X'$ is not compact. Let us illustrate this by an example.

\begin{itemize}

\item[(1)] Let $(X,\curlyo_{X}) = (\reals,\curlyo_{\reals})$.

\item[(2)] Let $(X',\curlyo_{X'}) = (\reals,\curlyo_{\reals})$.

\item[(3)] Let $x=0$. 

\item[(4)] Let $W = \big\{ (x,y) \in \reals^{2} \mid \text{$x \not= 0$ and $y < \left| \frac{1}{x} \right|$} \big\} \cup \big\{ (0,y) \mid y \in \reals \big\}$. 

\end{itemize} %
%
In the picture below $W$ is the shaded green area. The two dashed green curves do not themselves belong to $W$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[green,dashed] (3,0.1) to [looseness=1,out=175,in=-85] (0.1,3);
\draw[green,dashed] (-3,0.1) to [looseness=1,out=5,in=-95] (-0.1,3);

\draw[green] (-0.05,3) -- (0.05,3);
\draw[green] (-0.1,2.75) -- (0.1,2.75);
\draw[green] (-0.15,2.5) -- (0.15,2.5);
\draw[green] (-0.225,2.25) -- (0.225,2.25);
\draw[green] (-0.3,2) -- (0.3,2);
\draw[green] (-0.43,1.75) -- (0.43,1.75);
\draw[green] (-0.525,1.5) -- (0.525,1.5);
\draw[green] (-0.7,1.25) -- (0.7,1.25);
\draw[green] (-0.975,1) -- (0.975,1);
\draw[green] (-1.2,0.75) -- (1.2,0.75);
\draw[green] (-1.6,0.5) -- (1.6,0.5);
\draw[green] (-2.1,0.25) -- (2.1,0.25);
\draw[green] (-3,-0.25) -- (3,-0.25);
\draw[green] (-3,-0.5) -- (3,-0.5);
\draw[green] (-3,-0.75) -- (3,-0.75);
\draw[green] (-3,-1) -- (3,-1);
\draw[green] (-3,-1.25) -- (3,-1.25);

\node[anchor=north west,fill=white] at (0,0) {$(0,0)$};

\draw[yellow] (0,-1.25) -- (0,3);
\draw[yellow] (-3,0) -- (3,0);

\fill (0,0) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %
%
Then $W$ is a neighbourhood of $\{x\} \times \reals = \{0\} \times \reals$ in $(X \times X',\curlyo_{X \times X'}) = (\reals^{2},\curlyo_{\reals^{2}})$. The set $\{0\} \times \reals$, or in other words the $y$-axis, is depicted in yellow below.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[green,dashed] (3,0.1) to [looseness=1,out=175,in=-85] (0.1,3);
\draw[green,dashed] (-3,0.1) to [looseness=1,out=5,in=-95] (-0.1,3);

\draw[green] (-0.05,3) -- (0.05,3);
\draw[green] (-0.1,2.75) -- (0.1,2.75);
\draw[green] (-0.15,2.5) -- (0.15,2.5);
\draw[green] (-0.225,2.25) -- (0.225,2.25);
\draw[green] (-0.3,2) -- (0.3,2);
\draw[green] (-0.43,1.75) -- (0.43,1.75);
\draw[green] (-0.525,1.5) -- (0.525,1.5);
\draw[green] (-0.7,1.25) -- (0.7,1.25);
\draw[green] (-0.975,1) -- (0.975,1);
\draw[green] (-1.2,0.75) -- (1.2,0.75);
\draw[green] (-1.6,0.5) -- (1.6,0.5);
\draw[green] (-2.1,0.25) -- (2.1,0.25);
\draw[green] (-3,-0.25) -- (3,-0.25);
\draw[green] (-3,-0.5) -- (3,-0.5);
\draw[green] (-3,-0.75) -- (3,-0.75);
\draw[green] (-3,-1) -- (3,-1);
\draw[green] (-3,-1.25) -- (3,-1.25);
\draw[green] (-3,0) -- (3,0);

\draw[yellow] (0,-1.25) -- (0,3);

\end{tikzpicture}

\end{figuretikz} %
%
There is no neighbourhood $U$ of $x$ in $(\reals,\curlyo_{\reals})$ such that $U \times \reals \subset W$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[green,dashed] (3,0.1) to [looseness=1,out=175,in=-85] (0.1,3);
\draw[green,dashed] (-3,0.1) to [looseness=1,out=5,in=-95] (-0.1,3);

\draw[green] (-0.05,3) -- (0.05,3);
\draw[green] (-0.1,2.75) -- (0.1,2.75);
\draw[green] (-0.15,2.5) -- (0.15,2.5);
\draw[green] (-0.225,2.25) -- (0.225,2.25);
\draw[green] (-0.3,2) -- (0.3,2);
\draw[green] (-0.43,1.75) -- (0.43,1.75);
\draw[green] (-0.525,1.5) -- (0.525,1.5);
\draw[green] (-0.7,1.25) -- (0.7,1.25);
\draw[green] (-0.975,1) -- (0.975,1);
\draw[green] (-1.2,0.75) -- (1.2,0.75);
\draw[green] (-1.6,0.5) -- (1.6,0.5);
\draw[green] (-2.1,0.25) -- (2.1,0.25);
\draw[green] (-3,-0.25) -- (3,-0.25);
\draw[green] (-3,-0.5) -- (3,-0.5);
\draw[green] (-3,-0.75) -- (3,-0.75);
\draw[green] (-3,-1) -- (3,-1);
\draw[green] (-3,-1.25) -- (3,-1.25);
\draw[green] (-3,0) -- (3,0);

\draw[yellow] (0,-1.25) -- (0,3);

\draw[blue,dashed] (-0.35,-1.25) rectangle (0.35,3);

\node[blue] at (0.6,3.25) {$U$};
\end{tikzpicture}

\end{figuretikz} %
%
This is due to the `infinitesimal narrowing' of $W$. Proposition \ref{PropositionTubeLemma} establishes that this kind of behaviour cannot occur if $(X',\curlyo_{X'})$ is compact. For instance, suppose that we instead let $(X',\curlyo_{X'}) = (I,\curlyo_{I})$. By Proposition \ref{PropositionUnitIntervalIsCompact} we have that $(I,\curlyo_{I})$ is compact. The restriction of $W$ to $\reals \times I$ is pictured below.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\begin{scope}[xscale=3]

\draw[red] (-1.9,0.25) -- (1.9,0.25) node[black,anchor=west] {$\reals \times \{0\}$};
\draw[red] (-1.9,3.1) -- (1.9,3.1) node[black,anchor=west] {$\reals \times \{1\}$};

\begin{scope}

\clip (-2,0.4) rectangle (2,3.1);

\draw[green,dashed] (3,0.1) to [looseness=1,out=175,in=-85] (0.1,3);
\draw[green,dashed] (-3,0.1) to [looseness=1,out=5,in=-95] (-0.1,3);

\end{scope}

\draw[green] (-0.05,3) -- (0.05,3);
\draw[green] (-0.1,2.75) -- (0.1,2.75);
\draw[green] (-0.15,2.5) -- (0.15,2.5);
\draw[green] (-0.225,2.25) -- (0.225,2.25);
\draw[green] (-0.3,2) -- (0.3,2);
\draw[green] (-0.43,1.75) -- (0.43,1.75);
\draw[green] (-0.525,1.5) -- (0.525,1.5);
\draw[green] (-0.7,1.25) -- (0.7,1.25);
\draw[green] (-0.975,1) -- (0.975,1);
\draw[green] (-1.2,0.75) -- (1.2,0.75);
\draw[green] (-1.6,0.5) -- (1.6,0.5);

\draw[yellow] (0,0.25) -- (0,3.1);

\end{scope}

\fill (0,0.25)   circle[radius=0.05]; 
\fill (-0.3,3.1) circle[radius=0.05];
\fill (0.3,3.1)  circle[radius=0.05];

\node[anchor=north]      at (0,0.25)  {$(0,0)$};
\node[anchor=south west] at (0.1,3.1) {$(1,1)$};
\node[anchor=south east] at (-0.1,3.1) {$(-1,1)$};

\end{tikzpicture}

\end{figuretikz} %
%
We can find a neighbourhood $U$ of $\{0\} \times \reals$ such that $U \subset W$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\begin{scope}[xscale=3]

\draw[red]  (-1.9,0.25)  -- (-0.04,0.25);
\draw[red]  (0.04,0.25)  -- (1.9,0.25) node[black,anchor=west] {$\reals \times \{0\}$};
\draw[red]  (-1.9,3.1)   -- (-0.04,3.1);
\draw[red]  (0.04,3.1)   -- (1.9,3.1) node[black,anchor=west] {$\reals \times \{1\}$};
\draw[blue] (-0.04,0.25) -- (0.04,0.25);
\draw[blue] (-0.04,3.1)  -- (0.04,3.1);

\begin{scope}

\clip (-2,0.4) rectangle (2,3.1);

\draw[green,dashed] (3,0.1) to [looseness=1,out=175,in=-85] (0.1,3);
\draw[green,dashed] (-3,0.1) to [looseness=1,out=5,in=-95] (-0.1,3);

\end{scope}

\draw[green] (-0.05,3) -- (0.05,3);
\draw[green] (-0.1,2.75) -- (0.1,2.75);
\draw[green] (-0.15,2.5) -- (0.15,2.5);
\draw[green] (-0.225,2.25) -- (0.225,2.25);
\draw[green] (-0.3,2) -- (0.3,2);
\draw[green] (-0.43,1.75) -- (0.43,1.75);
\draw[green] (-0.525,1.5) -- (0.525,1.5);
\draw[green] (-0.7,1.25) -- (0.7,1.25);
\draw[green] (-0.975,1) -- (0.975,1);
\draw[green] (-1.2,0.75) -- (1.2,0.75);
\draw[green] (-1.6,0.5) -- (1.6,0.5);

\draw[yellow] (0,0.25) -- (0,3.1);

\end{scope}

\draw[dashed,blue] (-0.12,0.25) -- (-0.12,3.1)
                   (0.12,0.25)  -- (0.12,3.1);

\node[blue] at (0.25,3.4) {$U$};

\end{tikzpicture}

\end{figuretikz}

\end{rmk}

\begin{rmk} The role of compactness in the proof of Proposition \ref{PropositionTubeLemma} is very similar to its role in the proof of Lemma \ref{LemmaSeparationOfAPointAndACompactSubset}, which was discussed in Remark \ref{RemarkTypicalAppealToCompactness}. The key to the proof is observation (1), that if $J$ is finite then $U \in \curlyo_{X}$. 

The idea of the proof of Proposition \ref{PropositionTubeLemma} is that since $(X',\curlyo_{X'})$ is compact we can find a finite set of points $x' \in X'$ and a neighbourhood $U_{x'} \times U'_{x'} \subset W$ of $(x,x')$ for each of these points $x'$ such that $\{ x \} \times X'$ is contained in the union of the sets $U_{x'} \times U'_{x'}$. 

An example is depicted below when we have the following.

\begin{itemize}

\item[(1)] $(X,\curlyo_{X})=(\reals,\curlyo_{\reals})$,

\item[(2)] $(X',\curlyo_{X'})=(I,\curlyo_{I})$,

\item[(3)] $x=0$. 

\end{itemize} %
%
A set $W$ is not drawn, but should be thought of as an open subset of $\reals \times I$ which contains all the green rectangles. To avoid cluttering the picture, possible locations for the points $(0,x'_{1}),\ldots,(0,x'_{5})$ are indicated seperately to the right.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (0,6);

\draw[green,dashed] (-1,0)   -- (-1,1.5)
                    (-1,1.5) -- (3,1.5)
                    (3,0)    -- (3,1.5); 
\draw[green]        (-1,0)   -- (3,0);

\draw[green,dashed] (-0.5,0.75) rectangle (2,3);
\draw[green,dashed] (-2,2.5) rectangle (0.5,3.75);
\draw[green,dashed] (-0.25,2) rectangle (1.5,5);

\draw[green,dashed] (-0.75,4.5) -- (1,4.5)
                    (-0.75,4.5) -- (-0.75,6)
                    (1,4.5)     -- (1,6);
\draw[green]        (-0.75,6)   -- (1,6);



\node[anchor=west] at (3.1,0.75) {$U_{x'_{1}} \times U'_{x'_{1}}$};
\node[anchor=west] at (2.1,2) {$U_{x'_{2}} \times U'_{x'_{2}}$};
\node[anchor=east] at (-2.1,3.125) {$U_{x'_{3}} \times U'_{x'_{3}}$};
\node[anchor=west] at (1.6,3.75) {$U_{x'_{4}} \times U'_{x'_{4}}$};
\node[anchor=east] at (-0.85,5.25) {$U_{x'_{5}} \times U'_{x'_{5}}$};

\draw[yellow] (8,0) -- (8,6);
\fill (8,0.35) circle[radius=0.05] node[anchor=west] {$(0,x_{1}')$};
\fill (8,1.7)  circle[radius=0.05] node[anchor=west] {$(0,x_{2}')$};
\fill (8,3.1) circle[radius=0.05] node[anchor=west] {$(0,x_{3}')$};
\fill (8,3.9)  circle[radius=0.05] node[anchor=west] {$(0,x_{4}')$};
\fill (8,5.2)  circle[radius=0.05] node[anchor=west] {$(0,x_{5}')$};

\end{tikzpicture}

\end{figuretikz} 

The open set $U$ in the proof of Proposition \ref{PropositionTubeLemma} is the intersection of all the neighbourhoods $U_{x'} \times U'_{x'}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (0,6);

\draw[green,dashed] (-1,0)   -- (-1,1.5)
                    (-1,1.5) -- (3,1.5)
                    (3,0)    -- (3,1.5); 

\draw[green]        (-1,0)   -- (-0.25,0)
                    (0.5,0)   -- (3,0);

\draw[green,dashed] (-0.5,0.75) rectangle (2,3);
\draw[green,dashed] (-2,2.5) rectangle (0.5,3.75);
\draw[green,dashed] (-0.25,2) rectangle (1.5,5);

\draw[green,dashed] (-0.75,4.5) -- (1,4.5)
                    (-0.75,4.5) -- (-0.75,6)
                    (1,4.5)     -- (1,6);
\draw[green]        (-0.75,6)   -- (-0.25,6)
                    (0.5,6)     -- (1,6);

\draw[blue]        (-0.25,0) -- (0.5,0);
\draw[blue]        (-0.25,6) -- (0.5,6);
\draw[blue,dashed] (-0.25,0) -- (-0.25,6);
\draw[blue,dashed] (0.5,0) -- (0.5,6);

\node[blue,anchor=south] at (0.25,6.05) {$U$};

\end{tikzpicture}

\end{figuretikz} 

\end{rmk}

\begin{prpn} \label{PropositionProductOfCompactSpacesIsCompact} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be compact topological spaces. Then the topological space $(X \times Y,\curlyo_{X \times Y})$ is compact.  \end{prpn}

\begin{proof} Let $\{ W_{j} \}_{j \in J}$ be an open covering of $X \times Y$. For any $x \in X$, let $\{ x \} \times Y$ be equipped with its subspace topology $\curlyo_{\{x\} \times Y}$ with respect to $(X \times Y,\curlyo_{X \times Y})$. Then \[ \Big\{ W_{j} \cap \big( \{x\} \times Y \big) \Big\}_{j \in J} \] is an open covering of $\{x\} \times Y$. 

By Lemma \ref{LemmaHomeomorphismBetweenATopologicalSpaceAndAProductOfItWithASingletonSet} we have that $\big(\{x\} \times Y, \curlyo_{\{x\} \times Y}\big)$ is homeomorphic to $(Y,\curlyo_{Y})$. Since $Y$ is compact we have by Corollary \ref{CorollaryHomeomorphismPreservesCompactness} that $\big( \{x\} \times Y, \curlyo_{\{x\} \times Y}\big)$ is compact. We deduce that there is a finite subset $J_{x}$ of $J$ such that \[ \Big\{ W_{j} \cap \big( \{ x \} \times Y \big) \Big\}_{j \in J_{x}} \] is an open covering of $\{x\} \times Y$. 

Let $W_{x} = \bigcup_{j \in J_{x}} W_{j}$. Then \begin{align*} \{x\} \times Y \subset W_{x} &= \bigcup_{j \in J_{x}} \Big( W_{j} \cap \big(\{x\} \times Y\big) \Big) \\ &\subset \bigcup_{j \in J_{x}} W_{j} \\ &= W_{x}. \end{align*} Since $Y$ is compact we deduce by Proposition \ref{PropositionTubeLemma} that there is a neighbourhood $U_{x}$ of $x$ in $(X,\curlyo_{X})$ such that $U_{x} \times Y \subset W_{x}$. We have that \begin{align*} X &= \bigcup_{x \in X} \{x\} \\ &\subset \bigcup_{x \in X} U_{x}. \end{align*} %
%
Hence $X=\bigcup_{x \in X} U_{x}$. Thus $\{ U_{x} \}_{x \in X}$ is an open covering of $X$. Since $(X,\curlyo_{X})$ is compact we deduce that there is a finite subset $K$ of $X$ such that $\{ U_{x} \}_{x \in K}$ is an open covering of $X$. 

Let $J'=\bigcup_{x \in K} J_{x}$. We make the following observations.

\begin{itemize}

\item[(1)] We have that $J_{x}$ is finite for every $x \in X$. In particular, $J_{x}$ is finite for every $x \in K$. Thus $J'$ is finite.

\item[(2)] We have that \begin{align*} X \times Y &= \Big( \bigcup_{x \in K} U_{x} \Big) \times Y \\ &= \bigcup_{x \in K} \big( U_{x} \times Y \big) \\ &\subset \bigcup_{x \in K} W_{x}. \end{align*} %
%
Hence $X \times Y = \bigcup_{x \in K} W_{x}$. We deduce that \begin{align*} X \times Y  &= \bigcup_{x \in K} W_{x} \\ &= \bigcup_{x \in K} \Big( \bigcup_{j \in J_{x}} W_{j} \Big) \\ &= \bigcup_{j \in J'} W_{j}. \end{align*} 

\end{itemize} %
%
We conclude that $\{ W_{j} \}_{j \in J'}$ is a finite subcovering of $\{ W_{j} \}_{j \in J}$.

\end{proof} 

\begin{examples} \label{ExamplesGeometricCompactSpaces} By Proposition \ref{PropositionProductOfCompactSpacesIsCompact} we have that $(I^{2},\curlyo_{I^{2}})$ is compact. Thus by Proposition \ref{PropositionQuotientOfACompactSpaceIsCompact} we have that all the topological spaces of Examples \ref{ExamplesQuotientSpaces} (1) -- (5) are compact. 

Moreover we have by Examples \ref{ExamplesHomeomorphisms} (3) that $D^{2} \cong I^{2}$. By Corollary \ref{CorollaryHomeomorphismPreservesCompactness} we deduce that $(D^{2},\curlyo_{D^{2}})$ is compact. Hence by Proposition \ref{PropositionQuotientOfACompactSpaceIsCompact} we have that the topological space $(S^{2},\curlyo_{S^{2}})$ constructed in Examples \ref{ExamplesQuotientSpaces} (6) is compact. \end{examples}

\section{Characterisation of compact subsets of $\reals^{n}$}

\begin{terminology} \label{TerminologyBounded} Let $A$ be a subset of $\reals^{n}$. Then $A$ is {\em bounded} if there is an $m \in \mathbb{N}$ such that \[ A \subset \underbrace{[-m,m] \times \ldots \times [-m,m]}_{n} \] where $[-m,m]$ denotes the closed interval in $\reals$ from $-m$ to $m$.   

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\begin{scope}[scale=1]
\draw plot [smooth cycle] coordinates {(0,0) (1,1/2)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1,-2) (-2,-5/2) (-5/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\begin{comment}
\draw[yellow] (-0.25,-3)    -- (-0.25,1)
              (-2.5,-1) -- (2,-1);
\end{comment}

\draw[green] (-2.5,-3) rectangle (2,1); 

\node at (1.3,0.7) {$A$};
\fill[green] (2,1) circle[radius=0.05] node[anchor=south west] {$(m,m)$};
\fill[green] (-2.5,1) circle[radius=0.05] node[anchor=south east] {$(-m,m)$}; 
\fill[green] (2,-3) circle[radius=0.05] node[anchor=north west] {$(m,-m)$};
\fill[green] (-2.5,-3) circle[radius=0.05] node[anchor=north east] {$(-m,-m)$}; 

\end{tikzpicture}

\end{figuretikz}

\end{terminology}

\begin{rmk} Roughly speaking a subset $A$ of $\reals^{n}$ is bounded if we can enclose it in a box. There are many ways to express this, all of which are equivalent to the definition of Terminology \ref{TerminologyBounded}. \end{rmk}

\begin{notn} Let $m \in \mathbb{N}$. We denote the subset \[ \underbrace{[-m,m] \times \ldots \times [-m,m]}_{n} \] of $\reals^{n}$ by $[-m,m]^{n}$. 

Let $(-m,m)$ denote the open interval from $-m$ to $m$ in $\reals$. We denote the subset \[ \underbrace{(-m,m) \times \ldots \times (-m,m)}_{n} \] of $\reals^{n}$ by $(-m,m)^{n}$. 
\end{notn}

\begin{prpn} \label{PropositionCompactSubsetRnIffClosedAndBounded} Let $A$ be a subset of $\reals^{n}$ equipped with its subspace topology $\curlyo_{A}$ with respect to $(\reals^{n},\curlyo_{\reals^{n}})$. Then $(A,\curlyo_{A})$ is compact if and only if $A$ is bounded and closed in $(\reals^{n},\curlyo_{\reals^{n}})$. \end{prpn}

\begin{proof} Suppose that $A$ is bounded and closed in $(\reals^{n},\curlyo_{\reals^{n}})$. Since $A$ is bounded there is an $m \in \mathbb{N}$ such that $A \subset [-m,m]^{n}$. Let $\curlyo_{[-m,m]^{n}}$ denote the subspace topology on $[-m,m]^{n}$ with respect to $(\reals^{n},\curlyo_{\reals^{n}})$. We make the following observations.

\begin{itemize}

\item[(1)] The subspace topology $\curlyo_{A}$ on $A$ with respect to $(X,\curlyo_{X})$ is equal to the subspace topology on $A$ with respect to $\big( [-m,m]^{n}, \curlyo_{[-m,m]^{n}} \big)$.

\item[(2)] By Corollary \ref{CorollaryCompactnessClosedIntervals} we have that $[-m,m]$ is a compact subset of $\reals$. By Proposition \ref{PropositionProductOfCompactSpacesIsCompact} and induction we deduce that $[-m,m]^{n}$ is a compact subset of $\reals^{n}$.

\item[(3)] Since $A$ is closed in $(\reals^{n},\curlyo_{\reals^{n}})$ we have by Question 1 (c) of Exercise Sheet 4 that $A$ is closed in $\big( [-m,m]^{n}, \curlyo_{[-m,m]^{n}} \big)$. 
\end{itemize} %
%
We deduce by Proposition \ref{PropositionClosedSubsetOfACompactSpaceIsCompact} that $(A,\curlyo_{A})$ is compact. 

Suppose instead now that $(A,\curlyo_{A})$ is compact. By Examples \ref{ExamplesHausdorffness} (1) and Proposition \ref{PropositionProductHausdorffSpacesIsHausdorff} we have that $(\reals^{n},\curlyo_{\reals^{n}})$ is Hausdorff. We deduce by Proposition \ref{PropositionCompactSubsetOfAHausdorffSpaceIsClosed} that $A$ is closed.

The set $\big\{ A \cap (-m,m)^{n} \big\}_{m \in \mathbb{N}}$ is an open covering of $A$. Thus since $(A,\curlyo_{A})$ is compact there is a finite subset $J$ of $\mathbb{N}$ such that $\big\{ A \cap (-m,m)^{n} \big\}_{m \in J}$ is an open covering of $A$. Let $m'$ be the largest natural number which belongs to $J$. Then \begin{align*} A &= \bigcup_{m \in J} \big( A \cap (-m,m)^{n} \big) \\ &\subset  \bigcup_{m \in J} (-m,m)^{n}  \\ &\subset \bigcup_{m \in J} (-m',m')^{n} \\ &= (-m',m')^{n}. \end{align*} Thus $A$ is bounded. \end{proof} 

\begin{cor} \label{CorollaryFunctionToRealsOnCompactSpaceAttainsItsBounds} Let $(X,\curlyo_{X})$ be a compact topological space. Let \ar{X,\reals,f} be a continuous map. There is an $x \in X$ such that $f(x) = \inf f(X)$ and an $x' \in X$ such that $f(x') = \sup f(X)$. Equivalently we have that \[ f(x) \leq f(x'') \leq f(x') \] for all $x'' \in X$. \end{cor}

\begin{proof} Since $(X,\curlyo_{X})$ is compact we have by Proposition \ref{PropositionContinuousImageOfACompactSpaceIsCompact} that $f(X)$ is a compact subset of $\reals$. By Proposition \ref{PropositionCompactSubsetRnIffClosedAndBounded} we deduce that $f(X)$ is closed in $(\reals,\curlyo_{\reals})$ and bounded. 

Since $f(X)$ is bounded we have that $\sup f(X) \in \reals$ and $\inf f(X) \in \reals$. Thus by Question 5 (a) and (b) on Exercise Sheet 4 we have that $\sup f(X)$ and $\inf f(X)$ are limit points of $f(X)$ in $(\reals,\curlyo_{\reals})$. 

Since $f(X)$ is closed in $(\reals,\curlyo_{\reals})$ we deduce by Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure} that $\sup f(X)$ belongs to $f(X)$ and that $\inf f(X)$ belongs to $f(X)$. \end{proof} 

\begin{rmk} You will have met Corollary \ref{CorollaryFunctionToRealsOnCompactSpaceAttainsItsBounds} when $X$ is a closed interval in $\reals$ in real analysis/calculus, where it is sometimes known as the `extreme value theorem'! \end{rmk}
