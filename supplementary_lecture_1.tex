\part{Advanced point-set topology}

\renewcommand{\thechapter}{S\arabic{supplement}}
\setcounter{supplement}{1}
\setcounter{chapter}{1001}

\chapter{Tuesday 4th March --- Supplementary Topics}

\section{Normal topological spaces}

\begin{defn} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. A {\em vicinage} of $A$ in $X$ with respect to $\curlyo_{X}$ is a subset $U$ of $X$ which belongs to $\curlyo_{X}$, and of which $A$ is a subset. \end{defn}

\begin{rmk} In other references, a vicinage is often  referred to as a {\em neighbourhood} of $A$. We shall make use of the latter terminology only in the sense of Definition \ref{DefinitionNeighbourhood}. Suppose that $x$ belongs to $X$. A subset $U$ of $X$ is a vicinage of $\{x\}$ in $X$ with respect to $\curlyo_{X}$ if and only if it is a neighbourhood of $x$ in $X$ with respect to $\curlyo_{X}$. \end{rmk}

\begin{defn} A topological space $(X,\curlyo_{X})$ is {\em normal} if, for every pair $A_{0}$ and $A_{1}$ of subsets of $X$, both of which are closed in $X$ with respect to $\curlyo_{X}$, and for which $A_{0} \cap A_{1}$ is empty, there is a vicinage $U_{0}$ of $A_{0}$ in $X$ with respect to $\curlyo_{X}$, and a vicinage $U_{1}$ of $A_{1}$ in $X$ with respect to $\curlyo_{X}$, such that $U_{0} \cap U_{1}$ is empty. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}[scale=1.6]
\draw plot [smooth cycle] coordinates {(0,0) (1,1/2)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1,-2) (-2,-5/2) (-5/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\begin{scope}[scale=0.58]
\draw[green, dashed, shift={(-1.8cm,-0.3cm)}] plot [smooth cycle] coordinates {(-1/4,0) (1,1/4)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1/4,-2) (-5/3,-5/2) (-4/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\begin{scope}[scale=0.25]
\draw[shift={(-4.25cm,-1.75cm)}] plot [smooth cycle] coordinates {(-1/4,0) (1,1/4)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1/4,-2) (-5/3,-5/2) (-4/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\node at (-1.8,-0.25) {$A_{0}$};

\begin{scope}[scale=0.25,rotate=10]
\draw[shift={(0.5cm,-9cm)}] plot [smooth cycle] coordinates {(-1/4,0) (1,1/4)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1/4,-2) (-5/3,-5/2) (-4/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\node at (-0.19,-2) {$A_{1}$};
\node at (1.75,1.2) {$X$};

\begin{scope}[scale=0.55,rotate=10]
\draw[purple, dashed, shift={(0.35cm,-3.4cm)}] plot [smooth cycle] coordinates {(-1/4,0) (1,1/4)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1/4,-2) (-5/3,-5/2) (-4/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\node[green] at (-2.15,-1.9) {$U_{0}$};
\node[purple] at (1.55,-3.3) {$U_{1}$};

\end{tikzpicture}

\end{figuretikz}

\end{defn}

\begin{prpn} Let $(X,\curlyo_{X})$ be a topological space. Then $(X,\curlyo_{X})$ is normal if and only if, for every subset $A$ of $X$ which is closed in $X$ with respect to $\curlyo_{X}$, and for every vicinage $U_{0}$ of $A$ in $X$ with respect to $\curlyo_{X}$, there is a vicinage $U_{1}$ of $A$ with the property that $\closure{U_{1}}{(X,\curlyo_{X})}$ is a subset of $U_{0}$. \end{prpn}

\begin{proof} Suppose that the condition holds. Let $A_{0}$ and $A_{1}$ be subsets of $X$, both of which are closed in $X$ with respect to $\curlyo_{X}$, and for which $A_{0} \cap A_{1}$ is empty. The following hold.

\begin{itemize}

\item[(1)] Since $A_{1}$ is closed in $X$ with respect to $\curlyo_{X}$, we have that $X \setminus A_{1}$ belongs to $\curlyo_{X}$. 

\item[(2)] Since $A_{0} \cap A_{1}$ is empty, we have that $A_{0}$ is a subset of $X \setminus A_{1}$. 

\end{itemize} %
%
By assumption, we deduce that there is a vicinage $U$ of $A_{0}$ in $X$ with respect to $\curlyo_{X}$ with the property that $\closure{U}{(X,\curlyo_{X})}$ is a subset of $X \setminus A_{1}$. Take $U_{0}$ to be $U$, and take $U_{1}$ to be $X \setminus \closure{U}{(X,\curlyo_{X})}$. The following hold.

\begin{itemize}

\item[(1)] By Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure}, we have that $\closure{U}{(X,\curlyo_{X})}$ is closed in $X$ with respect to $\curlyo_{X}$. Thus $U_{1}$ belongs to $\curlyo_{X}$. 

\item[(2)] Since $\closure{U}{(X,\curlyo_{X})}$ is a subset of $X \setminus A_{1}$, we have that that $A_{1}$ is a subset of $U_{1}$.  

\item[(3)] By Remark \ref{RemarkSubsetContainedInItsClosure}, we have that $U$ is a subset of $\closure{U}{(X,\curlyo_{X})}$. Thus $U_{1}$ is a subset of $X \setminus U$. Since $U \cap \left( X \setminus U \right)$ is empty, we deduce that $U \cap U_{1}$ is empty. In other words, we have that $U_{0} \cap U_{1}$ is empty.  

\end{itemize} %
%
We conclude that $(X,\curlyo_{X})$ is normal.

Conversely, suppose that $(X,\curlyo_{X})$ is normal. The following hold.

\begin{itemize}

\item[(1)] Since $U_{0}$ belongs to $\curlyo_{X}$, we have that $X \setminus U_{0}$ is closed in $X$ with respect to $\curlyo_{X}$. 

\item[(2)] Since $A$ is a subset of $U_{0}$, we have that $A \cap \left( X \setminus U_{0} \right)$ is a subset of $U_{0} \cap \left(X \setminus U_{0} \right)$. Since $U_{0} \cap \left(X \setminus U_{0} \right)$ is empty, we thus have that $A \cap \left( X \setminus U_{0} \right)$ is empty.

\end{itemize} %
%
Since $(X,\curlyo_{X})$ is normal, we deduce that there is a vicinage $W_{0}$ of $A$ in $X$ with respect to $\curlyo_{X}$, and a vicinage $W_{1}$ of $X \setminus U_{0}$ in $X$ with respect to $\curlyo_{X}$, such that $W_{0} \cap W_{1}$ is empty. Take $U_{1}$ to be $W_{0}$. We shall prove that $\closure{U_{1}}{(X,\curlyo_{X})}$ is a subset of $U_{0}$.

Suppose that $x$ belongs to $X \setminus U_{0}$. Then $W_{1}$ is a neighbourhood of $x$ in $X$ with respect to $\curlyo_{X}$. Since $U_{1} \cap W_{1}$ is empty, we deduce that $x$ is not a limit point of $U_{1}$ in $X$ with respect to $\curlyo_{X}$. We conclude that $\closure{W_{0}}{(X,\curlyo_{X})}$ is a subset of $U_{0}$, as required. 

\end{proof}

\section{Urysohn's lemma}

\begin{notn} Let $D$ denote the set given by \[ \left\{ \frac{k}{2^{n-1}} \mid \text{$n \in \mathbb{N}$, $k \in \mathbb{Z}$, and $0 \leq k \leq 2^{n-1}$} \right\}. \] \end{notn}

\begin{rmk} \label{RemarkDescriptionsOfDyadicRationals} Explicitly, $D$ is the subset of $\mathbb{Q}$ given by \[ \left\{ 0,1,\frac{1}{2},\frac{1}{4},\frac{3}{4},\frac{1}{8},\frac{3}{8},\frac{5}{8},\frac{7}{8}, \ldots \right\}. \] In other words, we have that $D$ is $\bigcup_{n \in \mathbb{N}} D_{n}$, where $D_{1}$ is the set $\{0,1\}$, and where $D_{n}$ is the set given by \[ \left\{ \frac{k}{2^{n-1}} \mid \text{$n \in \mathbb{N}$, $k \in \mathbb{Z}$, $k$ odd, $1 \leq k \leq 2^{n-1} - 1$} \right\}. \]   \end{rmk}

\begin{terminology} The set $D$ is known as the set of {\em dyadic rationals}. \end{terminology}

\begin{lem} Let $(X,\curlyo_{X})$ be a normal topological space. Let $A_{0}$ and $A_{1}$ be subsets of $X$ which are closed in $X$ with respect to $\curlyo_{X}$. Suppose that $A_{0} \cap A_{1}$ is empty. Then there is a set $\left\{ U_{d} \right\}_{d \in D}$ of subsets of $X$ such that the following hold.

\begin{itemize}

\item[(I)] We have that $U_{d}$ is a vicinage of $A_{0}$ in $X$ with respect to $\curlyo_{X}$,

\item[(II)] We have that $U_{d} \cap A_{1}$ is empty.

\item[(III)] Suppose that $n$ belongs to $\mathbb{N}$, that $k_{0}$ and $k_{1}$ belong to $\mathbb{Z}$, and that $0 \leq k_{0} < k_{1} \leq 2^{n-1}$. Let $U_{d_{0}}$ denote the vicinage correponding to \[ \frac{k_{0}}{2^{n-1}} \] and let $U_{d_{1}}$ denote the vicinage correponding \[ \frac{k_{1}}{2^{n-1}}. \] Then $\closure{U_{d_{0}}}{(X,\curlyo_{X})}$ is a subset of $U_{d_{1}}$.     

\end{itemize}    

\end{lem}

\begin{proof} We proceed inductively. Suppose first that $n=1$. The following hold.

\begin{itemize}

\item[(1)] We have that $A_{1}$ is closed in $X$ with respect to $\curlyo_{X}$. Thus $X \setminus A_{1}$ belongs to $\curlyo_{X}$. 

\item[(2)] Since $A_{0} \cap A_{1}$ is empty, we moreover have that $A_{0}$ is a subset of $X \setminus A_{1}$.

\end{itemize} %
%
Since $(X,\curlyo_{X})$ is normal, we deduce, by Proposition \ref{PropositionCharacterisationOfNormalTopologicalSpaces}, that there is a vicinage $U$ of $A_{0}$ in $X$ with respect to $\curlyo_{X}$ with the property that $\closure{U}{(X,\curlyo_{X})}$ is a subset of $X \setminus A_{1}$. We define $U_{0}$ to be $U$, and define $U_{1}$ to be $X \setminus A_{1}$. The following hold.

\begin{itemize}

\item[(1)] By Remark \ref{RemarkSubsetContainedInItsClosure}, we have that $U$ is a subset of $\closure{U}{(X,\curlyo_{X})}$. Thus $U \cap A_{1}$ is a subset of $\closure{U}{(X,\curlyo_{X})} \cap A_{1}$. Since $\closure{U}{(X,\curlyo_{X})}$ is a subset of $X \setminus A_{1}$, we have that $\closure{U}{(X,\curlyo_{X})} \cap A_{1}$ is empty. We deduce that $U \cap A_{1}$ is empty. Thus $U_{0} \cap A_{1}$ is empty.

\item[(2)] We have that $U_{1} \cap A_{1} = \left(X \setminus A_{1} \right) \cap A_{1}$ is empty.

\end{itemize} %
%
Thus $U_{0}$ and $U_{1}$ satisfy (II). That (I) and (III) hold are the defining properties of $U$.

Suppose now that we have defined $U_{d}$ for all $d$ which belong to $D_{m}$, for all $m < n$. Suppose that \[ d=\frac{k}{2^{n-1}} \] which belongs to $D_{n}$. Let $d_{k-1}$ be \[ \frac{k-1}{2^{n-1}} \] and let $d_{k+1}$ be \[ \frac{k+1}{2^{n-1}}. \] %
%
Since $k$ is odd, we have already defined both $U_{d_{k-1}}$ and $U_{d_{k+1}}$. Since $k-1 < k + 1$, we have by (III) that $\closure{U_{d_{k-1}}}{(X,\curlyo_{X})}$ is a subset of $U_{d_{k+1}}$. By Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure}, we have that $\closure{U_{d_{k-1}}}{(X,\curlyo_{X})}$ is closed in $X$ with respect to $\curlyo_{X}$. By Proposition \ref{PropositionCharacterisationOfNormalTopologicalSpaces}, we deduce that there is a vicinage $U$ of $\closure{U_{d_{k-1}}}{(X,\curlyo_{X})}$ such that $\closure{U}{(X,\curlyo_{X})}$ is a subset of $U_{d_{k+1}}$. We take $U_{d}$ to be $U$. The following hold.

\begin{itemize}

\item[(1)] By (I), we have that $A$ is a subset of $U_{d_{k-1}}$. By Remark \ref{RemarkSubsetContainedInItsClosure}, we have that $U_{d_{k-1}}$ is a subset of $\closure{U_{d_{k-1}}}{(X,\curlyo_{X})}$. We deduce that $A$ is a subset of $U$.

\item[(2)] By (II), we have that $U_{d_{k+1}} \cap A_{1}$ is empty. Since $U$ is a subset of $U_{d_{k+1}}$, we deduce that $U \cap A_{1}$ is empty.

\item[(3)] Suppose that $l$ belongs to $\mathbb{Z}$, and that $0 \leq l < k$. Let $U_{d_{l}}$ denote the vicinage corresponding to \[ \frac{l}{2^{n-1}}. \] If $l < k-1$, we have, by (III), that $\closure{U_{d_{l}}}{(X,\curlyo_{X})}$ is a subset of $U_{d_{k-1}}$. By Remark \ref{RemarkSubsetContainedInItsClosure}, we have that $U_{d_{k-1}}$ is a subset of $\closure{U_{d_{k-1}}}{(X,\curlyo_{X})}$. By definition of $U$, we have that $\closure{U_{d_{k-1}}}{(X,\curlyo_{X})}$ is a subset of $U$. For any $1 \leq l < k$, we conclude that $U_{d_{k-1}}$ is a subset of $U$. 

\item[(4)] Suppose that $l$ belongs to $\mathbb{Z}$, and that $k < l \leq 2^{n-1}$. If $l \geq k+1$, we have, by (III), that $\closure{U_{d_{k+1}}}{(X,\curlyo_{X})}$ is a subset of $U_{d_{l}}$. By definition of $U$, we have that $\closure{U}{(X,\curlyo_{X})}$ is a subset of $U_{d_{k+1}}$. By Remark \ref{RemarkSubsetContainedInItsClosure}, we have that $U_{d_{k+1}}$ is a subset of $\closure{U_{d_{k+1}}}{(X,\curlyo_{X})}$. For any $k < l \leq 2^{n-1}$, we conclude that $\closure{U}{(X,\curlyo_{X})}$ is a subset of $U_{d_{l}}$. 

\end{itemize} 

\end{proof}

\begin{prpn} \label{PropositionCharacterisationOfNormalTopologicalSpaces} Let $(X,\curlyo_{X})$ be a normal topological space. Let $A_{0}$ and $A_{1}$ be subsets of $X$ which are closed in $X$ with respect to $\curlyo_{X}$. Suppose that $A_{0} \cap A_{1}$ is empty. There is a continuous map \ar{X,I,f} such that $f(A_{0}) = \{0\}$, and such that $f(A_{1}) = \{1\}$.  \end{prpn}

\begin{proof} 
 



 \end{proof}

