\setcounter{chapter}{7}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Tuesday 28th January}

\section{Further geometric examples of homeomorphisms}

\begin{example} Let $K$ be a subset of $\reals^{3}$ such as the following. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth,scale=1.5]

\clip (-1,-0.75) rectangle (2,0.7);

\draw[name path=arc1] (0,0) to[looseness=4,out=-60,in=-10] (1,0); 

\path[shorten >= 0.25em,name path=arc2] (1,0) to[looseness=4,out=-120,in=190] (0,0); 

\fill[name intersections={of=arc1 and arc2},white] (intersection-1) circle[radius=0.05];

\draw[shorten >= 0.25em,shorten <= 0.25em, name path=arc2] (1,0) to[looseness=4,out=-120,in=190] (0,0); 

\draw[shorten >= 0.25em] (0,0) to[looseness=2.25,out=120,in=60] (1,0);
 
\draw[shorten <= 0.25em] (0,0) to [looseness=1,out=10,in=170] (1,0);

\end{tikzpicture}

\end{figuretikz} % 
%
Let $\curlyo_{K}$ denote the subspace topology on $K$ with respect to $(\reals^{3},\curlyo_{\reals^{3}})$. Then $(K,\curlyo_{K})$ is an example of a {\em knot}. We have that $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S}^{1})$. \end{example}

\begin{rmk} The crucial point is that both $K$ and a circle can be obtained from a piece of string by glueing together the ends together. We may bend, twist, and stretch the string as much as we wish before we glue the ends together. \end{rmk}

\begin{rmk} We shall explore knot theory later in the course. \end{rmk}

\begin{example} \label{ExampleTorusIsHomeomorphicToAProductOfTwoCircles} We have that $(T^{2},\curlyo_{T^{2}})$ is homeomorphic to $(S^{1} \times S^{1},\curlyo_{S^{1} \times S^{1}})$. 

\begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (-1.5,0.15) rectangle (1.5,-0.60);

\draw (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (0,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}

\end{tikzpicture}

\end{figuretikz} %
%
To prove this is the topic of Task \ref{TaskTorusIsHomeomorphicToAProductOfTwoCircles}. \end{example} 

\begin{rmk} We can think of the left copy of $S^{1}$ in $S^{1} \times S^{1}$ as the circle depicted below. 

\begin{figuretikz} 

\begin{tikzpicture}
 
\draw[yellow] (0,0) ellipse [x radius=1.5, y radius=0.25];

\end{tikzpicture}

\end{figuretikz} %
%
Suppose that $x$ belongs to $S^{1}$.
 
\begin{figuretikz} 

\begin{tikzpicture}

\draw[yellow] (0,0) ellipse [x radius=1.5, y radius=0.25];

\fill[green] (-0.1,-0.25) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} % 
%
We can think of $\{ x \} \times S^{1}$ as a circle around $x$. 

\begin{figuretikz} 

\begin{tikzpicture}

\draw[green] (0,-0.25) ellipse [x radius=0.1,y radius= 0.2];
\fill[white] (-0.1,-0.25) circle[radius=0.05];

\draw[yellow] (0,0) ellipse [x radius=1.5, y radius=0.25];

\end{tikzpicture}

\end{figuretikz} %
%
In this way, we can think $S^{1} \times S^{1} = \bigcup_{x \in S^{1}} \{ x \} \times S^{1}$ as a `circle of circles'. 

\begin{figuretikz} 

\begin{tikzpicture}

\draw[green] (0,-0.25) ellipse [x radius=0.1,y radius= 0.2]
      (0.5,-0.225) ellipse [x radius=0.1,y radius= 0.2]
      (1,-0.15) ellipse [x radius=0.1,y radius= 0.2]
      (-0.5,-0.225) ellipse [x radius=0.1,y radius= 0.2]
      (-1,-0.15) ellipse [x radius=0.1,y radius= 0.2];

\fill[white] (-0.1,-0.25) circle[radius=0.05]
             (0.4,-0.225) circle[radius=0.05]
             (0.9,-0.2) circle[radius=0.05]
             (-0.6,-0.225) circle[radius=0.05]
             (-1.1,-0.175) circle[radius=0.05];

\draw[yellow] (0,0) ellipse [x radius=1.5, y radius=0.25];

\end{tikzpicture}

\end{figuretikz} %
%
A `circle of circles' is intuitively exactly a torus.  

\begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (-1.5,0.15) rectangle (1.5,-0.60);

\draw (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (0,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}

\end{tikzpicture}

\end{figuretikz}

\end{rmk}

\section{Neighbourhoods}

\begin{defn} \label{DefinitionNeighbourhood} Let $(X,\curlyo_{X})$ be a topological space. Suppose that $x$ belongs to $X$. A {\em neighbourhood} of $x$ in $X$ with respect to $\curlyo_{X}$ is a subset $U$ of $X$ such that $x$ belongs to $U$, and such that $U$ belongs to $\curlyo_{X}$. \end{defn}

\begin{caution} In other references, you may see a neighbourhood $U$ of $x$ defined simply to be a subset of $X$ to which $x$ belongs, without the requirement that $U$ belongs to $\curlyo_{X}$. \end{caution}

\begin{example} Let $X = \{ a,b,c,d \}$ be a set with four elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a\}, \{b\}, \{d\}, \{a,b\}, \{a,d\}, \{b,d\}, \{c,d\}, \{a,b,d\}, \{a,c,d\}, \{b,c,d\}, X \right\}. \] %
%
Here is a list of the neighbourhoods in $X$ with respect to $\curlyo_{X}$ of the elements of $X$. 

\begin{center}
\begin{tabular}{ll}
\toprule
Element & Neighbourhoods \\
\midrule
$a$     & $\{a\}$, $\{a,b\}$, $\{a,d\}$, $\{a,b,d\}$, $\{a,c,d\}$, $X$ \\
$b$     & $\{b\}$, $\{a,b\}$, $\{b,d\}$, $\{a,b,d\}$, $\{b,c,d\}$, $X$ \\
$c$     & $\{c,d\}$, $\{a,c,d\}$, $\{b,c,d\}$, $X$ \\
$d$     & $\{d\}$, $\{a,d\}$, $\{b,d\}$, $\{c,d\}$, $\{a,b,d\}$, $\{a,c,d\}$, $\{b,c,d\}$, $X$ \\ 
\bottomrule
\end{tabular}
\end{center}
\end{example}

\begin{example} Suppose that $x$ belongs to $D^{2}$. For instance, we can take $x$ to be $\left(\frac{1}{4},\frac{1}{4}\right)$. 

\begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) circle[radius=1.5];

\fill (3/8,3/8) circle[radius=0.05];
\node at (3/8,0.075) {$x$};

\end{tikzpicture}

\end{figuretikz} %
%
A typical example of a neighbourhood of $x$ in $D^{2}$ with respect to $\curlyo_{D^{2}}$ is a subset $U$ of $D^{2}$ which is an `open rectangle', and to which $x$ belongs. When $x$ is $\left(\frac{1}{4},\frac{1}{4}\right)$, we can, for instance, take $U$ to be $\opint{0,\frac{1}{2}} \times \opint{0,\frac{1}{2}}$. 

\begin{figuretikz} 

\begin{tikzpicture}
   
\draw (0,0) circle[radius=1.5];

\fill (3/8,3/8) circle[radius=0.05];

\draw[green, dashed] (0,0) rectangle (0.75,0.75);

\end{tikzpicture}

\end{figuretikz} %
%
We could also take the intersection $U$ with $D^{2}$ of any open rectangle in $\reals^{2}$ to which $x$ belongs. By definition of $\curlyo_{D^{2}}$, we have that $U$ belongs to $\curlyo_{D^{2}}$. For instance, when $x$ is $\left(\frac{1}{4},\frac{1}{4}\right)$, we can take $U$ to be the intersection with $D^{2}$ of $\opint{0,1} \times \opint{0,1}$. 

\begin{figuretikz} 

\begin{tikzpicture}
   
\draw (1.5,0) arc (0:-270:1.5);

\fill (3/8,3/8) circle[radius=0.05];

\draw[green, dashed] (0,1.5) -- (0,0) -- (1.5,0);

\draw[green] (0,1.5) arc (90:0:1.5);

\end{tikzpicture}

\end{figuretikz} %
%
A disjoint union $U_{0} \cup U_{1}$ of a pair of subsets of $D^{2}$ which both belong to $\curlyo_{D^{2}}$, with the property that $x$ belongs to either $U_{0}$ or $U_{1}$, is also a neighbourhood of $x$ in $D^{2}$ with respect to $\curlyo_{D^{2}}$. For $U_{0} \cup U_{1}$ belongs to $\curlyo_{D^{2}}$, and $x$ belongs to $U_{0} \cup U_{1}$. When $x$ is $\left(\frac{1}{4},\frac{1}{4}\right)$, we can for instance take $U_{0}$ to be $\opint{0,\frac{1}{2}} \times \opint{0,\frac{1}{2}}$, and take $U_{1}$ to be the intersection with $D^{2}$ of $\opint{-1,-\frac{1}{2}} \times \opint{-1,-\frac{1}{2}}$.  

\begin{figuretikz} 

\begin{tikzpicture}
   
\draw (240:1.5) arc (-120:210:1.5);

\fill (3/8,3/8) circle[radius=0.05];

\draw[green, dashed] (0,0) rectangle (0.75,0.75);

\draw[green] (210:1.5) arc (210:240:1.5);

\path[name path=vertical] (240:1.5) -- ++(0,1);

\path[name path=horizontal] (210:1.5) -- ++(1,0);

\draw[green, dashed, name intersections={of={horizontal and vertical}}] (240:1.5) -- (intersection-1) -- (210:1.5);

\end{tikzpicture}

\end{figuretikz} %
%
A subset of $D^{2}$ to which $x$ does not belong is not a neighbourhood of $x$ in $D^{2}$ with respect to $\curlyo_{D^{2}}$, even if it belongs to $\curlyo_{D^{2}}$. When $x$ is $\left(\frac{1}{4},\frac{1}{4}\right)$, the subset $\opint{-\frac{1}{2},0} \times \opint{-\frac{1}{2},0}$ is not a neighbourhood of $x$, for instance.  

\begin{figuretikz} 

\begin{tikzpicture}
   
\draw (0,0) circle[radius=1.5];

\fill (3/8,3/8) circle[radius=0.05];

\draw[green, dashed] (0,0) rectangle (-0.75,-0.75);

\end{tikzpicture}

\end{figuretikz} %
%
A subset of $D^{2}$ to which $x$ belongs, but which does not belong to $\curlyo_{D^{2}}$, is not a neighbourhood of $x$ in $D^{2}$ with respect to $\curlyo_{D^{2}}$.  When $x$ is $\left(\frac{1}{4},\frac{1}{4}\right)$, the subset $\opint{0,\frac{1}{2}} \times \clint{0,\frac{1}{2}}$ is not a neighbourhood of $x$, for instance.  

\begin{figuretikz} 

\begin{tikzpicture}
   
\draw (0,0) circle[radius=1.5];

\fill (3/8,3/8) circle[radius=0.05];

\draw[green, dashed] (0,0) -- (0.75,0)
                     (0,0.75) -- (0.75,0.75);

\draw[green] (0,0) -- (0,0.75)
             (0.75,0) -- (0.75,0.75);
\end{tikzpicture}

\end{figuretikz} 
 
\end{example}

\section{Limit points}

\begin{defn} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. Suppose that $x$ belongs to $X$. Then $x$ is a {\em limit point} of $A$ in $X$ with respect to $\curlyo_{X}$ if, for every neighbourhood $U$ of $x$ in $X$ with respect to $\curlyo_{X}$, there is an $a \in U$ such that $a$ belongs to $A$. \end{defn}

\begin{rmk} In other words, $x$ is a limit point of $A$ in $X$ with respect to $\curlyo_{X}$ if and only if for every neighbourhood $U$ of $x$ in $X$ with respect to $\curlyo_{X}$, we have that $A \cap U \not= \emptyset$. \end{rmk}

\begin{rmk} \label{RemarkElementsOfSubsetAreLimitPoints} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. Suppose that $a$ belongs to $A$. Then $a$ is a limit point of $A$ in $X$ with respect to $\curlyo_{X}$, since every neighbourhood of $a$ in $X$ with respect to $\curlyo_{X}$ contains $a$. \end{rmk}

\section{Examples of limit points}

\begin{example} \label{ExampleLimitPointsInSierpinskiInterval} Let $X = \{ a,b \}$ be a set with two elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{b\}, X \right\}. \] %
%
Let $A = \{ b \}$. By Remark \ref{RemarkElementsOfSubsetAreLimitPoints}, we have that $b$ is a limit point of $A$ in $X$ with respect to $\curlyo_{X}$. Moreover, $a$ is a limit point of $A$ in $X$ with respect to $\curlyo_{X}$. For the only neighbourhood of $a$ in $X$ with respect to $\curlyo_{X}$ is $X$, and we have that $b$ belongs to $X$. \end{example}

\begin{example} \label{ExampleLimitPointsSetWithFiveElements} Let $X = \{ a,b,c,d,e \}$ be a set with five elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{a\}, \{b\}, \{a,b\}, \{b,e\}, \{c,d\}, \{a,b,e\}, \{a,c,d\}, \{b,c,d\}, \{a,b,c,d\}, \{b,c,d,e\}, X \right\}. \] %
%
Let $A = \{ d \}$. By Remark \ref{RemarkElementsOfSubsetAreLimitPoints}, we have that $d$ is a limit point of $A$ in $X$ with respect to $\curlyo_{X}$. To decide whether the other elements of $X$ are limit points, we look at their neighbourhoods.

\begin{center}
\begin{tabular}{ll}
\toprule
Element & Neighbourhoods \\
\midrule
$a$     & $\{a\}$, $\{a,b\}$, $\{a,b,e\}$, $\{a,c,d\}$, $\{a,b,c,d\}$, $X$ \\
$b$     & $\{b\}$, $\{a,b\}$, $\{b,e\}$, $\{a,b,e\}$, $\{b,c,d\}$, $\{a,b,c,d\}$, $\{b,c,d,e\}$, $X$ \\
$c$     & $\{c,d\}$, $\{a,c,d\}$, $\{b,c,d\}$, $\{a,b,c,d\}$, $\{b,c,d,e\}$, $X$ \\
$e$     & $\{b,e\}$, $\{a,b,e\}$, $\{b,c,d,e\}$, $X$ \\ 
\bottomrule
\end{tabular}
\end{center} %
%
For each element, we check whether $d$ belongs to all of its neighbourhoods.

\begin{center}
\begin{tabular}{lll}
\toprule
Element & Limit Point & Neighbourhoods to which $d$ does not belong \\
\midrule
$a$     & \cross & $\{a\}$, $\{a,b\}$, $\{a,b,e\}$ \\
$b$     & \cross & $\{b\}$, $\{a,b\}$, $\{b,e\}$, $\{a,b,e\}$ \\
$c$     & \tick  & \\
$e$     & \cross & $\{b,e\}$, $\{a,b,e\}$ \\ 
\bottomrule
\end{tabular}
\end{center} %
%
To establish that $a$, $b$, and $e$ are not limit points, it suffices to observe that any {\em one} of the neighbourhoods listed in the table above does not contain $d$. 

\end{example}

\begin{example} \label{ExampleTwoLimitPointsSetWithFiveElements} Let $(X,\curlyo_{X})$ be as in Example \ref{ExampleLimitPointsSetWithFiveElements}. Let $A = \{b,d\}$. For each of the elements $a$, $c$, and $e$, we check whether every neighbourhood contains either $b$ or $d$. The neighbourhoods are listed in a table in Example \ref{ExampleLimitPointsSetWithFiveElements}.

\begin{center}
\begin{tabular}{lll}
\toprule
Element & Limit Point & Neighbourhoods $U$ such that $A \cap U = \emptyset$ \\ 
\midrule
\hline
$a$     & \cross & $\{a\}$ \\
$c$     & \tick & \\
$e$     & \tick & \\ 
\bottomrule
\end{tabular}
\end{center} 

\end{example} 

\begin{example} \label{ExampleLimitPointsInRealsOfHalfOpenIntervalFromZeroToOne} Let $(X,\curlyo_{X})$ be $(\reals,\streals)$. Let $A = \lhopint{0,1}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $U$ be a neighbourhood of $1$ in $\reals$ with respect to $\streals$. By definition of $\streals$, there is an open interval $\opint{a,b}$ such that $a < 1 < b$ and which is a subset of $U$.   

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$[$};

\draw[purple] (2.5,0.75) node {$]$}  -- (4.5,0.75) node {$[$};;

\draw (2.5,0) -- (2.5,-0.15);
\node[anchor=mid] at (2.5,-0.5) {$a$};

\draw (4.5,0) -- (4.5,-0.15);
\node[anchor=mid] at (4.5,-0.5) {$b$};

\end{tikzpicture}

\end{figuretikz} % 
%
There is an $x \in \reals$ such that $a < x < 1$, and $0 < x$. In particular, $x$ belongs to $\lhopint{0,1}$.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (5,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$[$};

\draw[purple] (2.5,0.75) node {$]$}  -- (4.5,0.75) node {$[$};;

\draw (2.5,0) -- (2.5,-0.15);
\node[anchor=mid] at (2.5,-0.5) {$a$};

\draw (4.5,0) -- (4.5,-0.15);
\node[anchor=mid] at (4.5,-0.5) {$b$};

\draw (3.25,0) -- (3.25,-0.15);
\node[anchor=mid] at (3.25,-0.5) {$x$};

\end{tikzpicture}

\end{figuretikz} %
% 
Since $\opint{a,1}$ is a subset of $\opint{a,b}$, and since $\opint{a,b}$ is a subset of $U$, we also have that $x$ belongs to $U$. This proves that if $U$ is a neighbourhood of $1$ in $\reals$ with respect to $\streals$, then $\lhopint{0,1} \cap U$ is not empty. Thus $1$ is a limit point of $\lhopint{0,1}$ in $\reals$ with respect to $\curlyo_{\reals}$. 

Suppose now that $x \in \reals$ has the property that $x > 1$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (8,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$[$};

\draw (6,0) -- (6,-0.15);
\node[anchor=mid] at (6,-0.5) {$x$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $\epsilon \in \reals$ be such that $0 < \epsilon \leq x - 1$. Then $\opint{x - \epsilon, x + \epsilon}$ is a neighbourhood of $x$ in $\reals$ with respect to $\curlyo_{\reals}$, but $\lhopint{0,1} \cap \opint{x - \epsilon, x + \epsilon}$ is empty. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (8,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node[anchor=mid] at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$[$};

\draw (6,0) -- (6,-0.15);
\node[anchor=mid] at (6,-0.5) {$x$};

\draw[purple] (5,0.25) node {$]$} -- (7,0.25) node {$[$};

\draw (5,0) -- (5,-0.15);
\node[anchor=mid] at (5,-0.5) {$x-\epsilon$};

\draw (7,0) -- (7,-0.15);
\node[anchor=mid] at (7,-0.5) {$x+\epsilon$};

\end{tikzpicture}

\end{figuretikz} %
%
Thus $x$ is not a limit point of $\lhopint{0,1}$ in $\reals$ with respect to $\curlyo_{\reals}$. In a similar way, one can demonstrate that if $x \in \reals$ has the property that $x < 0$, then $x$ is not a limit point of $\lhopint{0,1}$ in $\reals$ with respect to $\curlyo_{\reals}$. This is the topic of Task \ref{TaskRealNumberLessThanZeroNotALimitPointInRealsOfHalfOpenIntervalFromZeroToOne}. 

\end{example}

\begin{example} \label{ExampleLimitPointsOfRationalsInReals} Let $(X,\curlyo_{X})$ be $(\reals,\streals)$. Let $A = \mathbb{Q}$, the set of rational numbers. Suppose that $x$ belongs to $\reals$. Let $U$ be a neighbourhood of $x$ in $\reals$ with respect to $\curlyo_{\reals}$. By definition of $\streals$, there is an open interval $\opint{a,b}$ such that $a < x < b$ which is a subset of $U$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$a$};

\draw (1.5,0) -- (1.5,-0.15);
\node[anchor=mid] at (1.5,-0.5) {$x$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
There is a $q \in \mathbb{Q}$ such that $a < q < x$. This is a consequence of the completeness of $\reals$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$a$};

\draw (1.5,0) -- (1.5,-0.15);
\node[anchor=mid] at (1.5,-0.5) {$x$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$b$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\draw (0.75,0) -- (0.75,-0.15);
\node[anchor=mid] at (0.75,-0.5) {$q$};

\end{tikzpicture}

\end{figuretikz} %
%
Since $\opint{a,b} \cap \mathbb{Q}$ is a subset of $U \cap \mathbb{Q}$, we deduce that $q$ belongs to $U$. We have proven that, for every neighbourhood $U$ of $x$ in $\reals$ with respect to $\streals$, $U \cap \mathbb{Q}$ is not empty. Thus $x$ is a limit point of $\mathbb{Q}$ in $\reals$ with respect to $\streals$. \end{example} 

\begin{notn} Suppose that $x$ belongs to $\reals$. We denote by $\lfloor x \rfloor$ the largest integer $z$ such that $z \leq x$. We denote by $\lceil x \rceil$ the smallest integer $z$ such that $z \geq x$. \end{notn}

\begin{example} \label{ExampleLimitPointsOfIntegersInReals} Let $(X,\curlyo_{X})$ be $(\reals,\streals)$. Let $A = \mathbb{Z}$, the set of integers. Suppose that $x$ belongs to $\reals$, and that $x$ is not an integer. Then $\opint{\lfloor x \rfloor, \lceil x \rceil}$ is a neighbourhood of $x$ in $\reals$ with respect to $\streals$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-4,0) -- (7,0);

\draw (-3,0) -- (-3,-0.15);
\node[anchor=mid] at (-3,-0.5) {$\lfloor x \rfloor -1$};

\draw (0,0) -- (0,-0.15);
\node[anchor=mid] at (0,-0.5) {$\lfloor x \rfloor$};

\draw (2,0) -- (2,-0.15);
\node[anchor=mid] at (2,-0.5) {$x$};

\draw (3,0) -- (3,-0.15);
\node[anchor=mid] at (3,-0.5) {$\lceil x \rceil$};

\draw (6,0) -- (6,-0.15);
\node[anchor=mid] at (6,-0.5) {$\lceil x \rceil + 1$};

\draw[green] (0,0.25) node {$]$} -- (3,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} % 
%
Moreover $\mathbb{Z} \cap \opint{\lfloor x \rfloor, \lceil x \rceil}$ is empty. Thus $x$ is not a limit point of $\mathbb{Z}$ in $\reals$ with respect to $\streals$.  

\end{example}

\begin{example} \label{ExampleLimitPointsOfOpenUnitDiscInR2} Let $(X,\curlyo_{X})$ be $(\reals^{2},\stRtwo)$. Let $A$ be the subset of $\reals^{2}$ given by \[ \left\{ (x,y) \in \reals^{2} \mid \| (x,y) \| < 1 \right\}. \]

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\filldraw[fill=yellow, draw=black, dashed] (0,0) circle[radius=1.5];

\end{tikzpicture}

\end{figuretikz} %
%
Suppose that $(x,y) \in \reals^{2}$ belongs to $S^{1}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[dashed] (0,0) circle[radius=1.5];

\fill (45:1.5) circle[radius=0.05];

\node at ($(45:1.5) + (0.75,0)$) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $(x,y)$ is a limit point of $A$ in $\reals^{2}$ with respect to $\curlyo_{\reals^{2}}$. Every neighbourhood of $(x,y)$ in $\reals^{2}$ with respect to $\curlyo_{\reals^{2}}$ contains an `open rectangle' $U$ to which $(x,y)$ belongs. We have that $A \cap U$ is not empty. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[dashed] (0,0) circle[radius=1.5];

\fill (45:1.5) circle[radius=0.05];

\draw[green,dashed] (45:1) rectangle (45:2);

\end{tikzpicture}

\end{figuretikz} %
%
To fill in the details of this argument is the topic of Task \ref{TaskPointOnCircleIsALimitPointOfOpenDiscInR2}. Suppose now that $(x,y) \in \reals^{2}$ does not belong to $D^{2}$. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[dashed] (0,0) circle[radius=1.5];

\fill (2.5,1.5) circle[radius=0.05];

\node at (2.5,1.1) {$(x,y)$};

\end{tikzpicture}

\end{figuretikz} % 
%
Then $(x,y)$ is not a limit point of $A$ in $\reals^{2}$ with respect to $\stRtwo$. For let $\epsilon \in \reals$ be such that \[ 0 < \epsilon < \left\| (x,y) \right\| - 1. \] Let $U_{x}$ be the open interval given by \[ \opint{x - \tfrac{\epsilon \sqrt{2}}{\epsilon}, x + \tfrac{\epsilon \sqrt{2}}{\epsilon}}. \] Let $U_{y}$ be the open interval given by \[ \opint{y - \tfrac{\epsilon \sqrt{2}}{\epsilon}, y + \tfrac{\epsilon \sqrt{2}}{\epsilon}}. \] Then $U_{x} \times U_{y}$ is a neighbourhood of $(x,y)$ in $\reals^{2}$ whose intersection with $A$ is empty. 

\begin{figuretikz}

\begin{tikzpicture}[font=\small,>=stealth]

\draw[dashed] (0,0) circle[radius=1.5];

\fill (2.5,1.5) circle[radius=0.05];

\draw[green, dashed] (2,1) rectangle (3,2);

\end{tikzpicture}

\end{figuretikz} % 
%
To check this is the topic of Task \ref{TaskPointsOutsideTheClosedDiscAreNotLimitPointsOfTheOpenUnitDiscInR2}.  

\end{example}

\section{Closure}
 
\begin{defn} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. The {\em closure} of $A$ in $X$ with respect to $\curlyo_{X}$ is the set of limit points of $A$ in $X$. \end{defn}

\begin{notn} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$. We shall denote the closure of $A$ in $X$ with respect to $\curlyo_{X}$ by $\closure{A}{(X,\curlyo_{X})}$. 
\end{notn}

\begin{rmk} The notation $\overline{A}$ is also frequently used to denote closure. \end{rmk}

\begin{rmk} \label{RemarkSubsetContainedInItsClosure} By Remark \ref{RemarkElementsOfSubsetAreLimitPoints}, we have that $A$ is a subset of $\closure{A}{(X,\curlyo_{X})}$. \end{rmk}

\begin{defn} Let $(X,\curlyo_{X})$ be a topological space. A subset $A$ of $X$ is {\em dense} in $X$ with respect to $\curlyo_{X}$ if the closure of $A$ in $X$ with respect to $\curlyo_{X}$ is $X$. \end{defn}

\section{Examples of closure}

\begin{example} Let $(X,\curlyo_{X})$ and $A$ be as in Example \ref{ExampleLimitPointsInSierpinskiInterval}. We found in Example \ref{ExampleLimitPointsInSierpinskiInterval} that the limit points of $A$ in $X$ with respect to $\curlyo_{X}$ are $a$ and $b$. Hence $\closure{A}{(X,\curlyo_{X})}$ is $X$. Thus $A$ is dense in $X$ with respect to $\curlyo_{X}$. \end{example}

\begin{example} Let $(X,\curlyo_{X})$ and $A$ be as in Example \ref{ExampleLimitPointsSetWithFiveElements}. We found in Example \ref{ExampleLimitPointsSetWithFiveElements} that $\closure{A}{(X,\curlyo_{X})}$ is $\{c,d\}$. Thus $A$ is not dense in $X$ with respect to $\curlyo_{X}$. \end{example}

\begin{example} Let $(X,\curlyo_{X})$ and $A$ be as in Example \ref{ExampleTwoLimitPointsSetWithFiveElements}. We found in Example \ref{ExampleTwoLimitPointsSetWithFiveElements} that $\closure{A}{(X,\curlyo_{X})}$ is $\{b,c,d,e\}$. Thus $A$ is not dense in $X$ with respect to $\curlyo_{X}$. \end{example}

\begin{example} We found in Example \ref{ExampleRealNumberLessThanZeroNotALimitPointInRealsOfHalfOpenIntervalFromZeroToOne} that $1$ is the only limit point of $\lhopint{0,1}$ in $\reals$ with respect to $\streals$ which does not belong to $\lhopint{0,1}$. Thus $\closure{\lhopint{0,1}}{(\reals,\streals)}$ is $\clint{0,1}$. In particular, $\lhopint{0,1}$ is not dense in $\reals$ with respect to $\streals$. \end{example}  

\begin{example} We found in Example \ref{ExampleLimitPointsOfRationalsInReals} that every $x \in \reals$ is a limit point of $\mathbb{Q}$ in $\reals$ with respect to $\streals$. In other words, $\closure{\mathbb{Q}}{(\reals,\streals)}$ is $\reals$. Thus $\mathbb{Q}$ is dense in $\reals$ with respect to $\curlyo_{\reals}$. \end{example}

\begin{example} We found in Example \ref{ExampleLimitPointsOfIntegersInReals} that if $x \in \reals$ is not an integer, then $x$ is not a limit point of $\mathbb{Z}$ in $\reals$ with respect to $\streals$. In other words, $\closure{\mathbb{Z}}{(\reals,\streals)}$ is $\mathbb{Z}$. In particular, $\mathbb{Z}$ is not dense in $\reals$ with respect to $\curlyo_{\reals}$. \end{example}

\begin{example} Let $(X,\curlyo_{X})$ be $(\reals^{2},\stRtwo)$. Let $A$ be as in Example \ref{ExampleLimitPointsOfOpenUnitDiscInR2}. We found in Example \ref{ExampleLimitPointsOfOpenUnitDiscInR2} that if $(x,y) \in \reals^{2}$ does not belong to $A$, then $(x,y)$ is a limit point of $A$ in $\reals^{2}$ with respect to $\stRtwo$ if and only if $(x,y)$ belongs to $S^{1}$. We conclude that $\closure{A}{(\reals^{2},\stRtwo)}$ is $D^{2}$. In particular, $A$ is not dense in $\reals^{2}$ with respect to $\stRtwo$. \end{example}
