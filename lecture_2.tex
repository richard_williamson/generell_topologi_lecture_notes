\setcounter{chapter}{1}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Tuesday 7th January}

\section{\texorpdfstring{Standard topology on $\reals$, continued}{Standard topology on the real numbers, continued}}

\begin{example} Let $U$ be a disjoint union of open intervals. For instance, the union of $\opint{-3,-1}$ and $\opint{4,7}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-4,0) -- (8,0);

\draw (-3,0) -- (-3,-0.15);
\node at (-3,-0.5) {$-3$};

\draw (-1,0) -- (-1,-0.15);
\node at (-1,-0.5) {$-1$};

\draw[green] (-3,0.25) node {$]$} -- (-1,0.25) node {$[$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$]$} -- (7,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $U$ belongs to $\streals$. There are two cases.

\begin{itemize}

\item[(1)] If $-3 < x < -1$, we can, for instance, take $I$ to be $\opint{-3,-1}$

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-4,0) -- (8,0);

\draw (-3,0) -- (-3,-0.15);
\node at (-3,-0.5) {$-3$};

\draw (-1,0) -- (-1,-0.15);
\node at (-1,-0.5) {$-1$};

\draw (-2,0) -- (-2,-0.15);
\node at (-2,-0.5) {$x$};

\draw[green] (-3,0.25) node {$]$} -- (-1,0.25) node {$[$};

\draw[purple] (-3,0.75) node {$]$} -- (-1,0.75) node {$[$};

\node[purple] at (-3.25,1) {$I$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$]$} -- (7,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} 

\item[(2)] If $4 < x < 7$, we can, for instance, take $I$ to be $\opint{4,7}$.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-4,0) -- (8,0);

\draw (-3,0) -- (-3,-0.15);
\node at (-3,-0.5) {$-3$};

\draw (-1,0) -- (-1,-0.15);
\node at (-1,-0.5) {$-1$};

\draw (6,0) -- (6,-0.15);
\node at (6,-0.5) {$x$};

\draw[green] (-3,0.25) node {$]$} -- (-1,0.25) node {$[$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$]$} -- (7,0.25) node {$[$};

\draw[purple] (4,0.75) node {$]$} -- (7,0.75) node {$[$};

\node[purple] at (3.75,1) {$I$};

\end{tikzpicture}

\end{figuretikz} 

\end{itemize} 

\end{example}

\begin{rmk} In fact, {\em every} subset of $U$ which belongs to $\streals$ is a disjoint union of (possibly infinitely many) open intervals. To prove this is the topic of Task \ref{TaskOpenSubsetOfRealsIsDisjointUnionOfOpenIntervals}. \end{rmk}

\begin{example} \label{ExamplePointIsNotOpenInStandardTopology} Let $U=\{x\}$ be a subset of $\reals$ consisting of a single $x \in \reals$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (3,0);

\draw (1.5,0) -- (1.5,-0.15);
\node at (1.5,-0.5) {$x$};

\fill[green] (1.5,0.25) circle[radius=0.05];  

\end{tikzpicture}

\end{figuretikz} %
%
Then $U$ does not belong to $\streals$. The only subset of $\{x\}$ to which $x$ belongs is $\{ x \}$ itself, and $\{ x \}$ is not an open interval.

\end{example}

\begin{example} Let $U$ be the half open interval $\lhopint{1,5}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$1$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$5$};

\draw[green] (0,0.25) node {$[$} -- (2,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
Then $U$ does not belong to $\streals$, since there is no open interval $I$ such that $1 \in I$ and $I \subset U$.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (3,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$1$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$5$};

\draw[green] (0,0.25) node {$[$} -- (2,0.25) node {$[$};

\draw[purple] (-0.5,0.75) node {$]$} -- (0.5,0.75) node {$[$};

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{lem} \label{LemmaUnionOfOpenSetsInStandardTopologyIsOpen} Let $\big\{ U_{j} \big\}_{j \in J}$ be a set of (possibly infinitely many) subsets of $\reals$ such that $U_{j} \in \streals$ for all $j \in J$. Then $\bigcup_{j \in J} U_{j}$ belongs to $\streals$. \end{lem}

\begin{proof} Let \[ x \in \bigcup_{j \in J} U_{j}. \] By definition of $\bigcup_{j \in J} U_{j}$, we have that $x \in U_{j}$ for some $j \in J$. By definition of $\streals$, there is an open interval $I$ such that $x \in I$ and $I \subset U_{j} \subset \bigcup_{j \in J} U_{j}$. \end{proof}

\begin{observation} \label{ObservationPairwiseIntersectionOfOpenIntervalsIsOpen} Let $I$ and $I'$ be open intervals. Then $I \cap I'$ is a (possibly empty) open interval. This is the topic of Task \ref{TaskPairwiseIntersectionOfOpenIntervalsIsOpen}. \end{observation}

\begin{example} The intersection of the open intervals $\opint{0,2}$ and $\opint{1,3}$ is the open interval $\opint{1,2}$.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (2,0) -- (2,-0.15);
\node at (2,-0.5) {$2$};

\draw[green] (0,0.25) node {$]$} -- (2,0.25) node {$[$};

\draw (1,0) -- (1,-0.15);
\node at (1,-0.5) {$1$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$3$};

\draw[green] (1,0.75) node {$]$} -- (3,0.75) node {$[$};

\draw[purple] (1,1.25) node {$]$} -- (2,1.25) node {$[$};

\end{tikzpicture}

\end{figuretikz} %
%
The intersection of the open intervals $\opint{-3,-1}$ and $\opint{4,7}$ is the empty set.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-4,0) -- (8,0);

\draw (-3,0) -- (-3,-0.15);
\node at (-3,-0.5) {$-3$};

\draw (-1,0) -- (-1,-0.15);
\node at (-1,-0.5) {$-1$};

\draw[green] (-3,0.25) node {$]$} -- (-1,0.25) node {$[$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$4$};

\draw (7,0) -- (7,-0.15);
\node at (7,-0.5) {$7$};

\draw[green] (4,0.25) node {$]$} -- (7,0.25) node {$[$};

\end{tikzpicture}

\end{figuretikz}

\end{example}

\begin{lem} \label{LemmaIntersectionOfPairOfOpenSetsInStandardTopologyIsOpen} Let $U$ and $U'$ be subsets of $\reals$ which belong to $\streals$. Then $U \cap U'$ belongs to $\streals$. \end{lem}

\begin{proof} Let $x \in U \cap U'$. By definition of $\streals$, we have the following.

\begin{itemize}

\item[(1)] There is an open interval $I_{U}$ such that $x \in I_{U}$ and $I_{U} \subset U$.

\item[(2)] There is an open interval $I_{U'}$ such that $x \in I_{U'}$ and $I_{U'} \subset U'$.

\end{itemize} %
%
Then $x \in I_{U} \cap I_{U'}$ and $I_{U} \cap I_{U'} \subset U \cap U'$. By Observation \ref{ObservationPairwiseIntersectionOfOpenIntervalsIsOpen}, we have that $I_{U} \cap I_{U'}$ is an open interval.  
\end{proof}

\begin{prpn}  \label{PropositionStandardTopologyDefinesATopology} The set $\streals$ defines a topology on $\reals$. \end{prpn}

\begin{proof} This is exactly established by Observation \ref{ObservationRealsAndEmptySetBelongToStandardTopologyOnReals}, Lemma \ref{LemmaUnionOfOpenSetsInStandardTopologyIsOpen}, and Lemma \ref{LemmaIntersectionOfPairOfOpenSetsInStandardTopologyIsOpen}. \end{proof}

\begin{terminology} We shall refer to $\streals$ as the {\em standard topology} on $\reals$. \end{terminology}

\begin{rmk} \label{RemarkInfiniteIntersectionOfOpenSetsInStandardTopologyNotNecessarilyOpen} An infinite intersection of subsets of $\reals$ which belong to $\streals$ does {\em not} necessarily belong to $\streals$. For instance, by Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, we have that $\opint{-\frac{1}{n},\frac{1}{n}}$ belongs to $\streals$ for every integer $n \geq 1$. However, \[ \bigcap_{n \in \mathbb{N}} \opint{-\tfrac{1}{n},\tfrac{1}{n}} = \{ 0 \}. \] 

\begin{figuretikz}

\begin{tikzpicture}

\draw[yellow] (-3.5,0) -- (3.5,0);
\foreach \x/\xtext in {-1,-0.5/\frac{1}{2},0,0.5/\frac{1}{2},1}
  \draw (3*\x,0) -- (3*\x,-0.1) node[anchor=north] {$\xtext$};

\foreach \x in {1,2,...,20}
  \draw[green] ({0.25-(3/\x)},0) ++(-45:-0.25) arc (-45:45:-0.25);
\foreach \x in {1,2,...,20}
  \draw[green] ({(3/\x)-0.25},0) ++(-45:0.25) arc (-45:45:0.25);

\end{tikzpicture}

\end{figuretikz} % 
%
By Example \ref{ExamplePointIsNotOpenInStandardTopology}, the set $\{ 0 \}$ does not belong to $\streals$. 

\end{rmk}

\begin{rmk} The topological space $(\reals,\curlyo_{\reals})$ is fundamental. We shall construct all our geometric examples of topological spaces in various `canonical ways' from it. 

A principal reason that we allow infinite unions in (3) of Definition \ref{DefinitionTopologicalSpace}, but only finite intersections in (4) of Definition \ref{DefinitionTopologicalSpace}, is that these properties hold for $\streals$. \end{rmk}

\begin{rmk} An {\em Alexandroff topological space} is a topological space $(X,\curlyo)$ which, unlike $(\reals,\curlyo_{\reals})$ and the other geometric examples of topological spaces that we shall meet, has the property that if $U$ is an intersection of (possibly infinitely many) subsets of $X$ which belong to $\curlyo$, then $U$ belongs to $\curlyo$. Alexandroff topological spaces are the topic of Exploration \ref{SectionExplorationAlexandroffTopologicalSpaces}. \end{rmk}

\section{Subspace topologies}

\begin{rmk} We shall explore several `canonical ways' to construct topological spaces. In this section, we discuss the first of these. \end{rmk}

\begin{defn} \label{DefinitionSubspaceTopology} Let $(Y,\curlyo_{Y})$ be a topological space, and let $X$ be a subset of $Y$. Let $\curlyo_{X}$ denote the set \[ \left\{ X \cap U \mid U \in \curlyo_{Y} \right\}. \] \end{defn}

\begin{prpn} \label{PropositionSubspaceTopology} Let $(Y,\curlyo_{Y})$ be a topological space, and let $X$ be a subset of $Y$. Then $(X,\curlyo_{X})$ is a topological space. \end{prpn}

\begin{proof} We verify that each of the conditions of Definition \ref{DefinitionTopologicalSpace} holds. 

\begin{itemize}

\item[(1)] Since $\curlyo_{Y}$ is a topology on $Y$, we have that $\emptyset$ belongs to $\curlyo_{Y}$. We also have that $\emptyset = X \cap \emptyset$. Thus $\emptyset$ belongs to $\curlyo_{X}$.

\item[(2)] Since $\curlyo_{Y}$ is a topology on $Y$, we have that $Y$ belongs to $\curlyo_{Y}$. We also have that $X = X \cap Y$. Thus $X$ belongs to $\curlyo_{X}$.

\item[(3)] Let $\left\{ U_{j} \right\}_{j \in J}$ be a set of subsets of $X$ which belong to $\curlyo_{X}$. By definition of $\curlyo_{X}$, we have, for every $j \in J$, that \[ U_{j} = X \cap U_{j}', \] for a subset $U_{j}'$ of $Y$ which belongs to $\curlyo_{Y}$. Now \begin{align*} \bigcup_{j \in J} U_{j} &= \bigcup_{j \in J} \left( X \cap U_{j}' \right) \\ &= X \cap \left( \bigcup_{j \in J} U_{j}' \right). \end{align*} Since $\curlyo_{Y}$ is a topology on $Y$, we have that $\bigcup_{j \in J} U_{j}'$ belongs to $\curlyo_{Y}$. We deduce that $\bigcup_{j \in J} U_{j}$ belongs to $\curlyo_{X}$.

\item[(4)] Suppose that $U_{0}$ and $U_{1}$ are subsets of $X$ which belong to $\curlyo_{X}$. By definition of $\curlyo_{X}$, we have that \[ U_{0} = X \cap U_{0}' \] and \[ U_{1} = X \cap U_{1}', \] for a pair of subsets $U_{0}'$ and $U_{1}'$ of $Y$ which belong to $\curlyo_{Y}$. Now \begin{align*} U_{0} \cap U_{1} &= \left( X \cap U_{0}' \right) \cap \left( X \cap U_{1}' \right) \\ &= X \cap \left( U_{0}' \cap U_{1}' \right). \end{align*} Since $\curlyo_{Y}$ is a topology on $Y$, we have that $U_{0}' \cap U_{1}'$ belongs to $\curlyo_{Y}$. We deduce that $U_{0} \cap U_{1}$ belongs to $\curlyo_{X}$.          

\end{itemize}

\end{proof}

\begin{rmk} The flavour of this proof is very similar to many others in the early part of the course. It is a very good idea to work on it until you thoroughly understand it. This is the topic of Task \ref{TaskSubspaceTopologyConstruction}. \end{rmk}

\begin{terminology} We refer to $\curlyo_{X}$ as the {\em subspace topology} on $X$ with respect to $(Y,\curlyo_{Y})$. \end{terminology}

\section{Example of a subspace topology --- the unit interval}

\begin{defn} Let $I$ denote the closed interval $\clint{0,1}$. Let $\curlyo_{I}$ denote the subspace topology on $I$ with respect to $(\reals,\curlyo_{\reals})$. \end{defn}

\begin{terminology} We refer to $(I,\curlyo_{I})$ as the {\em unit interval}. \end{terminology} 

\begin{example} \label{ExampleOpenIntervalInUnitInterval} Let $\opint{a,b}$ be an open interval such that $0 < a < b < 1$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$1$};

\draw[green] (1,0.25) node {$]$} -- (3,0.25) node {$[$};

\draw (1,0) -- (1,-0.15);
\node at (1,-0.5) {$a$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$b$};

\end{tikzpicture}

\end{figuretikz} %
%
As we observed in Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, the open interval $\opint{a,b}$ belongs to $\curlyo_{\reals}$, We also have that \[ \opint{a,b} = I \cap \opint{a,b}. \] Thus $\opint{a,b}$ belongs to $\curlyo_{I}$.
\end{example}

\begin{example} \label{ExampleOpenLeftHalfOpenIntervalInUnitInterval} Let $\lhopint{0,b}$ be an half open interval such that $0 < b < 1$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (2.5,0.25) node {$[$};

\draw (2.5,0) -- (2.5,-0.15);
\node at (2.5,-0.5) {$b$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $a$ be any real number such that $a < 0$. As we observed in Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, the open interval $\opint{a,b}$ belongs to $\curlyo_{\reals}$, We have that \[ \lhopint{0,b} = I \cap \opint{a,b}. \] Thus $\lhopint{0,b}$ belongs to $\curlyo_{I}$.
\end{example}

\begin{example} \label{ExampleOpenRightHalfOpenIntervalInUnitInterval} Let $\rhopint{a,1}$ be an half open interval such that $0 < a < 1$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$1$};

\draw[green] (1,0.25) node {$]$} -- (4,0.25) node {$]$};

\draw (1,0) -- (1,-0.15);
\node at (1,-0.5) {$a$};

\end{tikzpicture}

\end{figuretikz} %
%
Let $b$ be any real number such that $b > 1$. As we observed in Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, the open interval $\opint{a,b}$ belongs to $\curlyo_{\reals}$, We have that \[ \rhopint{a,1} = I \cap \opint{a,b}. \] Thus $\rhopint{a,1}$ belongs to $\curlyo_{I}$.
\end{example}

\begin{example} As we proved in Proposition \ref{PropositionSubspaceTopology}, the set $I$ belongs to $\curlyo_{I}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$1$};

\draw[green] (0,0.25) node {$[$} -- (4,0.25) node {$]$};

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} Disjoint unions of subsets of $I$ of the kind discussed in Example \ref{ExampleOpenIntervalInUnitInterval}, Example \ref{ExampleOpenLeftHalfOpenIntervalInUnitInterval}, and Example \ref{ExampleOpenRightHalfOpenIntervalInUnitInterval}, belong to $\curlyo_{I}$. This is a consequence of Proposition \ref{PropositionSubspaceTopology}, but could also be demonstrated directly. For instance, the set \[ \lhopint{0,\tfrac{1}{4}} \cup \opint{\tfrac{3}{8},\tfrac{5}{8}} \cup \rhopint{\tfrac{1}{4},1} \] belongs to $\curlyo_{I}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (4,0);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$0$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$1$};

\draw[green] (4*3/8,0.25) node {$]$} -- (4*5/8,0.25) node {$[$};

\draw (4*3/8,0) -- (4*3/8,-0.15);
\node at (4*3/8,-0.5) {$\tfrac{3}{8}$};

\draw (4*5/8,0) -- (4*5/8,-0.15);
\node at (4*5/8,-0.5) {$\tfrac{5}{8}$};

\draw[green] (0,0.25) node {$[$} -- (1,0.25) node {$[$};

\draw (1,0) -- (1,-0.15);
\node at (1,-0.5) {$\tfrac{1}{4}$};

\draw[green] (3,0.25) node {$]$} -- (4,0.25) node {$]$};

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$\tfrac{3}{4}$};

\end{tikzpicture}

\end{figuretikz} %

\end{example}
