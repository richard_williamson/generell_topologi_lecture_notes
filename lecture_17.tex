\setcounter{chapter}{16}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 3rd February}

\section{Locally compact topological spaces}

Begin by recalling the Heine-Borel theorem.

\begin{defn} A topological space $(X,\curlyo_{X})$ is {\em locally compact} if for every $x \in X$ and every neighbourhood $U$ of $x$ in $(X,\curlyo_{X})$ there is a neighbourhood $U'$ of $x$ in $(X,\curlyo_{X})$ such that the following hold.

\begin{itemize}

\item[(1)] The closure $\overline{U'}$ of $U$ in $(X,\curlyo_{X})$ is a compact subset of $X$.

\item[(2)] We have that $\overline{U'} \subset U$. 

\end{itemize}

\end{defn}

\begin{examples} \label{ExamplesLocallyCompactSpaces}
~
\begin{itemize}

\item[(1)] The topological space $(\reals,\curlyo_{\reals})$ is locally compact, whereas in Examples \ref{ExamplesCompactNonCompactSpaces} (2) we saw that $(\reals,\curlyo_{\reals})$ is not compact. Let us prove that $(\reals,\curlyo_{\reals})$ is locally compact.

Let $x \in \reals$ and let $U$ be a neighbourhood of $x$ in $(\reals,\curlyo_{\reals})$. By definition of $\curlyo_{\reals}$ we have that \[ \{ (a,b) \mid a,b \in \reals \} \] is a basis of $(\reals,\curlyo_{\reals})$. By Question 3 (a) of Exercise Sheet 2 we deduce that there is an open interval $(a,b)$ in $\reals$ such that  $x \in (a,b)$ and $(a,b) \subset U$. 

Let $a' \in \reals$ be such that $a < a' < x$. Let $b' \in \reals$ be such that $x < b' < b$. Let $U'=(a',b')$. By Question 5 (c) of Exercise Sheet 4 we have that $\overline{U'} = [a',b']$. Here $\overline{U'}$ denotes the closure of $U'$ in $(\reals,\curlyo_{\reals})$. In particular we have that $\overline{U} \subset (a,b) \subset U$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (5,0);

\draw[green] (0.5,0.2) node {$($} -- (4.5,0.2) node {$)$};

\draw[purple] (1.5,0.45) node {$[$} to node[auto] {$\overline{U''}$} (3.5,0.45) node {$]$};

\draw (0.5,0) -- (0.5,-0.1)
      (1.5,0) -- (1.5,-0.1)
      (2.5,0) -- (2.5,-0.1)
      (3.5,0) -- (3.5,-0.1)
      (4.5,0) -- (4.5,-0.1);

\node[anchor=mid] at (0.5,-0.45) {$a$};      
\node[anchor=mid] at (1.5,-0.45) {$a'$}; 
\node[anchor=mid] at (2.5,-0.45) {$x$};        
\node[anchor=mid] at (3.5,-0.45) {$b'$};      
\node[anchor=mid] at (4.5,-0.45) {$b$};  

\end{tikzpicture}

\end{figuretikz}

By Corollary \ref{CorollaryCompactnessClosedIntervals} we have that $[a,b]$ is a compact subset of $(\reals,\curlyo_{\reals})$. Putting everything together we have that:

\begin{itemize}

\item[(i)] $U' \in \curlyo_{\reals}$

\item[(ii)] $x \in U'$

\item[(iii)] $\overline{U'} \subset U$

\item[(iv)] $\overline{U'}$ is a compact subset of $\reals$.

\end{itemize}

\item[(2)] Let $X$ be a set and let $\curlyo^{\text{disc}}$ be the discrete topology on $X$. Then $(X,\curlyo^{\text{disc}})$ is locally compact. Let us prove this.

Let $x \in X$ and let $U$ be a neighbourhood of $x$ in $(X,\curlyo^{\text{disc}})$. We make the following observations.

\begin{itemize}

\item[(i)]  $\{ x \} \subset U$.

\item[(ii)] $\{x\}$ is open in $(X,\curlyo^{\text{disc}})$.

\item[(iii)] $\{x\}$ is closed in $(X,\curlyo^{\text{disc}})$ since $X \setminus \{x\}$ is open in $(X,\curlyo^{\text{disc}})$. By Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure} we deduce that $\overline{\{x\}} = \{x\}$.

\end{itemize} %
%
Thus $(X,\curlyo^{\text{disc}})$ is locally compact. 

By contrast, if $X$ is infinite then $(X,\curlyo_{X})$ is not compact. For \[ \big\{ \{ x \} \big\}_{x \in X}\] is an open covering of $X$ which if $X$ is infinite has no finite subcovering.

\item[(3)] A product of locally compact topological spaces is locally compact. This is left as an exercise. Thus $(\reals^{n},\curlyo_{\reals^{n}})$ is locally compact.

\item[(4)] Let $(X,\curlyo_{X})$ be a locally compact topological space. Let $U$ be an open subset of $X$ equipped with its subspace topology $\curlyo_{U}$ with respect to $(X,\curlyo_{X})$. Then $(U,\curlyo_{U})$ is locally compact. This is left as an exercise. 

By (3) we conclude that any `open blob' in $(\reals^{n},\curlyo_{\reals^{n}})$ is locally compact. 

\item[(5)] Let $(X,\curlyo_{X})$ be a locally compact topological space. Let $A$ be a closed subset of $X$ equipped with its subspace topology $\curlyo_{A}$ with respect to $(X,\curlyo_{X})$. Then $(A,\curlyo_{A})$ is locally compact. This is left as an exercise. 
\end{itemize}
\end{examples}

\begin{prpn} \label{PropositionCompactHausdorffIsLocallyCompact} Let $(X,\curlyo_{X})$ be a compact Hausdorff topological space. Then $(X,\curlyo_{X})$ is locally compact. \end{prpn}

\begin{proof} Let $x \in X$ and let $U$ be a neighbourhood of $x$ in $(X,\curlyo_{X})$. Since $U$ is open in $(X,\curlyo_{X})$ we have that $X \setminus U$ is closed in $(X,\curlyo_{X})$. Since $(X,\curlyo_{X})$ is compact we deduce by Proposition \ref{PropositionClosedSubsetOfACompactSpaceIsCompact} that $X \setminus U$ is a compact subset of $X$. 

Since $(X,\curlyo_{X})$ is Hausdorff we deduce by Lemma \ref{LemmaSeparationOfAPointAndACompactSubset} that there are open subsets $U'$ and $U''$ of $X$ with the following properties:

\begin{itemize}

\item[(1)] $X \setminus U \subset U'$,

\item[(2)] $x \in U''$,

\item[(3)] $U' \cap U'' = \emptyset$. 

\end{itemize} %
%
By (3) and Question 1 (f) of Exercise Sheet 4 we have that $U' \cap \overline{U''} = \emptyset$. Here $\overline{U''}$ is the closure of $U''$ in $(X,\curlyo_{X})$. We deduce by appeal to (1) that \begin{align*} \overline{U''} &\subset X \setminus U' \\ &\subset X \setminus (X \setminus U) \\ &= U. \end{align*} 

By Proposition \ref{PropositionClosedSubsetOfACompactSpaceIsCompact} we have that $\overline{U''}$ is closed in $(X,\curlyo_{X})$. Since $(X,\curlyo_{X})$ is compact we deduce by Proposition \ref{PropositionClosedSubsetOfACompactSpaceIsCompact} that $\overline{U''}$ is a compact subset of $X$. Putting everything together we have the following.

\begin{itemize}

\item[(1)] $U'' \in \curlyo_{X}$.

\item[(2)] $x \in U''$.

\item[(3)] $\overline{U''} \subset U$.

\item[(4)] $\overline{U''}$ is a compact subset of $X$.

\end{itemize} %
% 
Thus $(X,\curlyo_{X})$ is locally compact. 
\end{proof}

\begin{example} Let $n \geq 1$. We make the following observations.

\begin{itemize}

\item[(1)] By Proposition \ref{PropositionUnitIntervalIsCompact}, Proposition \ref{PropositionProductOfCompactSpacesIsCompact}, and induction we have that $(I^{n},\curlyo_{I^{n}})$ is compact.

\item[(2)] By Examples \ref{ExamplesHausdorffness} (1) we have that $(\reals,\curlyo_{\reals})$ is Hausdorff. We deduce that $(I,\curlyo_{I})$ is Hausdorff by Proposition \ref{PropositionSubspaceHausdorffSpaceIsHausdorff}. Thus by Proposition \ref{PropositionProductHausdorffSpacesIsHausdorff} we have that $(I^{n},\curlyo_{I^{n}})$ is Hausdorff. 

\end{itemize} %
%
We conclude by Proposition \ref{PropositionCompactHausdorffIsLocallyCompact} that $(I^{n},\curlyo_{I^{n}})$ is locally compact. We can also see this by appealing to Examples \ref{ExamplesLocallyCompactSpaces} (1), (3), and (5). \end{example} 

\section{Topological spaces which are not locally compact}

\begin{example} \label{ExampleRationalsNotLocallyCompact} Let $\mathbb{Q}$ be equipped with its subspace topology $\curlyo_{\mathbb{Q}}$ with respect to $(\reals,\curlyo_{\reals})$. Then $(\mathbb{Q},\curlyo_{\mathbb{Q}})$ is not locally compact. Let us prove this. 

Let $q \in \mathbb{Q}$ and let $U$ be a neighbourhood of $q$ in $(\mathbb{Q},\curlyo_{\mathbb{Q}})$. By definition of $\curlyo_{\mathbb{Q}}$ there is a $U' \in \curlyo_{\reals}$ such that $U=\mathbb{Q} \cap U'$. By definition of $\curlyo_{\reals}$ we have that \[ \big\{ (a,b) \mid a,b \in \reals \big\} \] is a basis for $(\reals,\curlyo_{\reals})$. We deduce by Question 3 (a) of Exercise Sheet 2 that there are $a,b \in \reals$ such that $q \in (a,b)$ and $(a,b) \subset U'$. 

Suppose that $\overline{U}$ is a compact subset of $(\mathbb{Q},\curlyo_{\mathbb{Q}})$. The subspace topology on $\overline{U}$ with respect to $(\mathbb{Q},\curlyo_{\mathbb{Q}})$ is equal to the subspace topology on $\overline{U}$ with respect to $(\reals,\curlyo_{\reals})$. Thus we have that $\overline{U}$ is a compact subset of $(\reals,\curlyo_{\reals})$. 

By Examples \ref{ExamplesHausdorffness} (1) we have that $(\reals,\curlyo_{\reals})$ is Hausdorff. We deduce by Proposition \ref{PropositionCompactSubsetOfAHausdorffSpaceIsClosed} that $\overline{U}$ is closed in $(\reals,\curlyo_{\reals})$. Since $(a,b) \subset U'$ we have that \[ \overline{ \mathbb{Q} \cap (a,b)} \subset \overline{\mathbb{Q} \cap U'} = \overline{U}, \] where $\overline{\mathbb{Q} \cap (a,b)}$ denotes the closure of $\mathbb{Q} \cap (a,b)$ in $(\reals,\curlyo_{\reals})$. 

By Question 5 (d) of Exercise Sheet 4 we have that $\overline{\mathbb{Q} \cap (a,b)} = [a,b]$. We deduce that $[a,b] \subset \overline{U}$. Since $[a,b]$ contains irrational numbers this contradicts the fact that $\overline{U} \subset \mathbb{Q}$.
 
We conclude that no neighbourhood of $q$ in $(\mathbb{Q},\curlyo_{\mathbb{Q}})$ is compact. Hence $(\mathbb{Q},\curlyo_{\mathbb{Q}})$ is not locally compact. 

\end{example}

\begin{example} Let $X$ denote the subset \[ \{0,0\} \cup \{ (x,y) \mid x > 0 \}\] of $\reals^{2}$ equipped with the subspace topology $X$ with respect to $(\reals^{2},\curlyo_{\reals^{2}})$. Then $X$ is not locally compact. Indeed, let us prove that for every neighbourhood $U$ of $0$, we have that $\overline{U}$ is not compact. 

If $U$ were compact, it would also be a compact subset of $(\reals^{2},\curlyo_{\reals^{2}})$, and hence closed in $(\reals^{2},\curlyo_{\reals^{2}})$. Since $U$ must contain $(a,b) \times (a',b')$ for some $a < 0 < b$ and $a' < 0 < b'$, we would have that $[a,b] \times [a',b'] \subset \overline{U}$. But then for example $(0,y) \in U$ for any $0 < y <  b'$, which is a contradiction of the definition of $X$. 

\end{example}

\section{Exponentiability}

\begin{rmk} Discussion of cartesian closure for sets (relationship between products and sets of maps). \end{rmk}

\begin{rmk} Can we do something similar in topology? \end{rmk}

\begin{terminology} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let $A$ be a compact subset of $X$, and let $U$ be a subset of $Y$ which belongs to $\curlyo_{Y}$. We denote by $M_{X,Y}(A,U)$ the set of continuous maps \ar{X,Y,f} such that $f(A)$ is a subset of $U$. \end{terminology}

\begin{defn} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. A subset $M$ of $Y^{X}$ belongs to the {\em compact-open topoloy} on $Y^{X}$ if, for every $f \in M$, there is a finite set ${(A_{i},U_{i})}_{i \in I}$, where $A_{i}$ is a compact subset of $X$, and let $U$ be a subset of $Y$ which belongs to $\curlyo_{Y}$, such that $f$ belongs to $M(A_{i},U_{i})$ for every $i \in I$, and such that $\bigcap_{i \in I} M(A_{i},U_{i})$ is a subset of $M$. \end{defn}

\begin{prpn} \label{PropositionEvaluationMapIsContinuous} Let $(X,\curlyo_{X})$ be a locally compact topological space, and let $(Y,\curlyo_{Y})$ be a topological space. The map \ar{X \times Y^{X},Y,ev} given by $(x,f) \mapsto f(x)$ is continuous, where $X \times Y^{X}$ is equipped with the product topology.
\end{prpn}

\begin{rmk} The Proposition does not necessarily hold if $(X,\curlyo_{X})$ is not locally compact. See the Exercise Sheet. \end{rmk}

\begin{prpn} Let $(X,\curlyo_{X})$, $(Y,\curlyo_{Y})$, and $(Z,\curlyo_{Z})$ be topological spaces, and suppose that we have a continuous map \ar{X \times Y,{Z.},f} Then the map \ar{X,Z^{Y},{\alpha(f)}} given by $x \mapsto f_{x}$, where \ar{Y,Z,f_{x}} is the map $y \mapsto f(x,y)$, is continuous.

Conversely, suppose that $(X,\curlyo_{X})$ is locally compact and that we have a continuous map \ar{X,{Z^{Y}.},g} Then the map \ar[4]{X \times Y,Z,\alpha^{-1}(g)} given by $(x,y) \mapsto \Big(g(x)\Big)(y)$ is continuous.

\end{prpn}

\section{Local compactness, products, and quotients}

\begin{prpn} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let $\sim$ be an equivalence relation on $X$, and let \ar{X,X/\sim,\pi} denote the quotient map. Define an equivalence relation $\sim'$ on $X \times Y$ by $(x,y) \sim' (x',y')$ if $x \sim x'$ and $y=y'$. If $(Y,\curlyo_{Y})$ is locally compact, then $(X / \sim) \times Y$ is homeomorphic to $(X \times Y)/\sim'$. \end{prpn}

 \begin{example} We defined the cylinder to be $S^{1} \times I$. Since $S^{1} \cong I/\{0,1\}$, we have that the cylinder is homeomorphic to $(I \times I)/\sim'$, where $\sim'$ is defined by $(0,x) \sim (1,x)$ for all $x \in I$.  
\end{example}

\begin{prpn} Let $(X,\curlyo_{X})$ be a locally compact Hausdorff topological space, and let $\sim$ be an equivalence relation on $X$. Then $(X/\sim,\curlyo_{X/\sim})$ is a Hausdorff space if and only if $R_{\sim}$ is closed in $(X \times X, \curlyo_{X \times X})$. \end{prpn}
