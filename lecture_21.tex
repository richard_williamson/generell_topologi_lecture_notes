\setcounter{chapter}{20}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 17th March}

\section{Knots}

\begin{defn} A {\em knot} is a subset $K$ of $\reals^{3}$ such that $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S^{1}})$, where $\curlyo_{K}$ is the subspace topology on $K$ with respect to $(\reals^{3},\curlyo_{\reals^{3}})$. \end{defn}

\begin{example} \label{ExampleUnknotInTopologicalCategory} Let $K$ be the subset of $\reals^{3}$ given by \[ \left\{ (x,y,0) \in \reals^{3} \mid \| (x,y) \| = 1 \right\}. \] Then $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. This is the topic of Task \ref{TaskUnknotInTopologicalCategoryIsAKnot}. Thus $K$ is a knot.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) circle[radius=1.25];

\end{tikzpicture}

\end{figuretikz}

\end{example}

\begin{rmk} It is useful for many purposes to define knots by `glueing together straight lines'. Whilst the knot of Example \ref{ExampleUnknotInTopologicalCategory} is simple to define rigorously, it is far from straightforward to find a rigorous `curvy' definition of almost all of the other knots and links that we shall investigate, at least if we ask for a `nice' description: solutions to an equation, for instance.

In principle, it is certainly possible, one way or another. For example, one could glue together `pieces' of circles. Nevertheless, it is simpler to restrict to straight lines. 

Moreover, this has theoretical advantages, exactly analogous to the benefits we saw of equipping a surface with a $\Delta$-complex structure. Furthermore, it turns out not to be a loss of any generality! This is quite a deep fact.

\end{rmk}

\begin{caution} When drawing knots on paper or on the blackboard, we typically draw them in a `curvy' way. This is entirely harmless, as long as we remember that: we ultimately have to have a rigorous definition of our knots to work with; and this definition is, for us, one which involves glueing straight lines together. Roughly speaking, our `curvy' picture `smooths out' the one given by straight lines. \end{caution}

\begin{rmk} Knots defined by glueing together straight lines are sometimes referred to as {\em piecewise linear}. \end{rmk}

\begin{example} \label{ExampleUnknotInPiecewiseLinearCategory} Let $L_{1}$, $L_{2}$, $L_{3}$, and $L_{4}$ be the lines in $\reals^{3}$ given by the set of $(x,y,0)$ in $\reals^{3}$ which satisfy the indicated conditions.  

\begin{center}
\begin{tabular}{lll}
\toprule
Line & Condition on $x$ & Condition on $y$ \\
\midrule
$L_{1}$ & $-1 \leq x \leq 1$ & $y=1$ \\
$L_{2}$ & $-1 \leq x \leq 1$ & $y=-1$ \\
$L_{3}$ & $x=1$              & $-1 \leq y \leq 1$ \\
$L_{4}$ & $x=-1$             & $-1 \leq y \leq 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Let $K$ be the subset of $\reals^{3}$ given by the union of these four lines. Then $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. This is the topic of Task \ref{TaskUnknotInPiecewiseLinearCategoryIsAKnot}. Thus $K$ is a knot. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-1,-1) rectangle (1,1);

\end{tikzpicture}

\end{figuretikz}

\end{example}

\begin{rmk} We shall think of the knot $K$ of Example \ref{ExampleUnknotInPiecewiseLinearCategory} as the piecewise linear brother of the knot of Example \ref{ExampleUnknotInTopologicalCategory}. \end{rmk}

\begin{terminology} We refer to the knot $K$ of Example \ref{ExampleUnknotInPiecewiseLinearCategory} as the {\em unknot}. \end{terminology}

\begin{notn} We shall denote the unknot by $\knot{0,1}$. \end{notn}

\begin{notn} Suppose that $(a,b)$ belongs to $\reals^{2}$. We denote by $\knotcross{(a,b)}$ the subset of $\reals^{2}$ given by \[ \left\{ (x,y) \in \reals^{2} \mid \text{$a -\tfrac{1}{2} \leq x \leq a + \tfrac{1}{2}$ and $b - \tfrac{1}{2} \leq y \leq b + \tfrac{1}{2}$} \right\}. \]  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-0.75,0) -- (0.75,0);
\draw (0,-0.75) -- (0,0.75);

\end{tikzpicture}

\end{figuretikz} %
%
The `crossing point' is $(a,b)$. 

\end{notn}

\begin{notn} \label{NotationOverCrossing} Suppose that $(a,b)$ belongs to $\reals^{2}$. Let $L_{i}$ for $1 \leq i \leq 4$ be the lines in $\reals^{3}$ given by the set of $(x,y,z)$ in $\reals^{3}$ which satisfy the indicated conditions.   

\begin{center}
\begin{tabular}{llll}
\toprule
Line & Condition on $x$ & Condition on $y$ & Condition on $z$  \\
\midrule
$L_{1}$ & $x=a-\frac{1}{2}$                             & $y=b$ & $0 \leq z \leq \frac{1}{4}$ \\
$L_{2}$ & $a - \frac{1}{2} \leq x \leq a + \frac{1}{2}$ & $y=b$ & $z = \frac{1}{4}$ \\
$L_{3}$ & $x=a+\frac{1}{2}$                             & $y=b$ & $0 \leq z \leq \frac{1}{4}$ \\
$L_{4}$ & $x=a$                                         & $b - \frac{1}{2} \leq y \leq b + \frac{1}{2}$ & $z=0$ \\
\bottomrule
\end{tabular}
\end{center} %
%
We denote by $\knotovercross{(a,b)}$ the subset of $\reals^{3}$ given by the union of these lines.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) -- (0,0.5) -- (1,1.5) -- (1,1);
\draw[shorten >= 0.5em] (-0.2,1.5) -- (0.4,0.9);
\draw[shorten <= 0.5em] (0.4,0.9) -- (1.2,0.1); 
\end{tikzpicture}

\end{figuretikz}

\end{notn}

\begin{rmk} \label{RemarkDepictionOfOverCrossings} The picture of Notation \ref{NotationOverCrossing} is with respect to the following coordinate axes.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[->] (0,0,0) -- (0,1,0);
\draw[->] (0,0,0) -- (0,0,-1);
\draw[->] (0,0,0) -- (1,0,1);

\node at (0,1.2,0) {$z$};
\node at (0,0,-1.3) {$x$};
\node at (1.2,0,1.2) {$y$};

\end{tikzpicture}

\end{figuretikz} %
%
We shall typically view knots from a bird's eye point of view, looking down onto the $x$-$y$-plane.  We then see the following.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-0.75,0) -- (0.75,0);
\draw (0,-0.75) -- (0,0.75);

\end{tikzpicture}

\end{figuretikz} %
%
The `crossing point' is $(a,b)$. 

We cannot see that vertical line segment passes under the horizontal line segment. To indicate this, and thereby to distinguish between $\knotovercross{(a,b)}$ and $\knotcross{(a,b)}$, we shall depict $\knotovercross{(a,b)}$ from a bird's eye point of view as follows.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) -- (1.5,0);
\draw[shorten >= 0.5em] (0.75,-0.75) -- (0.75,0);
\draw[shorten <= 0.5em] (0.75,0) -- (0.75,0.75);

\end{tikzpicture}

\end{figuretikz} 

\end{rmk}

\begin{rmk} In words: if we think of $\knotcross{(a,b)}$ as lying in the $x$-$y$-plane in $\reals^{3}$, then $\knotovercross{(a,b)}$ is obtained from $\knotcross{(a,b)}$ by replacing the horizontal line segment by a `croquet hoop'. \end{rmk}

\begin{notn} \label{NotationUnderCrossing} Suppose that $(a,b)$ belongs to $\reals^{2}$. Let $L_{i}$ for $1 \leq i \leq 4$ be the lines in $\reals^{3}$ given by the set of $(x,y,z)$ in $\reals^{3}$ which satisfy the indicated conditions.   

\begin{center}
\begin{tabular}{llll}
\toprule
Line & Condition on $x$ & Condition on $y$ & Condition on $z$  \\
\midrule
$L_{1}$ & $x=a$                                       & $y=b-\frac{1}{2}$                         & $0 \leq z \leq \frac{1}{4}$ \\
$L_{2}$ & $x=a$                                       & $b-\frac{1}{2} \leq y \leq b+\frac{1}{2}$ & $z = \frac{1}{4}$ \\
$L_{3}$ & $x=a$                                       & $y=b+\frac{1}{2}$                         & $0 \leq z \leq \frac{1}{4}$ \\
$L_{4}$ & $a-\frac{1}{2} \leq x \leq a + \frac{1}{2}$ & $y=b$                                     & $z=0$ \\
\bottomrule
\end{tabular}
\end{center} %
%
We denote by $\knotundercross{(a,b)}$ the subset of $\reals^{3}$ given by the union of these lines.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) -- (0,0.5) -- (1,-0.5) -- (1,-1);
\draw[shorten >= 0.5em] (-0.2,-1) -- (0.6,-0.1);
\draw[shorten <= 0.5em] (0.6,-0.1) -- (1.2,-0.1 + 27/40); 
\end{tikzpicture}

\end{figuretikz}

\end{notn}

\begin{rmk} The picture of Notation \ref{NotationUnderCrossing} is with respect to the same coordinate axes as in Remark \ref{RemarkDepictionOfOverCrossings}. From a bird's eye point of view, looking down onto the $x$-$y$-plane, we again see the following.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-0.75,0) -- (0.75,0);
\draw (0,-0.75) -- (0,0.75);

\end{tikzpicture}

\end{figuretikz} %
%
The `crossing point' is $(a,b)$. 

We cannot see that horizontal line segment passes under the vertical line segment. To indicate this, and thereby to distinguish between $\knotundercross{(a,b)}$ and $\knotcross{(a,b)}$, we shall depict $\knotundercross{(a,b)}$ from a bird's eye point of view as follows.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0.75,0) -- (0.75,1.5);
\draw[shorten >= 0.5em] (0,0.75) -- (0.75,0.75);
\draw[shorten <= 0.5em] (0.75,0.75) -- (1.5,0.75);

\end{tikzpicture}

\end{figuretikz} 

\end{rmk}

\begin{rmk} In words: if we think of $\knotcross{(a,b)}$ as lying in the $x$-$y$-plane in $\reals^{3}$, then $\knotundercross{(a,b)}$ is obtained from $\knotcross{(a,b)}$ by replacing the vertical line segment by a `croquet hoop'. \end{rmk}

\begin{terminology} Let $K$ be a subset of $\reals^{2}$. Suppose that $(a,b)$ belongs to $K$, and that $\knotcross{(a,b)}$ is a subset of $K$. A {\em crossing replacement} in $K$ is the subset of $\reals^{3}$ obtained from $K \times \{0\}$ by replacing $\knotcross{(a,b)} \times \{0\}$ by $\knotovercross{(a,b)}$ or $\knotundercross{(a,b)}$. 

In other words: the set \[ \left( (K \setminus \knotcross{(a,b)}) \times \{0\} \right) \cup \knotovercross{(a,b)} \] or the set \[ \left( (K \setminus \knotcross{(a,b)}) \times \{0\} \right) \cup \knotundercross{(a,b)}. \] \end{terminology}

\begin{example} \label{ExampleTrefoil} Let $L_{i}$ for $1 \leq i \leq 8$ be the lines in $\reals^{2}$ given by the set of $(x,y)$ in $\reals^{2}$ which satisfy the indicated conditions.  

\begin{center}
\begin{tabular}{lll}
\toprule
Line & Condition on $x$ & Condition on $y$ \\
\midrule
$L_{1}$ & $-2 \leq x \leq 2$ & $y=1$ \\
$L_{2}$ & $x=2$              & $-2 \leq y \leq 1$ \\
$L_{3}$ & $-1 \leq x \leq 2$ & $y=-2$ \\
$L_{4}$ & $x=-1$             & $-2 \leq y \leq 1$\\
$L_{5}$ & $-1 \leq x \leq 1$ & $y=1$ \\
$L_{6}$ & $x=1$              & $-1 \leq y \leq 1$ \\
$L_{7}$ & $-2 \leq x \leq 1$ & $y=-1$ \\
$L_{8}$ & $x=-2$             & $-1 \leq y \leq 1$  \\
\bottomrule
\end{tabular}
\end{center} %
%
Let $\knotdiagram{3,1}$ be the subset of $\reals^{2}$ given by the union of these lines.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-1,0) -- (3,0) -- (3,-3) -- (0,-3) -- (0,1) -- (2,1) -- (2,-2) -- (-1,-2) -- (-1,0); 

\end{tikzpicture}

\end{figuretikz} %
%
Let $K$ be the subset of $\reals^{3}$ obtained from $\knotdiagram{3,1}$ by making the following crossing replacements. 

\begin{center}
\begin{tabular}{lll}
\toprule
$(a,b)$ & $\knotovercross{(a,b)}$ & $\knotundercross{(a,b)}$ \\
\midrule
$(-1,1)$ & \tick &         \\
$(1,1)$ &         & \tick \\
$(-1,-1)$ &        & \tick \\
\bottomrule
\end{tabular}
\end{center} % 
%
Then $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. This is the topic of Task \ref{TaskTrefoilIsAKnot}. Thus $K$ is a knot. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[shorten >= 0.5em] (-1,0) -- (2,0);
\draw[shorten <= 0.5em, shorten >= 0.5em] (2,0)  -- (3,0) -- (3,-3) -- (0,-3) -- (0,-2) -- (0,0);
\draw[shorten <= 0.5em, shorten >= 0.5em] (0,0) -- (0,1) -- (2,1) -- (2,-2) -- (0,-2); 
\draw[shorten <= 0.5em] (0,-2) -- (-1,-2) -- (-1,0);
 
\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{terminology} We refer to the knot $K$ of Example \ref{ExampleUnknotInPiecewiseLinearCategory} as the {\em trefoil}. \end{terminology}

\begin{notn} We shall denote the trefoil by $\knot{3,1}$. \end{notn}

\begin{example} \label{ExampleFigureOfEightKnot} Let $K$ be a subset of $\reals^{3}$ depicted below. To explicitly define $K$ is one of the topics of Task \ref{TaskFigureOfEightKnot}.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-2) -- (0,0) -- (1,0);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,0) -- (2,0) -- (2,-2) -- (-1,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (-1,-2) -- (-2,-2) -- (-2,1) -- (1,1) -- (1,-1) -- (0,-1);
\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-1) -- (-1,-1) -- (-1,-3) -- (0,-3) -- (0,-2);
 
\end{tikzpicture}

\end{figuretikz} %
%
Then $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. To prove this is also one of the topics of Task \ref{TaskFigureOfEightKnot}. Thus $K$ is a knot. \end{example}

\begin{terminology} We refer to the knot $K$ of Example \ref{ExampleFigureOfEightKnot} as the {\em figure of eight knot}. \end{terminology}

\begin{notn} We shall denote the figure of eight knot by $\knot{4,1}$. \end{notn}

\begin{example} \label{ExampleTrueLoversKnot} Let $K$ be a subset of $\reals^{3}$ depicted below. To explicitly define $K$ is one of the topics of Task \ref{TaskTrueLoversKnot}.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-3) -- (0,-3) -- (0,0) -- (2,0) -- (2,-1);
\draw[shorten <= 0.5em, shorten >= 0.5em] (2,-1) -- (2,-4) -- (1,-4);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-4) -- (-2,-4) -- (-2,-2) -- (-1,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (-1,-2) -- (0,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-2) -- (1,-2) -- (1,-5) -- (-1,-5) -- (-1,-4);
\draw[shorten <= 0.5em, shorten >= 0.5em] (-1,-4) -- (-1,-1) -- (0,-1);
\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-1) -- (3,-1) -- (3,-3) -- (2,-3);
\draw[shorten <= 0.5em, shorten >= 0.5em] (2,-3) -- (1,-3);
 
\end{tikzpicture}

\end{figuretikz} %
%
Then $(K,\curlyo_{K})$ is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. To prove this is also one of the topics of Task \ref{TaskTrueLoversKnot}. Thus $K$ is a knot. 

\end{example}

\begin{terminology} We refer to the knot $K$ of Example \ref{ExampleTrueLoversKnot} as the {\em true lovers' knot}. Can you see why? Look for two hearts! \end{terminology}

\begin{notn} We shall denote the true lovers' knot by $\knot{8,19}$. \end{notn}

\section{Links}

\begin{defn} A {\em link} is a subset $L$ of $\reals^{3}$ such that $L=K_{1} \sqcup \ldots \sqcup K_{n}$ for some $n \geq 1$, and $K_{i}$ is a knot for every $1 \leq i \leq n$. \end{defn}

\begin{rmk} Every knot is a link. \end{rmk}

\begin{example} \label{ExampleUnlinkWithNComponents} Suppose that $n$ belongs to $\mathbb{N}$. Let $L$ be a subset of $\reals^{3}$ consisting of $n$ rectangles in the $x$-$y$-plane, as depicted below. To explicitly define $L$ is one of the topics of Task \ref{TaskUnlinkWithNComponents}.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small, decoration={markings,
  mark=between positions 0 and 1 step 0.4em
  with { \draw [fill] (0,0) circle [radius=0.02];}}]

\draw (-1,-1) rectangle (1,1);
\draw (2,-1) rectangle (4,1);

\path[postaction={decorate}] (4.75,0) to (5.25,0);

\draw (6,-1) rectangle (8,1);
\draw (9,-1) rectangle (11,1);

\end{tikzpicture}

\end{figuretikz} %
%
Each of the $n$ rectangles, equipped with the subspace topology with respect to $(\reals^{3},\curlyo_{\reals^{3}})$, is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. To prove this is again one of the topics of Task \ref{TaskUnlinkWithNComponents}. Thus $L$ is a link.
 
\end{example}

\begin{terminology} We refer to the link $L$ of Example \ref{ExampleUnlinkWithNComponents} as the {\em unlink with $n$ components}. \end{terminology}

\begin{notn} We shall denote the unlink with $n$ components by $\link{0,n,1}$. \end{notn}

\begin{example} \label{ExampleHopfLink} Let $\linkdiagram{2,2,1}$ be a subset of $\reals^{2}$ given by a union of eight straight lines as depicted below. To explicitly define $\linkdiagram{2,2,1}$ is one of the topics of Task \ref{TaskHopfLink}.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (-1,-1) rectangle (1,1);
\draw (0,0) rectangle (2,2);
 
\end{tikzpicture}

\end{figuretikz} %
%
Let $L$ be the subset of $\reals^{3}$ depicted below, obtained from $\linkdiagram{2,2,1}$ by performing two crossing replacements.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[shorten <= 0.5em, shorten >= 0.5em] (0,0) -- (1,0) -- (1,-2) -- (-1,-2) -- (-1,0) -- (0,0); 
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-1) -- (2,-1) -- (2,1) -- (0,1) -- (0,-1) -- (1,-1); 

\end{tikzpicture}

\end{figuretikz} %
%
To explicitly define $L$ is also one of the topics of Task \ref{TaskHopfLink}. Each of the two rectangles, equipped with the subspace topology with respect to $(\reals^{3},\curlyo_{\reals^{3}})$, is homeomorphic to $(S^{1},\curlyo_{S^{1}})$. To prove this is again one of the topics of Task \ref{TaskHopfLink}. Thus $L$ is a link.
 
\end{example}

\begin{terminology} We refer to the link $L$ of Example \ref{ExampleHopfLink} as the {\em Hopf link}. \end{terminology}

\begin{notn} We shall denote the Hopf link by $\link{2,2,1}$. \end{notn}

\begin{example} \label{ExampleWhiteheadLink} Let $L$ be a subset of $\reals^{3}$ as depicted below. To explicitly define $L$ is one of the topics of Task \ref{TaskWhiteheadLink}.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-2) -- (0,0) -- (2,0) -- (2,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (2,-2) -- (2,-4) -- (0,-4) -- (0,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-1) -- (1,-1) -- (1,-3) -- (2,-3);
\draw[shorten <= 0.5em, shorten >= 0.5em] (2,-3) -- (3,-3) -- (3,-2) -- (1,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-2) -- (-1,-2) -- (-1,-1) -- (0,-1);

\end{tikzpicture}

\end{figuretikz} %
%
Then $L$, equipped with the subspace topology with respect to $(\reals^{3},\curlyo_{\reals^{3}})$, is a disjoint union of two knots. To prove this is also one of the topics of Task \ref{TaskWhiteheadLink}. Thus $L$ is a link.
 
\end{example}

\begin{terminology} We refer to the link $L$ of Example \ref{ExampleWhiteheadLink} as the {\em Whitehead link}. \end{terminology}

\begin{notn} We shall denote the Whitehead link by $\link{5,2,1}$. \end{notn}

\begin{example} \label{ExampleBorromeanRings} Let $L$ be a subset of $\reals^{3}$ as depicted below. To explicitly define $L$ is one of the topics of Task \ref{TaskBorromeanRings}.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[shorten <= 0.5em, shorten >= 0.5em] (0,-2) -- (0,0) -- (4,0) -- (4,-3) -- (2,-3);
\draw[shorten <= 0.5em, shorten >= 0.5em] (2,-3) -- (0,-3) -- (0,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-4) -- (-1,-4) -- (-1,-2) -- (1,-2);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-2) -- (2,-2) -- (2,-4) -- (1,-4);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1,-3) -- (1,-1) -- (3,-1) -- (3,-3);
\draw[shorten <= 0.5em, shorten >= 0.5em] (3,-3) -- (3,-5) -- (1,-5) -- (1,-3);

\end{tikzpicture}

\end{figuretikz} %
%
Then $L$, equipped with the subspace topology with respect to $(\reals^{3},\curlyo_{\reals^{3}})$, is a disjoint union of three knots. To prove this is also one of the topics of Task \ref{TaskBorromeanRings}. Thus $L$ is a link.
 
\end{example}

\begin{terminology} We refer to the link $L$ of Example \ref{ExampleBorromeanRings} as the {\em Borromean rings}. \end{terminology}

\begin{notn} We shall denote the Borromean rings by $\link{6,3,2}$. \end{notn}

\begin{rmk} The Borromean rings have the intriguing property that no two of the individual rings are linked! This is the topic of Task \ref{TaskNoTwoRingsInTheBorromeanRingsAreLinked}. This kind of link is known as a {\em Brunnian link}. One of the other members of the Geometry/Topology group, Professor Nils Baas, offers Master and other projects on Brunnian links. \end{rmk} 

\section{Isotopy}

\begin{defn} A knot $K_{1}$ is obtained from a knot $K_{0}$ by a {\em triangle move} if there is a solid triangle $\Delta$ in $\reals^{3}$ such that $\Delta \cap K_{0}$ is the union of two of the edges of $\Delta$, and such that $\Delta \cap K_{1}$ is exactly the other edge of $\Delta$, or vice versa. \end{defn}

\begin{example} Let $K_{1}$ be the knot depicted below. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) -- (1.5,0) -- (

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{defn} Let $K_{0}$ and $K_{1}$ be knots. Then $K_{0}$ is {\em isotopic} to $K_{1}$  \end{defn}

\section{Wild knots}

\begin{example} \label{ExampleWildKnot} Let $K$ be a subset of $\reals^{3}$ as depicted below. To describe $K$ explicitly, and to prove that it is homeomorphic to $(S^{1},\curlyo_{S^{1}})$, is the topic of Task \ref{TaskWildKnot}. Thus $K$ is a knot.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small, decoration={markings,
  mark=between positions 0 and 1 step 0.4em
  with { \draw [fill] (0,0) circle [radius=0.02];}}]

\path[postaction={decorate}] (0.75,-0.25) to (1.25,0.25);

\draw[shorten >= 0.5em] (0.5,-0.5) -- (6.5,-0.5) -- (6.5,4) -- (4.5,4);

\draw[shorten <= 0.5em] (1.5,1) -- (1,1) -- (1,1.5) -- (1.5,1.5) -- (1.5,0.5);
\draw[shorten <= 0.5em, shorten >= 0.5em] (1.5,1) -- (2.5,1) -- (2.5,3) -- (1.5,3) -- (1.5,2) -- (2.5,2); 
\draw[shorten <= 0.5em, shorten >= 0.5em] (2.5,2) -- (4.5,2) -- (4.5,6) -- (2.5,6) -- (2.5,4) -- (4.5,4);
 
\end{tikzpicture}

\end{figuretikz} %
%
Then $K$ would be isotopic to the unknot by a sequence of $\reidemeisterone$ moves, if it were not for the fact that this would take an infinite length of time! 
\end{example}
 
\begin{rmk} We shall not consider knots of this kind, known as {\em wild knots}. We shall restrict our attention to knots obtained by glueing together finitely many straight lines. \end{rmk} 

\begin{comment}

\begin{examples}

\begin{itemize}

\item[(1)] Torus. Do also with glueing diagram. Keep track of discs as go through, and glue on handles afterwards. Euler characteristic is $2 - 2 = 0$.  

\item[(2)] Handlebody with three holes. Do also with glueing diagram. Euler characteristic is $2-6=-4$. 

\item[(3)] Sphere with tunnels. Euler characteristic is $2-6=-4$.   

\item[(4)] One on the front cover of book of Armstrong. Euler characteristic is $2-4=-2$.

\item[(5)] Klein bottle (glueing diagram). Euler characteristic is $2-2=0$.  

\end{itemize}

\end{examples}

\begin{rmk} Difference between homeomorphism and isotopy. Example of the dogbone (on cover of Armstrong's book). One which cannot. Leads to knot theory.  \end{rmk}

\begin{rmk} Knot theory as in Lecture 17. Give knot diagrams a more intuitive treatment. Explain that not necessarily obvious how to use the Reidemister moves to distinguish between knots (see notes). \end{rmk}

\end{comment}
