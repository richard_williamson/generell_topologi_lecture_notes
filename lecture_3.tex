\setcounter{chapter}{2}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 13th January}

\section{Product topologies}

\begin{rmk} In this section, we discuss our second `canonical way' to construct topological spaces. \end{rmk}

\begin{defn} \label{DefinitionProductTopology} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let $\curlyo_{X \times Y}$ denote the set of subsets $U$ of $X \times Y$ with the property that, for every $(x,y) \in U$, there is a subset $U_{X}$ of $X$ and a subset $U_{Y}$ of $Y$ with the following properties.

\begin{itemize}

\item[(1)] We have that $x \in U_{X}$, and that $U_{X}$ belongs to $\curlyo_{X}$.

\item[(2)] We have that $y \in U_{Y}$, and that $U_{Y}$ belongs to $\curlyo_{Y}$.

\item[(3)] We have that $U_{X} \times U_{Y} \subset U$. 

\end{itemize}

\end{defn}

\begin{prpn} \label{PropositionProductTopology} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Then $(X \times Y, \curlyo_{X \times Y})$ is a topological space. \end{prpn}

\begin{proof} We verify that each of the conditions of Definition \ref{DefinitionTopologicalSpace} holds. 

\begin{itemize}

\item[(1)] The empty set $\emptyset$ belongs to $\curlyo_{X \times Y}$, since the required property vacuously holds.

\item[(2)] Let $(x,y) \in X \times Y$. We have the following.

\begin{itemize}

\item[(a)] Since $\curlyo_{X}$ is a topology on $X$, we have that $X$ belongs to $\curlyo_{X}$. Evidently, $x \in X$. 

\item[(b)] Since $\curlyo_{Y}$ is a topology on $Y$, we have that $Y$ belongs to $\curlyo_{Y}$. Evidently, $y \in Y$. 

\item[(c)] We have that $X \times Y \subset X \times Y$.

\end{itemize} %
%
Taking $U_{X}$ to be $X$, and taking $U_{Y}$ to be $Y$, we deduce that $X \times Y$ belongs to $\curlyo_{X \times Y}$.

\item[(3)] Let $\left\{ U_{j} \right\}_{j \in J}$ be a set of subsets of $X \times Y$ which belong to $\curlyo_{X \times Y}$. Let $(x,y) \in \bigcup_{j \in J} U_{j}$. By definition of $\bigcup_{j \in J} U_{j}$, there is a $j \in J$ such that $(x,y) \in U_{j}$.  

By definition of $\curlyo_{X \times Y}$, there is a subset $U_{X}$ of $X$ and a subset $U_{Y}$ of $Y$ with the following properties.     

\begin{itemize}

\item[(a)] We have that $x \in U_{X}$, and that $U_{X}$ belongs to $\curlyo_{X}$.

\item[(b)] We have that $y \in U_{Y}$, and that $U_{Y}$ belongs to $\curlyo_{Y}$.

\item[(c)] We have that $U_{X} \times U_{Y} \subset U_{j}$. 

\end{itemize} %
%
We have that $U_{j} \subset \bigcup_{j \in J} U_{j}$. By (c), we deduce that $U_{X} \times U_{Y} \subset \bigcup_{j \in J} U_{j}$. We conclude from the latter, (a), and (b), that $\bigcup_{j \in J} U_{j}$ belongs to $\curlyo_{X \times Y}$.

\item[(4)] Let $U_{0}$ and $U_{1}$ be subsets of $X \times Y$ which belong to $\curlyo_{X \times Y}$. Let $(x,y) \in U_{0} \cap U_{1}$. By definition of $\curlyo_{X \times Y}$, there is a subset $U_{0}^{X}$ of $X$ and a subset $U_{0}^{Y}$ of $Y$ with the following properties. 

\begin{itemize}

\item[(a)] We have that $x \in U_{0}^{X}$, and that $U_{0}^{X}$ belongs to $\curlyo_{X}$.

\item[(b)] We have that $y \in U_{0}^{Y}$, and that $U_{0}^{Y}$ belongs to $\curlyo_{Y}$.

\item[(c)] We have that $U_{0}^{X} \times U_{0}^{Y} \subset U_{0}$. 

\end{itemize} %
%
Moreover, by definition of $\curlyo_{X \times Y}$, there is a subset $U_{1}^{X}$ of $X$ and a subset $U_{1}^{Y}$ of $Y$ with the following properties. 

\begin{itemize}

\item[(d)] We have that $x \in U_{1}^{X}$, and that $U_{1}^{X}$ belongs to $\curlyo_{X}$.

\item[(e)] We have that $y \in U_{1}^{Y}$, and that $U_{1}^{Y}$ belongs to $\curlyo_{Y}$.

\item[(f)] We have that $U_{1}^{X} \times U_{1}^{Y} \subset U_{1}$. 

\end{itemize} %
%
We deduce the following.

\begin{itemize}

\item[(i)] By (a) and (d), we have that $x \in U_{0}^{X} \cap U_{1}^{X}$. Moreover, since $\curlyo_{X}$ defines a topology on $X$, we have by (a) and (d) that $U_{0}^{X} \cap U_{1}^{X}$ belongs to $\curlyo_{X}$.

\item[(ii)] By (b) and (e), we have that $y \in U_{0}^{Y} \cap U_{1}^{Y}$. Moreover, since $\curlyo_{Y}$ defines a topology on $Y$, we have by (b) and (e) that $U_{0}^{Y} \cap U_{1}^{Y}$ belongs to $\curlyo_{Y}$.

\item[(iii)] We have that \[ \left( U_{0}^{X} \cap U_{1}^{X} \right) \times \left( U_{0}^{Y} \cap U_{1}^{Y} \right) = \left( U_{0}^{X} \times U_{0}^{Y} \right) \cap \left( U_{1}^{X} \times U_{1}^{Y} \right). \] By (c) and (f), we have that \[ \left( U_{0}^{X} \times U_{0}^{Y} \right) \cap \left( U_{1}^{X} \times U_{1}^{Y} \right) \subset U_{0} \cap U_{1}. \] Hence \[ \left( U_{0}^{X} \cap U_{1}^{X} \right) \times \left( U_{0}^{Y} \cap U_{1}^{Y} \right) \subset U_{0} \cap U_{1}. \] 

\end{itemize} %
%
Taking $U_{X}$ to be $U_{0}^{X} \cap U_{1}^{X}$, and taking $U_{Y}$ to be $U_{0}^{Y} \cap U_{1}^{Y}$, we conclude that $U_{0} \cap U_{1}$ belongs to $\curlyo_{X \times Y}$.

\end{itemize}
 
\end{proof}

\begin{rmk} This proof has much in common with the proof of Proposition \ref{PropositionStandardTopologyDefinesATopology} and the proof of Proposition \ref{PropositionSubspaceTopology}. Perhaps you can begin to see how to approach a proof of this kind? Again, it is a very good idea to work on the proof until you thoroughly understand it. This is the topic of Task \ref{TaskProductTopologyConstruction}. \end{rmk}

\section{\texorpdfstring{The product topology on $\reals^{2}$}{The product topology on the real plane}}

\begin{defn} Let $\curlyo_{\reals^{2}}$ denote the product topology on $\reals^{2}$ with respect to two copies of $(\reals,\streals)$. \end{defn}

\begin{example} \label{ExampleProductOfOpenIntervalsIsOpenInR2} Let $U_{0} = \opint{a_{0},b_{0}}$, and let $U_{1} = \opint{a_{1},b_{1}}$ be open intervals. Let $(x,y) \in U_{0} \times U_{1}$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) rectangle (4,2);
\draw[dashed] (0,0) rectangle (4,2);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{0}$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$b_{0}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{1}$};

\draw (0,2) -- (-0.15,2);
\node at (-0.5,2) {$b_{1}$};

\fill (1,1) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} % 
%
By Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, both $U_{0}$ and $U_{1}$ belong to $\curlyo_{\reals}$. We deduce that $U_{0} \times U_{1}$ belongs to $\stRtwo$, since we can take $U_{X}$ to be $U_{0}$, and can take $U_{Y}$ to be $U_{1}$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\draw[dashed, green] (0,0) rectangle (4,2);

\draw (0,0) -- (0,-0.15);
\node at (0,-0.5) {$a_{0}$};

\draw (4,0) -- (4,-0.15);
\node at (4,-0.5) {$b_{0}$};

\draw (0,0) -- (-0.15,0);
\node at (-0.5,0) {$a_{1}$};

\draw (0,2) -- (-0.15,2);
\node at (-0.5,2) {$b_{1}$};

\fill (1,1) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} %

\end{example}

\begin{caution} In the figures, the dashed boundary does {\em not} belong to $U_{0} \times U_{1}$. We shall adopt the same convention in all our figures. \end{caution}

\begin{example} \label{ExampleOpenDiscOpenInStandardTopologyOnR2} Let $U$ denote the disc \[ \left\{ (x,y) \in \reals^{2} \mid \| (x,y) \| < 1 \right\}. \] 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green]  (0,0) circle[radius=1.5];
\draw[dashed] (0,0) circle[radius=1.5];

\end{tikzpicture}

\end{figuretikz} %
%
Let $(x,y)$ be a point of $U$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\draw[dashed] (0,0) circle[radius=1.5];

\fill (0.5,0.5) circle[radius=0.05];

\end{tikzpicture}

\end{figuretikz} % 
%
Let $\epsilon$ be a real number such that \[ 0 < \epsilon < 1 - \left\| (x,y) \right\|. \] Let $U_{X}$ denote the open interval \[ \opint{x - \tfrac{\epsilon \sqrt{2}}{2}, x + \tfrac{\epsilon \sqrt{2}}{2}}. \] Let $U_{Y}$ denote the open interval \[ \opint{y - \tfrac{\epsilon \sqrt{2}}{2}, y + \tfrac{\epsilon \sqrt{2}}{2}}. \] %
%
We have that $x \in U_{X}$, and that $y \in U_{Y}$. Let $(x',y')$ be a point of $U_{X} \times U_{Y}$. Then 

\begin{align*} %
%
\left\| (x',y') \right\| &= \left\| \left( \left| x' \right| , \left| y' \right| \right) \right\| \\
                         &< \left\| \left( \left| x \right| + \tfrac{\epsilon \sqrt{2}}{2}, \left| y \right| + \tfrac{\epsilon \sqrt{2}}{2} \right) \right\| \\
                         &\leq \left\| \left( \left| x \right|, \left| y \right| \right) \right\| + \left\| \left( \tfrac{\epsilon \sqrt{2}}{2} , \tfrac{\epsilon \sqrt{2}}{2} \right) \right\| \\
                         &= \left\| \left( x ,  y \right) \right\| + \epsilon \\
                         &< \left\| \left( x ,  y \right) \right\| + \left( 1 - \left\| \left( x , y \right) \right\| \right) \\
                         &= 1. 
\end{align*} %
%
Thus $U_{X} \times U_{Y} \subset U$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\draw[dashed, name path=opencircle] (0,0) circle[radius=1.5];

\fill (0.5,0.5) circle[radius=0.05];

\draw[dashed, green] (0.1,0.1) rectangle (0.9,0.9);

\end{tikzpicture}

\end{figuretikz} %
%
By Example \ref{ExampleOpenIntervalIsOpenInStandardTopology}, both $U_{0}$ and $U_{1}$ belong to $\curlyo_{\reals}$. We conclude that $U$ belongs to $\curlyo_{\reals^{2}}$.    

\end{example}

\begin{rmk} There are very many subsets $U$ of $\reals^{2}$ which belong to $\stRtwo$. We just have to be able to find a small enough `open rectangle' around every point of $U$ which is contained in $U$. \end{rmk} 

\begin{example} \label{ExampleOpenStarBelongsToStandardTopologyOnR2} An `open star' belongs to $\stRtwo$.   

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\node[star, star points=6, star point height=0.6cm, minimum size=8em, fill=green, draw=black, dashed] at (0,0) {};

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} An `open ladder' belongs to $\stRtwo$. 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill[green] (-0.5,-1) -- (-0.5,3.5) -- (0,3.5) -- (0,3) -- (1.75,3) -- (1.75,3.5) -- (2.25,3.5) -- (2.25,-1) -- (1.75,-1) -- (1.75,-0.5) -- (0,-0.5) -- (0,-1) -- (-0.5,-1);

\draw[dashed] (-0.5,-1) -- (-0.5,3.5) -- (0,3.5) -- (0,3) -- (1.75,3) -- (1.75,3.5) -- (2.25,3.5) -- (2.25,-1) -- (1.75,-1) -- (1.75,-0.5) -- (0,-0.5) -- (0,-1) -- (-0.5,-1);

\fill[white] (0,0) rectangle (1.75,0.5);
\fill[white] (0,1) rectangle (1.75,1.5);
\fill[white] (0,2) rectangle (1.75,2.5);

\draw[dashed] (0,0) rectangle (1.75,0.5);
\draw[dashed] (0,1) rectangle (1.75,1.5);
\draw[dashed] (0,2) rectangle (1.75,2.5);

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} \label{ExampleOpenBlobBelongsToStandardTopologyOnR2} An `open blob' belongs to $\stRtwo$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth]

\fill[green,scale=0.75] plot [smooth cycle] coordinates {(-0.5,-1) (0.5,0.2) (1.5,-1) (3.2,-0.1) (2.5,2.5) (1.5,2) (0.5,2.75) (-0.5,1.75) (-1.4,2) (-1.2,0.75) (-1.3,-0.1)};

\draw[dashed,scale=0.75] plot [smooth cycle] coordinates {(-0.5,-1) (0.5,0.2) (1.5,-1) (3.2,-0.1) (2.5,2.5) (1.5,2) (0.5,2.75) (-0.5,1.75) (-1.4,2) (-1.2,0.75) (-1.3,-0.1)};

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} The open half plane given by \[ \left\{ (x,y) \in \reals^{2} \mid x > 0 \right\}  \] belongs to $\stRtwo$.     

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small, decoration={markings,
  mark=between positions 0 and 1 step 4pt
  with { \draw [fill,green] (0,0) circle [radius=0.5pt];}}]

\fill[green]  (0,0) rectangle (4,5);
\draw[dashed] (0,0) -- (0,5);

\path[postaction={decorate}] (2,5.25) -- (2,5.75); 
\path[postaction={decorate}] (2,-0.25) -- (2,-0.75); 
\path[postaction={decorate}] (4.25,2.5) -- (4.75,2.5); 

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} The union of two `open infinite wedges' belongs to $\stRtwo$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small, decoration={markings,
  mark=between positions 0 and 1 step 4pt
  with { \draw [fill,green] (0,0) circle [radius=0.5pt];}}]

\fill[green] (-5,1) -- (0,0) -- (-3,3);
\draw[dashed] (-5,1) -- (0,0) -- (-3,3);

\fill[green] (5.5,1) -- (0.5,0) -- (3.5,3);
\draw[dashed] (5.5,1) -- (0.5,0) -- (3.5,3);

\path[postaction={decorate}] (-4.75,2.4) -- (-5.5,3); 
\path[postaction={decorate}] (5.25,2.4) -- (6,3); 

\end{tikzpicture}

\end{figuretikz} 

\end{example}

\begin{example} Let $X$ denote the subset of $\reals^{2}$ given by \[ \left\{ (x,y) \in \reals^{2} \mid \text{$0 < x < 1$ and $y=0$} \right\}. \] 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw[yellow] (0,0) -- (2,0);

\end{tikzpicture}

\end{figuretikz} %
%
Let $x$ be any point of $X$. 

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw[yellow] (0,0) -- (2,0);
\fill (1.25,0) circle[radius=0.05];

\node at (1.25,-0.25) {$x$};

\end{tikzpicture}

\end{figuretikz} % 
%
No matter how small a rectangle we take around $x$, there will always be a point of $\reals^{2}$ inside this rectangle which does not belong to $X$. Thus $X$ does not belong to $\stRtwo$.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw[yellow] (0,0) -- (2,0);
\fill (1.25,0) circle[radius=0.05];

\draw[dashed, green] (1,-0.25) rectangle (1.5,0.25);

\end{tikzpicture}

\end{figuretikz} 
 
\end{example}

\begin{example} Let $X$ denote the `half open strip' given by $\lhopint{0,1} \times \opint{0,1}$.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\fill[green] (0,0) rectangle (2,1);

\draw[dashed] (0,0) -- (2,0);
\draw[dashed] (0,1) -- (2,1);

\draw (0,0) -- (0,1);
\draw (2,0) -- (2,1); 

\end{tikzpicture}

\end{figuretikz} %
%
The solid part of the boundary of this figure belongs to $X$. Let $(x,y)$ belong to either of the vertical boundary lines. For example, we can take $(x,y)$ to be $(0,\frac{1}{2})$.  

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw[dashed] (0,0) -- (2,0);
\draw[dashed] (0,1) -- (2,1);

\draw (0,0) -- (0,1);
\draw (2,0) -- (2,1); 

\fill (0,0.5) circle[radius=0.05];
\node at (-0.25,0.5) {$x$};

\end{tikzpicture}

\end{figuretikz} %
%
No matter how small a rectangle we take around $(x,y)$, there will always be a point inside this rectangle which does not belong to $X$. For example, if $(x,y)$ is $(0,\frac{1}{2})$, there will always be a point $(x',y')$ inside this rectangle such that $x' < 0$. Thus $X$ does not belong to $\stRtwo$.  

\begin{figuretikz}

\begin{tikzpicture}[>=stealth, font=\small]

\draw[dashed] (0,0) -- (2,0);
\draw[dashed] (0,1) -- (2,1);

\draw (0,0) -- (0,1);
\draw (2,0) -- (2,1); 

\fill (0,0.5) circle[radius=0.05];

\draw[green,dashed] (-0.25,0.25) rectangle (0.25,0.75);

\end{tikzpicture}

\end{figuretikz} 
\end{example}
