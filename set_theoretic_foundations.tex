\chapter{Set theoretic foundations}

\begin{comment}

\section{Logic}

\begin{rmk} A foundation for mathematics is built upon what we might call a {\em formal language}. This is, in essence, a collection of symbols which we allow ourselves to make use of, together with rules for how to construct `strings' of these symbols from strings which we have already constructed.  
\end{rmk}

\begin{defn} We shall make use of symbols in two ways.

\begin{itemize}

\item[(1)] Symbols which play a {\em structural} role in our formal language: $\vdash$, $:$, $\forall$, $\exists$, $=$, $\mathsf{Set}$, and $\mathsf{Prop}$.

\item[(2)] Symbols which we use for {\em labelling}. We could allow anything that we wish here, but, for concreteness, the following will suffice for everything in the course: lowercase and uppercase letters of the English and Greek alphabets; the blackboard symbols $\mathbb{N}$, $\mathbb{Z}$, $\mathbb{Q}$, and $\mathbb{R}$; the symbol $\curlyo$; the digits $0, 1, 2, 3, 4, 5,6, 7, 8, 9$; and the symbols

%\begin{comment} $'$, $($, $)$, $[$, $]$, $\sim$, $\cup$, $\cap$, $\sqcup$, $/$, $\$, $\sigma$. \end{comment}

%Remark that could also call the structural symbols something different.

\end{itemize}
\end{defn}

\begin{rmk} The only point of significance in the list of symbols for labelling is that this list is finite. We could, for instance, give it to a computer. \end{rmk} 

\end{comment}

\section{Set theoretic equalities and relations}

\begin{rmk} \label{RemarkSetTheoreticEqualities} Throughout the course, we shall make use of various set theoretic equalities and relations. Table \ref{TableSetTheoreticEqualities} and Table \ref{TableSetTheoreticRelations} list many of these. \end{rmk}

\begin{rmk} Here is one more set theoretic identity which does not fit into Table \ref{TableSetTheoreticEqualities}! Given a set $X$, a set $Y$, a subset $A$ of $X$, and a subset $B$ of $Y$, we have that \[ (X \times Y) \setminus (A \times B) = \left( \left(X \setminus A \right) \times B \right) \cup \left( A  \times \left(Y \setminus B \right) \right) \cup \left( \left(X \setminus A \right) \times  \left(Y \setminus B \right) \right). \] \end{rmk}

\section{Injections, surjections, and bijections}

\begin{defn} Let $X$ and $Y$ be sets. A map \ar{X,Y,f} is an {\em injection}, or {\em injective}, if, for every $x_{0}$ and $x_{1}$ which belong to $X$ such that $f(x_{0}) = f(x_{1})$, we have that $x_{0} = x_{1}$. \end{defn}

\begin{prpn} \label{PropositionCompositionOfInjectionsIsAnInjection} Let $X$, $Y$, and $Z$ be sets. Let \ar{X,Y,f} and \ar{Y,Z,g} be injections. Then \ar{X,Z,g \circ f} is an injection. \end{prpn}

\begin{proof} Suppose that $x_{0}$ and $x_{1}$ belong to $X$, and that $g\left(f(x_{0})\right) = g\left(f(x_{1})\right)$. Since $g$ is injective, we deduce that $f(x_{0}) = f(x_{1})$. Since $f$ is injective, we deduce that $x_{0} = x_{1}$. \end{proof}

\begin{rmk} \label{RemarkInclusionMapIsAnInjection} Let $X$ be a set. Let $A$ be a subset of $X$. Let \ar{A,X,i} be the inclusion map of Terminology \ref{TerminologyInclusionMap}. Then $i$ is an injection. \end{rmk}

\begin{defn} Let $X$ and $Y$ be sets. A map \ar{X,Y,f} is a {\em surjection}, or {\em surjective}, if, for every $y$ which belongs to $Y$, there is an $x$ which belongs to $X$ such that $f(x) = y$. \end{defn}

\begin{prpn} \label{PropositionCantorBernsteinSchroeder} Let $X$ and $Y$ be sets. Suppose that there exists an injection \ar{X,{Y,},f} and that there exists an injection \ar{Y,{X.},g} Then there exists a bijection \ar{X,{Y.}} \end{prpn}

\begin{proof} Let $A_{1}$ be the subset $X \setminus g(Y)$ of $X$. For every $n$ which belongs to $\mathbb{N}$, let $A_{n}$ be the subset of $Y$ given by $g\left(f(A_{n-1})\right)$.  

Suppose that $x$ belongs to $X$, and that $x$ does not belong to $A_{1}$. Then $x$ belongs to $g(Y)$. Since $g$ is injective, we deduce that there is a unique $y_{x} \in Y$ such that $g(y_{x}) = x$. 

Let \ar{X,Y,f'} be the map given by \[ x \mapsto \begin{cases} f(x) & \text{if $x$ belongs to $A_{n}$ for some $n \in \mathbb{N}$,} \\ y_{x} & \text{otherwise.} \end{cases} \] %
%
We shall first prove that $f'$ is injective. Suppose that $x_{0}$ and $x_{1}$ belong to $X$, and that $f'(x_{0}) = f'(x_{1})$. Suppose that $x_{1}$ does not belong to $A_{n}$ for any $n \in \mathbb{N}$. Then $f'(x_{1}) = y_{x_{1}}$. Suppose that $x_{0}$ belongs to $A_{m}$ for some $m \in \mathbb{N}$. Then $f'(x_{0}) = f(x_{0})$. We deduce that \[ x_{1} = g\left( y_{x_{1}} \right) = g\left(f'(x_{1})\right) =  g\left(f(x_{0})\right). \] %
%
Thus $x_{1}$ belongs to $A_{m+1}$. This contradicts our assumption on $x_{1}$. We deduce that $x_{0}$ does not belong to $A_{m}$ for any $m \in \mathbb{N}$. Thus $f'(x_{0}) = y_{x_{0}}$. Then \[ x_{0} = g(y_{x_{0}}) = g\left( f'(x_{0}) \right) = g\left( f'(x_{1}) \right) = g(y_{x_{1}}) = x_{1}. \] %
%
An entirely analogous argument demonstrates that if $x_{0}$ does not belong to $A_{n}$ for any $n \in \mathbb{N}$, then $x_{0} = x_{1}$. 

Suppose now that there is an $m \in \mathbb{N}$ such that $x_{0}$ belongs to $A_{n}$, and that there is an $n \in \mathbb{N}$ such that $x_{1}$ belongs to $A_{n}$, Then \[ f(x_{0}) = f'(x_{0}) = f'(x_{1}) = f(x_{1}). \] Since $f$ is injective, we deduce that $x_{0} = x_{1}$. This completes our proof that $f'$ is injective.

We shall now prove that $f$ is surjective. Suppose that $y$ belongs to $Y$. We have that $g(y)$ does not belong to $A_{1}$. For every $n \in \mathbb{N}$, suppose that $y$ does not belong to $f(A_{n})$. Then $g(y)$ does not belong to $A_{n}$ for every $n \in \mathbb{N}$. Thus $f'\left(g(y)\right) = y$. Suppose instead that there is an $n \in \mathbb{N}$ such that $y$ belongs to $f(A_{n})$. Then there is an $x \in A_{n}$ such that $f(x) = y$. This completes our proof that $f'$ is surjective.

We have demonstrated that $f'$ is both injective and surjective. By Task \ref{TaskBijectionIffInjectiveAndSurjective}, we conclude that $f'$ is bijective.
\end{proof}

\begin{caution} Proposition \ref{PropositionCantorBernsteinSchroeder} does not assert that $f$ and $g$ are inverse to each other. Rather, we used $f$ and $g$ to find a new map \ar{X,{Y,}} which we proved to be a bijection. \end{caution}

\begin{rmk} Proposition \ref{PropositionCantorBernsteinSchroeder} is sometimes known as the {\em Cantor-Bernstein-Schr{\"o}der theorem}. \end{rmk}

\section{Coproducts} 

\begin{notn} Let $J$ be a set. For every $j$ which belongs to $J$, let $X_{j}$ be a set. We denote by $\bigsqcup_{j \in J} X_{j}$ the set $\bigcup_{j \in J} \left( X_{j} \times \{j\} \right)$. \end{notn}

\begin{rmk} Suppose that $j_{0}$ and $j_{1}$ belong to $J$. We allow that $X_{j_{0}} = X_{j_{1}}$. \end{rmk}

\begin{defn} \label{DefinitionCoproduct} Let $J$ be a set. For every $j$ which belongs to $J$, let $X_{j}$ be a set. We refer to $\bigsqcup_{j \in J} X_{j}$ as a {\em coproduct}. \end{defn}

\begin{notn} Let $J$ and $X$ be sets. For every $j$ which belongs to $J$, let $X_{j}$ be $X$. We often denote $\bigsqcup_{j \in J} X_{j}$ by $\bigsqcup_{j \in J} X$. \end{notn}

\begin{notn} When $J$ is $\{0,1\}$, we often denote $\bigsqcup_{\{0,1\}} X_{j}$ by $X_{0} \sqcup X_{1}$. In particular, we often denote $\bigsqcup_{j \in \{0,1\}} X$ by $X \sqcup X$. When $J$ is $\{0,1,\ldots,n\}$, we similarly often denote $\bigsqcup_{j \in \{0,1,\ldots,n\}} X_{j}$ by \[ \underbrace{X_{0} \sqcup X_{1} \sqcup \ldots \sqcup X_{n}}_{n}. \] We sometimes also denote $\bigsqcup_{j \in \{0,1,\ldots,n\}} X_{j}$ by $\bigsqcup_{0 \leq j \leq n} X_{j}$. \end{notn}

\begin{caution} It is important to appreciate that $X \sqcup X$ and $X \cup X$ are very different! For $X \cup X$ is $X$, but $X \sqcup X$ can be thought of as `two disjoint copies' of $X$. Think of $T^{2}$. 

\begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (-1.5,0.15) rectangle (1.5,-0.60);

\draw (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (0,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}

\end{tikzpicture}

\end{figuretikz} %
%
One doughnut is very different from two doughnuts!

\begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (-1.5,0.15) rectangle (1.5,-0.60);

\draw (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (0,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}

\draw (5,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (4.5,0.15) rectangle (6.5,-0.60);

\draw (5,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (5,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (5,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}
\end{tikzpicture}

\end{figuretikz} 

\end{caution}

\begin{prpn} \label{PropositionDisjointUnionIsCoproduct} Let $X$ be a set, and let $X_{0}$ and $X_{1}$ be subsets of $X$. Suppose that $X = X_{0} \cup X_{1}$. Moreover, suppose that this union is disjoint, in the sense of Terminology \ref{TerminologyDisjointUnion}. Then there is a bijection between $X$ and the coproduct of $X_{0}$ and $X_{1}$. \end{prpn}

\begin{proof} Let \ar{{\left( X_{0} \times \{0\} \right) \cup \left(X_{1} \times \{1\} \right)}, X,f} be the map given by $(x_{0},0) \mapsto x_{0}$ for every $x_{0}$ which belongs to $X_{0}$, and by $(x_{1},1) \mapsto x_{1}$ for every $x_{1}$ which belongs to $X_{1}$. Let \ar{X,{\left( X_{0} \times \{0\} \right) \cup \left(X_{1} \times \{1\} \right)},g} be the map given by \[ x \mapsto \begin{cases} (x,0) & \text{if $x_{0}$ belongs to $X_{0}$,} \\ (x,1) & \text{if $x_{1}$ belongs to $X_{1}$.} \end{cases} \]  The fact that $X$ is the disjoint union of $X_{0}$ and $X_{1}$ exactly ensures that $g$ is well defined. We have that $g \circ f = id_{\left( X_{0} \times \{0\} \right) \cup \left(X_{1} \times \{1\} \right)}$, and that $f \circ g = id_{X}$.

\end{proof}

\begin{rmk} Proposition \ref{PropositionDisjointUnionIsCoproduct} justifies our use of the same notation for disjoint unions and coproducts. \end{rmk}

\section{Equivalence relations} \label{SectionEquivalenceRelations}

\begin{defn} Let $X$ be a set. A {\em relation} on $X$ is a subset of $X \times X$. \end{defn}

\begin{notn} Let $X$ be a set, and let $R$ be a relation on $X$. Suppose that $x_{0}$ belongs to $X$, that $x_{1}$ belongs to $X$, and that $(x_{0},x_{1})$ belongs to $R$. We write $x_{0} \sim x_{1}$. \end{notn}

\begin{defn} \label{DefinitionEquivalenceRelation} Let $X$ be a set. A relation $R$ on $X$ is an {\em equivalence relation} if the following hold.

\begin{itemize}

\item[(1)] For all $x \in X$, we have that $x \sim x$.

\item[(2)] For all $x_{0} \in X$ and $x_{1} \in X$, such that $x_{0} \sim x_{1}$, we have that $x_{1} \sim x_{0}$.

\item[(3)] For all $x_{0} \in X$, $x_{1} \in X$, and $x_{2} \in X$, such that $x_{0} \sim x_{1}$ and $x_{1} \sim x_{2}$, we have that $x_{0} \sim x_{2}$.

\end{itemize}

\end{defn}

\begin{rmk} Axiom (1) is known as {\em reflexivity}. Axiom (2) is known as {\em symmetry}. Axiom (3) is known as {\em transitivity}. \end{rmk}

\begin{example} Let $X=\{a,b,c\}$ be a set with three elements. We have the following.

\begin{itemize}

\item[(1)] The relation $R$ of $X$ given by \[ \left\{ (a,b), (b,a) \right\} \] is not an equivalence relation. Symmetry and transitivity hold, but reflexivity does not.

\item[(2)] The relation $R$ of $X$ given by \[ \left\{ (a,a), (b,b), (c,c), (b,c) \right\} \] is not an equivalence relation. Reflexivity and transitivity hold, but symmetry does not, since $(c,b)$ does not belong to $R$.

\item[(3)] The relation $R$ of $X$ given by \[ \left\{ (a,a), (b,b), (c,c), (a,c), (c,a), (b,c), (c,b) \right\} \] is not an equivalence relation. Reflexivity and symmetry hold, but transitivity does not, since $(a,c)$ and $(c,b)$ belong to $R$, but $(a,b)$ does not.

\item[(4)] The relation $R$ of $X$ given by \[ \left\{ (a,a), (b,b), (c,c) \right\} \] is an equivalence relation on $R$. It is the relation defined by equality.

\item[(5)] The relation $R$ of $X$ given by \[ \left\{ (a,a), (b,b), (c,c), (a,c), (c,a) \right\} \] is an equivalence relation on $R$. 

\end{itemize}

\end{example}

\begin{notn} \label{NotationEquivalenceRelationGeneratedByARelation} Let $X$ be a set, and let $R$ be a relation on $X$. Let us consider the following subsets of $X \times X$.

\begin{itemize}

\item[(1)] Let $\Delta$ be the subset of $X \times X$ given by \[ \left\{ (x,x) \in X \times X \mid x \in X \right\}. \] 

\item[(2)] Let $R_{\mathsf{sym}}$ denote the set \[ \left\{ (x_{0},x_{1}) \in X \times X \mid (x_{1},x_{0}) \in R \right\}. \] 

\item[(3)] Let $R_{\mathsf{equiv}}$ denote the set of $(x,x') \in X \times X$ such that there are is an integer $n \geq 2$ and an $n$-tuple $(x_{1},\ldots,x_{n}) \in X \times \ldots \times X$ with the following properties.

\begin{itemize}

\item[(a)] We have that $x=x_{1}$.

\item[(b)] We have that $x'=x_{n}$.

\item[(c)] For every $1 \leq i \leq n-1$, we have that $(x_{i},x_{i+1}) \in \Delta \cup R \cup R_{\mathsf{sym}}$.

\end{itemize} 

\end{itemize}

\end{notn}

\begin{rmk} \label{RemarkRelationIsSymmetric} For any $y, y' \in X$, we have that if $(y,y') \in \Delta \cup R \cup R_{\mathsf{sym}}$, then $(y',y) \in \Delta \cup R \cup R_{\mathsf{sym}}$. \end{rmk} 

\begin{prpn} Let $X$ be a set, and let $R$ be a relation on $X$. Then $R_{\mathsf{equiv}}$ defines an equivalence relation on $X$. \end{prpn}

\begin{proof} We verify that the conditions of Definition \ref{DefinitionEquivalenceRelation} hold.

\begin{itemize}

\item[(1)] Let $x \in X$. Since $(x,x) \in \Delta$, and hence $(x,x) \in \Delta \cup R \cup R_{\mathsf{sym}}$. Thus the pair $(x,x)$ exhibits that $(x,x) \in R_{\mathsf{equiv}}$.

\item[(2)] Let $(x,x') \in R_{\mathsf{equiv}}$. By definition of $R_{\mathsf{equiv}}$, there is an integer $n \geq 2$, and an $n$-tuple $(x_{1},\ldots,x_{n}) \in X \times \ldots \times X$, with the following properties.

\begin{itemize}

\item[(a)] We have that $x=x_{1}$.

\item[(b)] We have that $x'=x_{n}$.

\item[(c)] For every $1 \leq i \leq n-1$, we have that $(x_{i},x_{i+1}) \in \Delta \cup R \cup R_{\mathsf{sym}}$.

\end{itemize} %
%
By Remark \ref{RemarkRelationIsSymmetric}, we have, for every $1 \leq n-1$, that $(x_{i+1},x_{i}) \in \Delta \cup R \cup R_{\mathsf{sym}}$. Thus the $n$-tuple $(x_{n},\ldots,x_{1})$ exhibits that $(x',x)$ belongs to $R_{\mathsf{equiv}}$.

\item[(3)] Let $(x,x') \in R_{\mathsf{equiv}}$, and let $(x',x'') \in R_{\mathsf{equiv}}$.

By definition of $R_{\mathsf{equiv}}$, there is an integer $m \geq 2$, and an $m$-tuple $(x_{1},\ldots,x_{m}) \in X \times \ldots \times X$, with the following properties.

\begin{itemize}

\item[(a)] We have that $x=x_{1}$.

\item[(b)] We have that $x'=x_{n}$.

\item[(c)] For every $1 \leq i \leq m-1$, we have that $(x_{i},x_{i+1}) \in \Delta \cup R \cup R_{\mathsf{sym}}$.

\end{itemize} %
%
In addition, there is an integer $n \geq 2$, and an $n$-tuple $(y_{1},\ldots,y_{n}) \in X \times \ldots \times X$, with the following properties.

\begin{itemize}

\item[(a)] We have that $x'=y_{1}$.

\item[(b)] We have that $x''=y_{n}$.

\item[(c)] For every $1 \leq i \leq n-1$, we have that $(y_{i},y_{i+1}) \in \Delta \cup R \cup R_{\mathsf{sym}}$.

\end{itemize} %
%
The $(m+n-1)$-tuple $(x_{1},\ldots,x_{m-1},x_{m}=y_{1},y_{2},\ldots,y_{n})$ exhibits that $(x,x'')$ belongs to $R_{\mathsf{equiv}}$.

\end{itemize} 

\end{proof}

\begin{rmk} Let $X$ be a set, and let $R$ be a relation on $X$. It is straightforward to prove that if $R'$ is a relation on $X$ such that $R \subset R'$, then $R_{\mathsf{equiv}} \subset R'$. In other words, $R_{\mathsf{equiv}}$ is the smallest equivalence relation on $X$ containing $R$. \end{rmk}

\begin{terminology} Let $X$ be a set, and let $R$ be a relation on $X$. We refer to $R_{\mathsf{equiv}}$ as the {\em equivalence relation generated by $R$}. 
\end{terminology}

\begin{rmk} In practise, given $R$, we typically do not determine $R_{\mathsf{equiv}}$ by working directly with the definition given in Notation \ref{NotationEquivalenceRelationGeneratedByARelation}. Rather we just `inductively throw in by hand everything we need to obtain an equivalence relation, but nothing else'!\end{rmk}

\begin{example}  Let $X=\{a,b,c\}$ be a set with three elements. We have the following.

\begin{itemize}

\item[(1)] Let $R$ be the relation on $X$ given by \[ \left\{ (a,b), (b,a) \right\}. \] Then $R_{\mathsf{equiv}}$ is given by \[ \left\{ (a,a), (b,b), (c,c), (a,b), (b,a) \right\}. \] 

\item[(2)] Let $R$ be the relation on $X$ given by \[ \left\{ (a,a), (b,b), (c,c), (b,c) \right\}. \] Then $R_{\mathsf{equiv}}$ is given by \[ \left\{ (a,a), (b,b), (c,c), (b,c), (c,b) \right\}. \] 

\item[(3)] Let $R$ be the relation on $X$ given by \[ \left\{ (a,a), (b,b), (c,c), (a,c), (c,a), (b,c), (c,b) \right\}. \] Then $R_{\mathsf{equiv}}$ is given by \[ \left\{ (a,a), (b,b), (c,c), (a,c), (c,a), (b,c), (c,b), (a,b), (b,a) \right\}. \] In other words, $R_{\mathsf{equiv}}$ is all of $X \times X$. 

\item[(4)] Let $R$ be the relation on $X$ given by \[ \left\{ (b,b) \right\}. \] Then $R_{\mathsf{equiv}}$ is given by \[ \left\{ (a,a), (b,b), (c,c) \right\}. \] 

\end{itemize}

\end{example}

\begin{table}[h]
\begin{center}
\begin{tabular}{l p{20em}}
\toprule
Equality & Setting \\
\midrule
$X \cap \left( \bigcup_{i \in I} Y_{i} \right) = \bigcup_{i \in I} \left( X \cap Y_{i} \right)$ & A set $X$, and a (possibly infinite) set $\left\{ Y_{i} \right\}_{i \in I}$ of sets. \\
$X \setminus \left( \bigcup_{i \in I} A_{i} \right) = \bigcap_{i \in I} \left( X \setminus A_{i} \right)$ & A set $X$, and a (possibly infinite) set $\left\{ A_{i} \right\}_{i \in I}$ of subsets of $X$. \\ 
$X \setminus \left( \bigcap_{i \in I} A_{i} \right) = \bigcup_{i \in I} \left( X \setminus A_{i} \right)$ & A set $X$, and a (possibly infinite) set $\left\{ A_{i} \right\}_{i \in I}$ of subsets of $X$. \\
$f^{-1}\left( \bigcup_{i \in I} A_{i} \right) = \bigcup_{i \in I} f^{-1}\left(A_{i}\right)$ & A map \ar{X,Y,f} of sets, and a (possibly infinite) set $\left\{ A_{i} \right\}_{i \in I}$ of subsets of $Y$. \\   
$f^{-1}\left( \bigcap_{i \in I} A_{i} \right) = \bigcap_{i \in I} f^{-1}\left(A_{i}\right)$ & A map \ar{X,Y,f} of sets, and a (possibly infinite) set $\left\{ A_{i} \right\}_{i \in I}$ of subsets of $Y$. \\
$f^{-1}(Y \setminus A) = X \setminus f^{-1}(A)$ & A map \ar{X,Y,f} of sets, and a subset $A$ of $Y$. \\
$f\left(f^{-1}(A)\right) = A$ &  A surjective map \ar{X,Y,f} of sets, and a subset $A$ of $Y$. \\
\bottomrule 
\end{tabular}
\caption{Set theoretic equalities}
\label{TableSetTheoreticEqualities}
\end{center} 
\end{table}

\begin{table}[h]
\begin{center}
\begin{tabular}{l p{20em}}
\toprule
Relation & Setting \\
\midrule
$f(A \cap B) \subset f(A) \cap f(B)$ & A map \ar{X,Y,f} of sets, a subset $A$ of $X$, and a subset $B$ of $X$. \\
$f(A) \subset f(B)$ & A map \ar{X,Y,f} of sets, and subsets $A$ and $B$ of $X$ such that $A \subset B$. \\
$f^{-1}(A) \subset f^{-1}(B)$ & A map \ar{X,Y,f} of sets, and subsets $A$ and $B$ of $Y$ such that $A \subset B$. \\
$A \subset f^{-1}\left(f(A)\right)$ & A map \ar{X,Y,f} of sets, and a subset $A$ of $X$. \\
$f\left(f^{-1}(A)\right) \subset A$ & A map \ar{X,Y,f} of sets, and a subset $A$ of $Y$. \\
\bottomrule 
\end{tabular}
\caption{Set theoretic relations}
\label{TableSetTheoreticRelations}
\end{center} 
\end{table}
