\renewcommand{\thechapter}{E\arabic{exercises}}
\setcounter{exercises}{13}
\setcounter{chapter}{112}

\chapter{Exercises for Lecture 13}

\section{Exam questions}

\begin{task} Let $X = \{a,b,c,d\}$ be a set with four elements. Let $\curlyo_{X}$ be the topology on $X$ given by \[ \left\{ \emptyset, \{ a \}, \{ c \}, \{d\}, \{a,b\}, \{ a,c\}, \{a,d\}, \{c,d\}, \{a,b,c\}, \{a,b,d\},\{a,c,d\}, X \right\}. \] Demonstrate that $(X,\curlyo_{X})$ is not Hausdorff. \end{task}

\begin{task} Prove that the Sorgenfrey line of Task \ref{TaskSorgenfreyLineIsTotallyDisconnected} is Hausdorff. \end{task}

\begin{task} Let $\curlyo$ be the topology on $I^{2}$ given by the set of subsets $U$ of $I^{2}$ such that, for every $x$ which belongs to $U$, we have either that $x=0$, or else that one of the following holds.

\begin{itemize}

\item[(1)] We have that $x$ belongs to $\lhopint{0,y} \times \lhopint{0,y}$ for some $0 < y < \frac{1}{2}$, and this set is a subset of $U$.

\begin{figuretikz} 

\begin{tikzpicture}

\draw[green] (0,0.5) -- (0,0) -- (0.5,0);
\draw[green, dashed] (0,0.5) -- (0.5,0.5) -- (0.5,0); 
\draw (0,0.5) -- (0,2) -- (2,2) -- (2,0) -- (0.5,0);

\end{tikzpicture}

\end{figuretikz}

\item[(2)] We have that $x$ belongs to $\lhopint{0,y} \times \rhopint{1-y,1}$ for some $0 < y < \frac{1}{2}$, and this set is a subset of $U$.

\begin{figuretikz} 

\begin{tikzpicture}

\draw[green] (0,-0.5) -- (0,0) -- (0.5,0);
\draw[green, dashed] (0,-0.5) -- (0.5,-0.5) -- (0.5,0); 
\draw (0.5,0) -- (2,0) -- (2,-2) -- (0,-2) -- (0,-0.5);

\end{tikzpicture}

\end{figuretikz}

\item[(3)] We have that $x$ belongs to $\rhopint{1-y,1} \times \lhopint{0,y}$ for some $0 < y < \frac{1}{2}$, and this set is a subset of $U$.

\begin{figuretikz} 

\begin{tikzpicture}

\draw[green] (1.5,0) -- (2,0) -- (2,0.5);
\draw[green, dashed] (2,0.5) -- (1.5,0.5) -- (1.5,0); 
\draw (2,0.5) -- (2,2) -- (0,2) -- (0,0) -- (1.5,0);

\end{tikzpicture}

\end{figuretikz}

\item[(4)] We have that $x$ belongs to $\rhopint{1-y,1} \times \rhopint{1-y,1}$ for some $0 < y < \frac{1}{2}$, and this set is a subset of $U$.

\begin{figuretikz} 

\begin{tikzpicture}

\draw[green] (2,-0.5) -- (2,0) -- (1.5,0);
\draw[green, dashed] (2,-0.5) -- (1.5,-0.5) -- (1.5,0); 
\draw (2,-0.5) -- (2,-2) -- (0,-2) -- (0,0) -- (1.5,0);

\end{tikzpicture}

\end{figuretikz}



\end{itemize} %
%
Is $(I^{2},\curlyo)$ homeomorphic to $(I^{2},\curlyo_{I^{2}})$? 

\end{task}

\begin{task} \label{TaskTorusIsHausdorff} Prove that $(T^{2},\curlyo_{T^{2}})$ is Hausdorff. 

\begin{figuretikz} 

\begin{tikzpicture}

\draw (0,0) ellipse [x radius=1.5,y radius=0.75];

\begin{scope}

\clip (-1.5,0.15) rectangle (1.5,-0.60);

\draw (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 
 
\end{scope}

\begin{scope}

\clip (0,0.15) ellipse [x radius=0.4,y radius=0.2]; 

\draw (0,0) ellipse [x radius=0.25,y radius=0.125]; 
 
\end{scope}

\end{tikzpicture}

\end{figuretikz} %

\end{task}

\begin{rmk} The intention in Task \ref{TaskTorusIsHausdorff} is for you to give a proof from first principles. In a later lecture, we shall see how to prove that $(T^{2},\curlyo_{T^{2}})$ is Hausdorff by a `canonical method'. 

It is also possible to give a proof by appealing to Corollary \ref{CorollaryHomeomorphismsPreserveHausdorffness} and the fact, discussed in Example \ref{ExampleTorusIsHomeomorphicToAProductOfTwoCircles}, that $(T^{2},\curlyo_{T^{2}})$ is homeomorphic to $(S^{1} \times S^{1}, \curlyo_{S^{1} \times S^{1}})$. Since $(S^{1},\curlyo_{S^{1}})$ is Hausdorff by Example \ref{ExampleCircleIsHausdorff}, we have that $(S^{1} \times S^{1}, \curlyo_{S^{1} \times S^{1}})$ is Hausdorff by Proposition \ref{PropositionProductOfHausdorffTopologicalSpacesIsHausdorff}. \end{rmk}

\section{In the lecture notes}

\begin{task} \label{TaskOpenIntervalsToInfinityAndMinusInfinityBelongToStandardTopologyOnReals} Suppose that $x$ belongs to $\reals$. Prove that $\opint{-\infty,x}$ and $\opint{x,\infty}$ belong to $\streals$. \end{task}

\begin{task} \label{TaskInfiniteStripsDefineATopologyOnR2} Prove that the set $\curlyo$ of Example \ref{ExampleTopologyOnR2GivenByInfiniteStripsIsNotHausdorff} defines a topology on $\reals^{2}$. \end{task}

\begin{task} \label{TaskOpenBijectionsPreserveHausdorffness} Let $X$ and $Y$ be sets, and let \ar{X,Y,f} be a bijection. Thus there is a map \ar{Y,X,g} such that $g \circ f = id_{X}$ and $f \circ g = id_{Y}$.

\begin{itemize}

\item[(1)] Suppose that $y_{0}$ and $y_{1}$ belong to $Y$, and that $y_{0} \not= y_{1}$. Prove that $g(y_{0}) \not= g(y_{1})$. You may wish to appeal to the fact that $f \circ g = id_{Y}$.

\item[(2)] Suppose that $U_{0}$ and $U_{1}$ are subsets of $X$, and that $U_{0} \cap U_{1}$ is empty. Prove that $f(U_{0}) \cap f(U_{1})$ is empty. You may wish to appeal to the fact that $g \circ f = id_{X}$. 

\end{itemize}

\end{task}

\begin{task} \label{TaskTechnicalStepInProofThatRealLineWithTwoOriginsIsNotHausdorff} In the notation of Example \ref{ExampleRealLineWithTwoOriginsIsNotHausdorff}, prove that, for any neighbourhood $U$ of $\pi\left((0,0)\right)$ in $\quotientset{X}$ with respect to $\curlyo_{\quotientset{X}}$, there is an open interval $\opint{a,b}$ with $a < 0 < b$ such that $\opint{a,b} \times \{0\}$ is a subset of $\pi^{-1}(U)$.  

\end{task}

\section{For a deeper understanding}

\begin{exdefn} A topological space $(X,\curlyo_{X})$ is $\text{T1}$ if, for every ordered pair $(x_{0},x_{1})$ such that $x_{0}$ and $x_{1}$ belong to $X$ and $x_{0} \not= x_{1}$, there is a neighbourhood of $x_{0}$ in $X$ with respect to $\curlyo_{X}$ which does not contain $x_{1}$.

\begin{figuretikz}

\begin{tikzpicture}[>=stealth,font=\small]

\begin{scope}[scale=1.25]
\draw plot [smooth cycle] coordinates {(0,0) (1,1/2)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1,-2) (-2,-5/2) (-5/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\begin{scope}[scale=0.6]
\draw[green, dashed, shift={(-1.5cm,-0.4cm)}] plot [smooth cycle] coordinates {(-1/4,0) (1,1/4)  (3/4,-1) (3/2,-2) (1/2,-5/2) (-1/4,-2) (-5/3,-5/2) (-4/3,-5/4) (-2,0) (-5/4,1/2) };
\end{scope}

\fill (-1,-0.75) circle[radius=0.05] node[anchor=north] {$x_{0}$};

\fill (0.325,-1.6) circle[radius=0.05] node[anchor=north] {$x_{1}$};

\node at (1.5,1) {$X$};

\end{tikzpicture}

\end{figuretikz}

\end{exdefn}

\begin{exrmk} \label{RemarkHausdorffIsT1}. Suppose that $(X,\curlyo_{X})$ is a Hausdorff topological space. Then $(X,\curlyo_{X})$ is a T1 topological space. \end{exrmk}

\begin{task} \label{TaskSingletonSetsClosedIfAndOnlyIfT1} Let $(X,\curlyo_{X})$ be a topological space. Suppose that $x$ belongs to $X$. Prove that $\{x\}$ is closed in $X$ with respect to $\curlyo_{X}$ if and only if $(X,\curlyo_{X})$ is a T1 topological space. You may wish to proceed as follows.

\begin{itemize}

\item[(1)] Suppose that $(X,\curlyo_{X})$ is a T1 topological space. Suppose that $y$ belongs to $X$, and that $x \not= y$. Since $(X,\curlyo_{X})$ is a T1 topological space, there is a neighbourhood $U_{y}$ of $y$ in $X$ with respect to $\curlyo_{X}$ such that $x$ does not belong to $U_{y}$. Deduce that $y$ is not a limit point of $\{x\}$ in $X$ with respect to $\curlyo_{X}$.

\item[(2)] By Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure}, deduce from (1) that $\{x\}$ is closed in $X$ with respect to $\curlyo_{X}$..    

\item[(3)] Suppose instead that $\{x\}$ is closed in $X$ with respect to $\curlyo_{X}$ for every $x$ which belongs to $X$. Suppose that $x_{0}$ and $x_{1}$ belong to $X$, and that $x_{0} \not= x_{1}$. Since $\{x_{0}\}$ is closed in $X$ with respect to $\curlyo_{X}$, observe have that $X \setminus \{x_{1}\}$ belongs to $\curlyo_{X}$. 

\item[(4)] Moreover, observe that $x_{0}$ belongs to $X \setminus \{x_{1}\}$. Conclude that $(X,\curlyo_{X})$ is T1. 

\end{itemize}

\end{task}  

\begin{excor} \label{CorollaryHausdorffImpliesSingletonSetsClosed} Let $(X,\curlyo_{X})$ be a Hausdorff topological space. Suppose that $x$ belongs to $X$. Then $\{x\}$ is closed in $X$ with respect to $\curlyo_{X}$. \end{excor}

\begin{proof} Follows immediately from Task \ref{TaskSingletonSetsClosedIfAndOnlyIfT1} and Remark \ref{RemarkHausdorffIsT1}. \end{proof}

\begin{task} \label{TaskT1AndFiniteImpliesDiscrete} Let $(X,\curlyo_{X})$ be a T1 topological space. Suppose that $\curlyo_{X}$ is finite. Prove that $\curlyo_{X}$ is the discrete topology on $X$. You may wish to proceed as follows.

\begin{itemize}

\item[(1)] Suppose that $x$ belongs to $X$. Since $(X,\curlyo_{X})$ is T1, there is, for every $y$ which belongs to $X$ such that $x \not= y$, a neighbourhood $U_{y}$ of $x$ in $X$ with respect to $\curlyo_{X}$ such that $y$ does not belong to $U_{y}$. Observe that \[ \bigcap_{y \in Y \setminus \{x\}} U_{y} \] is $\{x\}$.

\item[(2)] Since $\curlyo_{X}$ is finite, observe that \[ \bigcap_{y \in Y \setminus \{x\}} U_{y} \] belongs to $\curlyo_{X}$.

\item[(3)] Deduce that $\{ x\}$ belongs to $\curlyo_{X}$. Conclude that $\curlyo_{X}$ is the discrete topology on $X$.

\end{itemize}

\end{task} 

\begin{excor} \label{CorollaryHausdorffAndFiniteImpliesDiscrete} Let $(X,\curlyo_{X})$ be a Hausdorff topological space. Suppose that $\curlyo_{X}$ is finite. Then $\curlyo_{X}$ is the discrete topology on $X$. \end{excor} 

\begin{proof} Follows immediately from Task \ref{TaskT1AndFiniteImpliesDiscrete} and Remark \ref{RemarkHausdorffIsT1}. \end{proof}

\begin{task} \label{TaskRealLineWithTwoOriginsIsT1} Let $(\quotientset{X},\curlyo_{\quotientset{X}})$ be the real line with two origins of Example \ref{ExampleRealLineWithTwoOriginsIsNotHausdorff}. Prove that $(\quotientset{X},\curlyo_{\quotientset{X}})$ is T1. You may wish to appeal to the fact that for any open interval $\opint{a,b}$ such that $a < 0 < b$, we have that $\pi\left(\opint{a,b} \times \{0\} \right)$ belongs to $\curlyo_{\quotientset{X}}$, but does not contain $\pi\left((0,1)\right)$. \end{task}

\begin{exrmk} Example \ref{ExampleRealLineWithTwoOriginsIsNotHausdorff} and Task \ref{TaskRealLineWithTwoOriginsIsT1} demonstrate that a T1 topological space is not necessarily Hausdorff. \end{exrmk}

\section{Exploration --- Hausdorffness for metric spaces}

\begin{exdefn} Let $X$ be a set. A metric $d$ on $X$ is {\em separating} if, for any $x_{0}$ and $x_{1}$ which belong to $X$ with the property that $d(x_{0},x_{1}) = 0$, we have that $x_{0} = x_{1}$. \end{exdefn}

\begin{exdefn} A metric space $(X,d)$ is {\em separated} if $d$ is separating. \end{exdefn}

\begin{task} Let $(X,d)$ be a separated, symmetric metric space. Let $\curlyo_{d}$ be the topology on $X$ corresponding to $d$ of Task \ref{TaskTopologyFromMetric}. Prove that $(X,\curlyo_{d})$ is Hausdorff. You may wish to proceed as follows.  

\begin{itemize}

\item[(1)] Suppose that $x_{0}$ and $x_{1}$ belong to $X$, and that $x_{0} \not= x_{1}$. Since $(X,d)$ is separated, deduce that $d(x_{0},x_{1}) > 0$.  

\item[(2)] Let $\epsilon = \frac{d(x_{0},x_{1})}{2}$. Appealing to Task \ref{TaskOpenBallsAreOpenInTopologyAssociatedToAMetric}, observe that $B_{\epsilon}(x_{0})$ is a neighbourhood of $x_{0}$ in $X$ with respect to $\curlyo_{d}$, and that $B_{\epsilon}(x_{1})$ is a neighbourhood of $x_{1}$ in $X$ with respect to $\curlyo_{d}$- 

\item[(3)] Suppose that $y$ belongs to $B_{\epsilon}(x_{0})$. By definition of $d$, we have that 

\begin{align*} d(x_{0},x_{1}) &\leq d(x_{0},y) + d(y,x_{1}) \\ 
                              &< \tfrac{d(x_{0},x_{1})}{2} + d(y,x_{1}).
\end{align*} %
%
Thus we have that \[ d(y,x_{1}) > \tfrac{d(x_{0},x_{1})}{2}. \] Since $(X,d)$ is symmetric, deduce that \[ d(x_{1},y) > \tfrac{d(x_{0},x_{1})}{2}. \] 

\item[(4)] Deduce from (3) that $y$ does not belong to $B_{\epsilon}(x_{1})$, and thus that $B_{\epsilon}(x_{0}) \cap B_{\epsilon}(x_{1})$ is empty.

\item[(5)] Conclude from (2) and (4) that $(X,d)$ is Hausdorff.

\end{itemize} 

\begin{figuretikz} 

\begin{tikzpicture} [>=stealth,font=\small]

\fill (0,0) circle[radius=0.05] node[anchor=north] {$x_{0}$};

\fill (45:3) circle[radius=0.05] node[anchor=south] {$x_{1}$};

\draw[dashed] (0,0) circle[radius=1.5];
\draw[dashed] (45:3) circle[radius=1.5];

\draw[<->, purple, shorten <= 0.15em, shorten >= 0.15em] (0,0) to node[auto] {$\epsilon$} (45:1.5);
\draw[<->, purple, shorten <= 0.15em, shorten >= 0.15em] (45:1.5) to node[auto] {$\epsilon$} (45:3);

\end{tikzpicture}

\end{figuretikz} %

\end{task}

\begin{exdefn} A topological space $(X,\curlyo_{X})$ is {\em perfectly normal} if, for every ordered pair of subsets $A_{0}$ and $A_{1}$ of subsets of $X$ which are closed in $X$ with respect to $\curlyo_{X}$, which have the property that $A_{0} \cap A_{1}$ is empty, and which are both not empty, there is a continuous map \ar{X,I,f} such that $f^{-1}\left( \{0\} \right)=A_{0}$ and $f^{-1}\left( \{1\} \right)=A_{1}$. \end{exdefn}

\begin{task} \label{TaskPerfectlyNormalImpliesHausdorff} Let $(X,\curlyo_{X})$ be a perfectly normal topological space. Prove that $(X,\curlyo_{X})$ is Hausdorff. You may wish to appeal to Corollary \ref{CorollaryHausdorffImpliesSingletonSetsClosed}. \end{task}

\begin{task} \label{TaskMetricSpaceIsPerfectlyNormal} Let $(X,d)$ be a separated, symmetric metric space. Let $\curlyo_{d}$ be the topology on $X$ corresponding to $d$ of Task \ref{TaskTopologyFromMetric}. Prove that $(X,\curlyo_{d})$ is perfectly normal. You may wish to proceed as follows.  

\begin{itemize}

\item[(1)] Since $A_{0} \cap A_{1}$ is empty, deduce, by Task \ref{TaskDistanceFromClosedSetToPointOutsideItIsStrictlyGreaterThanZero}, that $d(x,A_{0}) + d(x,A_{1}) > 0$ for every $x$ which belongs to $X$. 

\item[(2)] Since $(X,d)$ is symmetric, we have by Task \ref{TaskDistanceMapIsContinuousForASymmetricMetricSpace} that the map \ar[5]{X,\reals,{d(-,A_{0})}} given by $x \mapsto d(x,A_{0})$ is continuous, and that the map \ar[5]{X,\reals,{d(-,A_{1})}} given by $x \mapsto d(x,A_{1})$ is continuous. By (1), Task \ref{TaskSumOfTwoContinuousMapsToRealsIsContinuous}, Task \ref{TaskQuotientOfTwoContinuousMapsToRealsIsContinuous}, and Task \ref{TaskRestrictionInTargetOfContinuousMapIsContinuous}, deduce that the map \ar{X,I,f} given by $x \mapsto \frac{d(x,A_{0})}{d(x,A_{0}) + d(x,A_{1})}$ is continuous.

\item[(3)] By Remark \ref{RemarkDistanceFromPointToSetContainingItIsZero} and Task \ref{TaskDistanceFromClosedSetToPointOutsideItIsStrictlyGreaterThanZero}, observe that $f^{-1}\left( \{0\} \right) = A_{0}$, and that $f^{-1}\left( \{1\} \right) = A_{1}$. 

\item[(4)] Conclude from (2) and (3) that $(X,d)$ is perfectly normal.  

\end{itemize}

\end{task}

\begin{rmk} Task \ref{TaskMetricSpaceIsPerfectlyNormal} and Task \ref{TaskPerfectlyNormalImpliesHausdorff} give a second proof that the topological space arising from every metric space is Hausdorff. \end{rmk}

\begin{comment}

\section{Exploration --- Homotopy and the fundamental group}

\begin{exrmk} Let $(X,\curlyo_{X})$ be a topological space. Let $(I,\curlyo_{I}))$ be the unit interval. In this section, we shall always regard $X \times I$ as equipped with the product topology $\curlyo_{X \times I}$. \end{exrmk}

\begin{exnotn} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let \ar{X \times I,Y,h} be a continuous map. We shall denote by \ar{X,Y,h_{0}} the map given by $x \mapsto h(x,0)$. We shall denote by \ar{X,Y,h_{1}} the map given by $x \mapsto h(x,1)$. \end{exnotn}

\begin{task} Prove that $h_{0}$ and $h_{1}$ are continuous. You may wish to appeal to Task \ref{TaskIdentityMapIsContinuous}, Task \ref{TaskConstantMapsAreContinuous}, Task \ref{TaskProductOfContinuousMapsIsContinuous}, and Proposition \ref{PropositionCompositionOfContinuousMapsIsContinuous}. \end{task}

\begin{exdefn} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let \pair{X,Y,f_{0},f_{1}} be continuous maps. A {\em homotopy from $f_{0}$ to $f_{1}$} is a continuous map \ar{X \times I,Y,h} such that $h_{0} = f_{0}$, and such that $h_{1} = f_{1}$. \end{exdefn}

\begin{terminology} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let \pair{X,Y,f_{0},f_{1}} be continuous maps. Then $f_{0}$ is {\em homotopic} to $f_{1}$ if there exists a homotopy from $f_{0}$ to $f_{1}$. \end{terminology}

TODO: Convex.

\begin{task} Let $(X,\curlyo_{X})$ be a topological space. Let \pair{X,\reals^{n},f_{0},f_{1}} be continuous maps. Prove that   \end{task}

\end{comment}
