\setcounter{chapter}{13}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Tuesday 18th February}

\section{Characterisation of Hausdorff topological spaces}

\begin{notn} Let $X$ be a set. We denote by $\Delta(X)$ the subset \[ \{ (x,x) \in X \times X \mid x \in X \} \] of $X \times X$. \end{notn}

\begin{prpn} \label{PropositionHausdorffIffDiagonalClosed} Let $(X,\curlyo_{X})$ be a topological space. Then $(X,\curlyo_{X})$ is Hausdorff if and only if $\Delta(X)$ is closed in $(X \times X, \curlyo_{X \times X})$.
\end{prpn}

%TODO: Change proof below to if and only ifs everywhere.

\begin{proof} Suppose that $(X,\curlyo_{X})$ is Hausdorff. Let $(x,x') \in (X \times X) \setminus \Delta(X)$. By definition of $\Delta(X)$, we have that $x \not= x'$. Since $(X,\curlyo)$ is Hausdorff there is a neighbourhood $U$ of $x$ in $(X,\curlyo)$ and a neighbourhood $U'$ of $x'$ in $(X,\curlyo)$ such that $U \cap U' = \emptyset$. 

We have that $\Delta(X) \cap (U \times U') = \Delta(U \cap U') = \emptyset$. Hence $(x,x')$ is not a limit point of $\Delta(X)$. We deduce that $\Delta(X) = \overline{\Delta(X)}$. By Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure} we conclude that $\Delta(X)$ is closed in $(X \times X, \curlyo_{X \times X})$.

Suppose instead that $\Delta(X)$ is closed in $(X \times X,\curlyo_{X \times X})$. For any $x,x' \in X$ with $x \not= x'$ we have by Proposition \ref{PropositionClosedIfAndOnlyIfOwnClosure} that $(x,x')$ is not a limit point of $\Delta(X)$. Hence there is a neighbourhood $U$ of $x$ in $(X,\curlyo_{X})$ and a neighbourhood $U'$ of $x'$ in $(X,\curlyo_{X})$ such that $\Delta(X) \cap (U \times U') = \emptyset$. 

Appealing again to the fact that $\Delta(X) \cap (U \times U') = \Delta(U \cap U')$ we deduce that $\Delta(U \cap U') = \emptyset$. Hence $U \cap U' = \emptyset$. We conclude that $(X,\curlyo)$ is Hausdorff.

\end{proof}

 \begin{notn} Let $X$ be a set and let $\sim$ be a relation on $X$. Let \[ R_{\sim} = \{ (x,x') \in X \times X \mid x \sim x' \}. \] \end{notn}

\begin{prpn} Let $(X,\curlyo_{X})$ be a Hausdorff topological space. Let $\sim$ be an equivalence relation on $X$. If $(X/\sim, \curlyo_{X/\sim})$ is a Hausdorff topological space then $R_{\sim}$ is a closed subset of $(X \times X, \curlyo_{X \times X})$. 
\end{prpn}  

\begin{proof} Let \ar{X,{X / \sim},\pi} be the quotient map. 

Let \ar[4]{X \times X, {\big(X / \sim\big) \times \big(X / \sim \big)},\pi \times \pi} be the map given by $(x,x') \mapsto \big( \pi(x),\pi(x') \big)$. By Observation \ref{ObservationQuotientMapIsContinuous} we have that $\pi$ is continuous. By Question 4 (c) on Exercise Sheet 3 we deduce that $\pi \times \pi$ is continuous. 

If $X / \sim$ is Hausdorff then by Proposition \ref{PropositionHausdorffIffDiagonalClosed} we have that $\Delta(X / \sim)$ is closed in \[ \big(X / \sim\big) \times \big(X / \sim\big).\] 

By Question 1 (a) on Exercise Sheet 3 we deduce that $(\pi \times \pi)^{-1}\Big(\Delta(X / \sim) \Big)$ is closed in $X \times X$. Note that $R_{\sim} = (\pi \times \pi)^{-1}\Big( \Delta(X / \sim) \Big)$. We conclude that $R_{\sim}$ is closed in $X \times X$. \end{proof}

\begin{caution} That $R_{\sim}$ be closed in $(X \times X,\curlyo_{X \times X})$ is not sufficient in general to ensure that $(X/\sim,\curlyo_{X/\sim})$ is Hausdorff, as the following example demonstrates (exercises). Sufficient when compact, though. \end{caution}

\section{Compact topological spaces}

\begin{terminology} Let $(X,\curlyo)$ be a topological space. An {\em open covering} of $X$ is a set $\{ U_{j} \}_{j \in J}$ of open subsets of $X$ such that $X = \bigcup_{j \in J} U_{j} $. \end{terminology} 

\begin{defn} A topological space $(X,\curlyo)$ is {\em compact} if for every open covering $\{ U_{j} \}_{j \in J}$ of $X$ there is a finite subset $J'$ of $J$ such that $X = \bigcup_{j' \in J'} U_{j'}$. \end{defn}

\begin{terminology} Let $(X,\curlyo)$ be a topological space, and let $\{ U_{j} \}_{j \in J}$ be an open covering of $X$. Suppose that there is a finite subset $J'$ of $J$ such that $X = \bigcup_{j' \in J'} U_{j'}$. We write that $\{ U_{j'} \}_{j' \in J'}$ is a {\em finite subcovering} of $\{ U_{j} \}_{j \in J}$. \end{terminology} 

\begin{examples} \label{ExamplesCompactNonCompactSpaces}
~
\begin{itemize}

\item[(1)] Let $(X,\curlyo)$ be a topological space. If $\curlyo$ is finite then $(X,\curlyo)$ is compact. For if $\curlyo$ is finite then every set $\{ U_{j} \}_{j \in J}$ of open subsets of $X$ is finite. 

In particular if $X$ is finite then $(X,\curlyo)$ is compact. For if $X$ is finite there are only finitely many subsets of $X$, and thus $\curlyo$ is finite.

\item[(2)] $(\reals,\curlyo_{\reals})$ is not compact. The open covering $\{ (-n,n) \}_{n \in \mathbb{N}}$ of $\reals$ has no finite subcovering for instance.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (7,0);

\draw (3,0) -- (3,-0.15);
\node at (3,-0.5) {$0$};

\draw[green] (2,0.5) node {$($} -- (4,0.5) node {$)$};
\draw[green] (1,1) node {$($} -- (5,1) node {$)$};
\draw[green] (0,1.5) node {$($} -- (6,1.5) node {$)$};

\foreach \x in {1,2,3}
  \draw (3-\x,0) -- (3-\x,-0.15)
        (3+\x,0) -- (3+\x,-0.15);

\foreach \x in {1,2,3}
  \node at (3-\x,-0.5) {$-\x$};

\foreach \x in {1,2,3}
  \node at (3+\x,-0.5) {$\x$};

\end{tikzpicture}

\end{figuretikz}

\item[(3)] $(\reals^{2},\curlyo_{\reals^{2}})$ is not compact. The open covering of $\reals^{2}$ given by \[ \big\{ \reals \times (-n,n) \big\}_{n \in \mathbb{N}} \] has no finite subcovering for instance.

This open covering consists of horizontal strips of increasing height.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (-1,0) -- (7,0)
              (3,-1.5) -- (3,1.5);

\draw[dashed,green] (-1,0.5) rectangle (7,-0.5);

\draw (3,-0.5) -- (2.85,-0.5) node[anchor=north east] {$-1$}
      (3,0.5)  -- (2.85,0.5)  node[anchor=south east] {$1$};

\foreach \x in {1,2,...,16}
  \draw[green] (-1 + \x/2,0.5) -- (-1 + \x/2 - 0.25,-0.5);       

\draw[yellow] (-1,-4) -- (7,-4)
              (3,-5.5) -- (3,-2.5);

\draw[dashed,green] (-1,-3) rectangle (7,-5);

\draw (3,-5) -- (2.85,-5) node[anchor=north east] {$-2$}
      (3,-3) -- (2.85,-3) node[anchor=south east] {$2$};

\foreach \x in {1,2,...,16}
  \draw[green] (-1 + \x/2,-3) -- (-1 + \x/2 - 0.5,-5);    

\end{tikzpicture}

\end{figuretikz}

A different open covering of $\reals^{2}$ which has no finite subcovering is given by \[ \big\{ (-n,n) \times (-n,n) \big\}_{n \in \mathbb{N}}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]
 
\draw[yellow] (0,0) -- (4,0)
              (2,-1.5) -- (2,1.5);

\draw[dashed,green] (1.5,0.5) rectangle (2.5,-0.5);

\foreach \x in {1,2,...,4}
  \draw[green] (1.5 + \x/4,0.5) -- (1.5 + \x/4 - 0.2,-0.5);    


\draw[yellow] (5,0) -- (9,0)
              (7,-1.5) -- (7,1.5);

\draw[dashed,green] (6,1) rectangle (8,-1);

\foreach \x in {1,2,...,8}
  \draw[green] (6 + \x/4,1) -- (6 + \x/4 - 0.2,-1);    

\end{tikzpicture}

\end{figuretikz}

\item[(4)] An open interval $\big((a,b),\curlyo_{(a,b)}\big)$, where $\curlyo_{(a,b)}$ denotes the subspace topology with respect to $(\reals,\curlyo_{\reals})$, is not compact for any $a,b \in \reals$. The open covering of $(a,b)$ given by \[ \big\{ (a+\frac{1}{n},b-\frac{1}{n}) \big\}_{n \in \mathbb{N} \text{ and } \frac{1}{n} < \frac{b-a}{2}} \] has no finite subcovering for instance. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small,decoration={markings,
  mark=between positions 0 and 1 step 4pt
  with { \draw [fill,purple] (0,0) circle [radius=0.4pt];}}]

\draw[yellow] (-1,0) -- (7,0);

\draw[green] (0,0.1) node {$($} -- (6,0.1) node {$)$};

\foreach \x in {2,...,10}
  \draw[purple] (0 + 3/\x, 4.5 - \x * 0.35) node {$($} -- (6 - 3/\x, 4.55 - \x * 0.35) node {$)$};

\path[postaction={decorate}] (3,0.75) -- (3,0.25);

\foreach \x in {3}
  \draw (3-\x,0) -- (3-\x,-0.15)
        (3+\x,0) -- (3+\x,-0.15);

\draw (3,0) -- (3,-0.15);

\node at (3,-0.5) {$\frac{b-a}{2}$};
\node at (0,-0.5) {$a$};
\node at (6,-0.5) {$b$};

\end{tikzpicture}

\end{figuretikz}

\item[(5)] Generalising (2) let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces, one of which is not compact. Then $(X \times Y, \curlyo_{X \times Y})$ is not compact. 

Suppose for example that $(X,\curlyo_{X})$ is not compact. Let $\{ U_{j} \}_{j \in J}$ be an open covering of $X$ which does not admit a finite subcovering. Then $\big\{ U_{j} \times Y \big\}_{j \in J}$ is an open covering of $X \times Y$ which does not admit a finite subcovering. 

For instance let $(S^{1} \times (0,1), \curlyo_{S^{1} \times (0,1)})$ be a cylinder with the two circles at its ends cut out. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[dashed] (0,0) ellipse[x radius=0.5,y radius=0.15]
              (0,2) ellipse[x radius=0.5,y radius=0.15];

\draw (0,1) ellipse[x radius=0.5,y radius=0.15];

\draw (-0.5,0) -- (-0.5,2)
      (0.5,0)  -- (0.5,2);

\end{tikzpicture}

\end{figuretikz}

Since $(0,1)$ is not compact by (4) we have that $(S^{1} \times (0,1), \curlyo_{S^{1} \times (0,1)})$ is not compact.  

The open covering \[ \Big\{ S^{1} \times \big(\frac{1}{n},1-\frac{1}{n}\big) \Big\}_{n \in \mathbb{N} \text{ and } n \geq 2} \] of $S^{1} \times (0,1)$ is pictured below. It does not admit a finite subcovering. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small,decoration={markings,
  mark=between positions 0 and 1 step 4pt
  with { \draw [fill,purple] (0,0) circle [radius=0.4pt];}}]

\draw[dashed] (0,0.5) ellipse[x radius=0.5,y radius=0.15]
              (0,1.5) ellipse[x radius=0.5,y radius=0.15];

\draw (0,1) ellipse[x radius=0.5, y radius=0.15];

\draw (-0.5,0.5) -- (-0.5,1.5)
      (0.5,0.5)  -- (0.5,1.5);


\draw[dashed] (2,0.25) ellipse[x radius=0.5,y radius=0.15]
              (2,1.75) ellipse[x radius=0.5,y radius=0.15];

\draw (2,1) ellipse[x radius=0.5, y radius=0.15];

\draw (1.5,0.25) -- (1.5,1.75)
      (2.5,0.25) -- (2.5,1.75);

\draw[dashed] (4,0.125) ellipse[x radius=0.5,y radius=0.15]
              (4,1.825) ellipse[x radius=0.5,y radius=0.15];

\draw (4,1) ellipse[x radius=0.5, y radius=0.15];

\draw (3.5,0.125) -- (3.5,1.825)
      (4.5,0.125) -- (4.5,1.825);

\path[postaction=decorate] (5.25,1) -- (5.75,1);

\draw[dashed] (7,0) ellipse[x radius=0.5,y radius=0.15]
              (7,2) ellipse[x radius=0.5,y radius=0.15];

\draw (7,1) ellipse[x radius=0.5, y radius=0.15];

\draw (6.5,0) -- (6.5,2)
      (7.5,0) -- (7.5,2);


\end{tikzpicture}

\end{figuretikz}

\item[(6)] Let $D^{2} \setminus S^{1}$ be the disc $D^{2}$ with its boundary circle removed. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[dashed] circle[radius=1];

\clip circle[radius=0.95];

\foreach \x in {1,2,...,4}
  \draw (-1 + \x/5,-1) -- (-1 + \x/5,1)
        (1 - \x/5,-1) -- (1 - \x/5,1)
        (0,-1) -- (0,1);

\end{tikzpicture}

\end{figuretikz}

In other words $D^{2} \setminus S^{1}$ is \[ \big\{ (x,y) \in \reals^{2} \mid \| (x,y) \| < 1 \big\} \] equipped with the subspace topology $\curlyo_{D^{2} \setminus S^{1}}$ with respect to $(\reals^{2},\curlyo_{\reals^{2}})$.

Then $(D^{2} \setminus S^{1},\curlyo_{D^{2} \setminus S^{1}})$ is not compact. The open covering \[ \big\{ (x,y) \in \reals^{2} \mid \| (x,y) \| < 1 - \frac{1}{n} \big\}_{n \in \mathbb{N}} \] of $D^{2} \setminus S^{1}$ does not admit a finite subcovering for instance.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small,decoration={markings,
  mark=between positions 0 and 1 step 4pt
  with { \draw [fill,purple] (0,0) circle [radius=0.4pt];}}]

\draw[dashed] (0,0) circle[radius=1];

\begin{scope}
\draw[green,dashed] (0,0) circle[radius=0.5];
\clip (0,0) circle[radius=0.45];

\foreach \x in {1,2,...,5}
    \draw[green] (-0.5 + \x/6,-1) -- (-0.5 + \x/6,1)
                 (0.5 - \x/6,-1) -- (0.5 - \x/6,1)
                 (0,-1) -- (0,1);
\end{scope} 

\draw[dashed] (3,0) circle[radius=1];

\begin{scope}
\draw[green,dashed] (3,0) circle[radius=0.75];
\clip (3,0) circle[radius=0.7];

\foreach \x in {1,2,...,5}
    \draw[green] (2.25 + \x/6,-1) -- (2.25 + \x/6,1)
                 (3.75 - \x/6,-1) -- (3.75 - \x/6,1);
        
\end{scope}

\draw[dashed] (6,0) circle[radius=1];

\begin{scope}
\draw[green,dashed] (6,0) circle[radius=0.825];
\clip (6,0) circle[radius=0.775];

\foreach \x in {1,2,...,4}
    \draw[green] (5.175 + \x/6,-1) -- (5.175 + \x/6,1)
                 (6.825 - \x/6,-1) -- (6.825 - \x/6,1)
                 (6,-1) -- (6,1);
        
\end{scope}

\path[postaction=decorate] (7.75,0) -- (8.25,0);

\draw[dashed] (10,0) circle[radius=1];

\clip (10,0) circle[radius=0.95];

\foreach \x in {1,2,...,4}
  \draw (9 + \x/5,-1) -- (9 + \x/5,1)
        (11 - \x/5,-1) -- (11 - \x/5,1)
        (10,-1) -- (10,1);

\end{tikzpicture}

\end{figuretikz}
 
\end{itemize}
\end{examples}

