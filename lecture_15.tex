\setcounter{chapter}{14}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Monday 24th February}

\section{Continuous surjections with a compact source}

\begin{prpn} \label{PropositionContinuousImageOfACompactSpaceIsCompact} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let \ar{X,Y,f} be a surjective continuous map. If $(X,\curlyo_{X})$ is compact then $(Y,\curlyo_{Y})$ is compact. \end{prpn}

\begin{proof} Let $\{ U_{j} \}_{j \in J}$ be an open covering of $Y$. Since $f$ is continuous we have that $f^{-1}(U_{j}) \in \curlyo_{X}$ for all $j \in J$. Moreover \begin{align*} \bigcup_{j \in J} f^{-1}(U_{j}) &= f^{-1}(\bigcup_{j \in J} U_{j}) \\ &= f^{-1}(Y) \\ &= X. \end{align*} % 
%
Thus $\big\{ f^{-1}(U_{j}) \big\}_{j \in J}$ is an open covering of $X$. 

Since $(X,\curlyo_{X})$ is compact there is a finite subset $J'$ of $J$ such that $\big\{ f^{-1}(U_{j'}) \big\}_{j' \in J'}$ is an open covering of $X$. We have that \begin{align*} \bigcup_{j' \in J'} U_{j'} &= \bigcup_{j' \in J'} f\big(f^{-1}(U_{j'})\big) \\ &= f \Big( \bigcup_{j' \in J'} f^{-1}(U_{j'}) \Big) \\ &= f(X) \\ &= Y. \end{align*} %
%
Thus $\{ U_{j'} \}_{j' \in J'}$ is an open covering of $Y$. \end{proof}

\begin{cor} \label{CorollaryHomeomorphismPreservesCompactness} Let $(X,\curlyo_{X})$ and $(Y,\curlyo_{Y})$ be topological spaces. Let \ar{X,Y,f} be a homeomorphism. If $(X,\curlyo_{X})$ is compact then $(Y,\curlyo_{Y})$ is compact. \end{cor}     

\begin{proof} Follows immediately from Proposition \ref{PropositionContinuousImageOfACompactSpaceIsCompact}, since by Proposition \ref{PropositionEquivalentCharacterisationOfAHomeomorphism} a homeomorphism is in particular surjective and continuous. \end{proof}

\begin{rmk} Let the open interval $(a,b)$ for $a,b \in \reals$ be equipped with its subspace topology $\curlyo_{(a,b)}$ with respect to $(\reals,\curlyo_{\reals})$. By Examples \ref{ExamplesHomeomorphismsFiniteAndIntervals} (6) we have that $\big( (a,b), \curlyo_{(a,b)} \big)$ is homeomorphic to $(\reals,\curlyo_{\reals})$.  

Once we know by Examples \ref{ExamplesCompactNonCompactSpaces} (2) that $(\reals,\curlyo_{\reals})$ is not compact we could appeal to Corollary \ref{CorollaryHomeomorphismPreservesCompactness} to deduce that $\big( (a,b), \curlyo_{(a,b)} \big)$ is not compact. We observed this directly in Examples \ref{ExamplesCompactNonCompactSpaces} (4). \end{rmk}

\section{\texorpdfstring{$(I,\curlyo_{I})$ is compact}{Unit interval is compact}}

\begin{lem} \label{LemmaRefinementOpenCovering} Let $(X,\curlyo_{X})$ be a topological space. Let $\{ U_{j} \}_{j \in J}$ and $\{ W_{k} \}_{k \in K}$ be open coverings of $X$. Suppose that for every $k \in K$ there is a $j_{k} \in J$ such that $W_{k} \subset U_{j_{k}}$. If $\{ W_{k} \}_{k \in K}$ has a finite subcovering then $\{ U_{j} \}_{j \in J}$ has a finite subcovering.  \end{lem}

\begin{proof} Let $K'$ be a finite subset of $K$ such that $\{ W_{k'} \}_{k' \in K}$ is an open covering of $X$. We have that \begin{align*} X &= \bigcup_{k' \in K'} W_{k'} \\ &\subset \bigcup_{k' \in K'} U_{j_{k'}}. \end{align*} Then $X = \bigcup_{k' \in K'} U_{j_{k'}}$. Thus $\{ U_{j_{k'}} \}_{k' \in K'}$ is a finite subcovering of $\{ U_{j} \}_{j \in J}$. \end{proof}

\begin{lem} Let $\{ U_{t} \}_{t \in I}$ be a set of subsets of $I$ with the property that $U_{t}$ is an interval, for every $t \in I$. Suppose that $s$ belongs to $I$. Suppose that there is a finite subset $J_{s}$ of $I$ such that $\clint{0,s}$ is a subset of $\bigcup_{r \in J_{s}} U_{r}$. Suppose that $x$ belongs to $U_{s}$. Then $\clint{0,x}$ is a subset of $\bigcup_{r \in J_{s} \cup \{s\}} U_{r}$. \end{lem}

\begin{proof} Let $y \in I$ be such that $y \in \clint{0,x}$. If $y \leq s$, then since $\clint{0,s}$ is a subset of $\bigcup_{r \in J_{s}} U_{r}$, there is an $r \in J_{s}$ such that $y$ belongs to $U_{r}$. If $s \leq y$, we have the following situation:

\begin{itemize}

\item[(1)] We have that $s \leq y \leq x$.

\item[(2)] We have that $U_{s}$ is an interval. 

\item[(3)] Both $s$ and $x$ belong to $U_{s}$.

\end{itemize} %
%
By Task \ref{TaskCharacterisationOfIntervals}, we deduce that $y$ belongs to $U_{s}$. Putting the case that $y \leq s$ and the case that $y \leq s$ together, we have that for any $y \in \clint{0,x}$, $y$ belongs to $\bigcup_{r \in J \cup \{s\}} U_{r}$. We conclude that that $\clint{0,x}$ is a subset of $\bigcup_{r \in J \cup \{ t \}} U_{r}$. 

\end{proof}

\begin{lem} Let $\{ U_{t} \}_{t \in I}$ be a set of subsets of $I$ with the property that, for all $t \in I$, there is an open interval $\opint{a_{t},b_{t}}$ such that $U_{t} = I \cap \opint{a_{t},b_{t}}$. Let \ar{I,{\{0,1\}},f} be the map defined by $s \mapsto 0$ if there is a finite subset $J$ of $I$ such that $\clint{0,s}$ is a subset of $\bigcup_{x \in J} U_{x}$, and by $s \mapsto 1$ otherwise. Then $f$ is continuous.  \end{lem}

\begin{proof} Suppose that $s$ belongs to $I$, and that $f(s)=0$. Suppose that $x$ belongs to $U_{s}$. Suppose that $y$ belongs to $\clint{0,x}$. By Lemma \ref{}, we have that $\clint{0,y}$ is a subset of $\bigcup_{r \in J \cup \{s\}} U_{r}$. Since $J$ is finite, $J \cup \{s\}$ is finite. Thus 

TODO: Finish!
 
 \end{proof}

\begin{prpn} \label{PropositionUnitIntervalIsCompact} The unit interval $(I,\curlyo_{I})$ is compact. \end{prpn}

\begin{proof} Let $\{ U_{j} \}_{j \in J}$ be an open covering of $I$ with respect to $\curlyo_{I}$. By definition of $\curlyo_{I}$, for every $j \in J$, there is a subset of $U'_{j}$ of $\reals$ which belongs to $\streals$ such that $U_{j} = I \cap U'_{j}$. Suppose that $t$ belongs to $I$. Since $\{ U_{j} \}_{j \in J}$ is an open covering of $I$, there a $j_{t} \in J$ such that $t$ belongs to $U_{j_{t}}$. By definition of $\streals$,  there is an open interval $\opint{a_{t},b_{t}}$ such that $a_{t} < t < b_ {t}$, and such that $\opint{a_{t},b_{t}}$ is a subset of $U'_{j_{t}}$. Let $U_{t}$ be $I \cap \opint{a_{t},b_{t}}$. Since $t$ belongs to $U_{t}$ for every $t \in I$, we have that $\bigcup_{t \in I} U_{t}$ is $I$. By Example \ref{ExampleOpenIntervalsAreOpen}, we have that $\opint{a_{t},b_{t}}$ belongs to $\streals$ for every $t \in I$. Thus $U_{t}$ belongs to $\curlyo_{I}$. We conclude that $\{ U_{t} \}_{t \in I}$ is an open covering of $I$ with respect to $\curlyo_{I}$. By Lemma \ref{LemmaRefinementOpenCovering}, to prove that $\{ U_{j} \}_{j \in J}$ admits a finite subcovering, it suffices to prove that $\{ U_{t} \}_{t \in I}$ admits a finite subcovering.

Define \ar{I,{\{0,1\}},f} to be the map given by $s \mapsto 0$ if there is a finite subset $J$ of $I$ such that $\clint{0,s}$ is a subset of $\bigcup_{t' \in J} U_{t'}$ and by $s \mapsto 1$ otherwise. Let $\left\{ 0, 1 \right\}$ be equipped with its discrete topology. We shall prove that $f$ is continuous.  Suppose that $t$ belongs to $I$. Suppose that $s$ belongs to $U_{t}$, and that $f(s)=0$. By definition of $f$, there is a finite subset $J$ of $I$ such that $\clint{0,s}$ is a subset of $\bigcup_{t' \in J} U_{t'}$.  

Since $J$ is finite we have that $J \cup \{t \}$ is finite. Hence $f(s')=0$.   

We have now proven that for any $t \in I$, if $f(s) = 0$ for some $s \in A_{t}$ then $f(A_{t}) = \left\{0\right\}$. We draw the following conclusions.

\begin{itemize}

\item[(1)] We have that \[ f^{-1}\big( \left\{ 0 \right\} \big) = \bigcup_{\text{$t \in I$ such that $f(s) = 0$ for some $s \in A_{t}$}} A_{t}. \] We deduce that $f^{-1}\big(\left\{0\right\}\big) \in \curlyo_{I}$ since $A_{t} \in \curlyo_{I}$ for all $t \in I$. 

\item[(2)] We have that \[ f^{-1}\big( \left\{ 1 \right\} \big) = \bigcup_{\text{$t \in I$ such that $f(s) = 1$ for all $s \in A_{t}$}} A_{t}. \] We deduce that $f^{-1}\big( \left\{ 1 \right\} \big) \in \curlyo_{I}$ since $A_{t} \in \curlyo_{I}$ for all $t \in I$. 

\end{itemize} %
%
This completes our proof that $f$ is continuous. 

By Proposition \ref{PropositionConnectedSubsetOfRealsIffAnInterval} we have that  $(I,\curlyo_{I})$ is connected. Since $f$ is continuous we deduce by Proposition \ref{PropositionConnectedIffNoContinuousSurjectionToDiscreteTwoPointSpace} that $f$ is constant. Moreover $f(0)=0$ since $[0,0] = \left\{ 0 \right\} \subset A_{0}$.

We deduce that $f(s) = 0$ for all $s \in I$. In particular we have that $f(1) = 0$. Thus by definition of $f$ there is a finite subset $J$ of $I$ such that $I = \bigcup_{t' \in J} A_{t'}.$ We conclude that $\{ A_{t'} \}_{t' \in J}$ is a finite subcovering of $\{ A_{t} \}_{t \in I}$. 

\end{proof} 

\begin{scholium} The proof of Proposition \ref{PropositionUnitIntervalIsCompact} is perhaps the most difficult in the course. The idea is to carry out a kind of inductive argument. We begin by noting that we need only the singleton set $\{ A_{0} \}$ to ensure that $0 \in \bigcup_{t \in I} \{ A_{t} \}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (4,0);

\draw (0,0) -- (0,-0.1) node[anchor=north] {$0$};
\draw (4,0) -- (4,-0.1) node[anchor=north] {$1$};

\fill[red] (0,0) circle[radius=0.05];

\draw[green] (0,0.4) node {$[$} to node[auto] {$A_{0}$} (1,0.4) node {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
Next we suppose that we know that for some $t \in I$ and some $s \in A_{t}$ we need only a finite number of the sets $A_{t'}$ to ensure that $[0,s] \subset \bigcup_{t' \in I} A_{t'}$. In the picture below we suppose that we need only three sets $A_{0}=A_{t_{0}}$, $A_{t_{1}}$, and $A_{t_{2}}$. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (8,0);

\draw (0,0) -- (0,-0.1)
      (5,0) -- (5,-0.1) 
      (8,0) -- (8,-0.1);

\node[anchor=mid] at (0,-0.35) {$0$};
\node[anchor=mid] at (5,-0.35) {$s$};
\node[anchor=mid] at (8,-0.35) {$1$};

\draw[red] (0,0.3) node {$[$} -- (5,0.3) node {$]$};

\draw[green] (0,0.8) node {$[$} to node[auto] {$A_{t_{0}}$} (1,0.8) node {$)$} 
             (0.5,1.7) node {$($} to node[auto] {$A_{t_{1}}$} (3,1.7) node {$)$}
             (2,2.6) node {$($} to node[auto] {$A_{t_{2}}$} (5.5,2.6) node {$)$};

\draw[blue] (4.5,3.5) node {$($} to node[auto] {$A_{t}$} (6.5,3.5) node {$)$};

\end{tikzpicture}

\end{figuretikz} %
%
We then observe that for any $s' \in A_{t}$ we require only the finite number of sets $A_{t'}$ which we needed to cover $[0,s]$ together with the set $A_{t}$ to ensure that $[0,s'] \subset \bigcup_{t' \in I} A_{t'}$.  

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (8,0);

\draw (0,0) -- (0,-0.1)
      (5,0) -- (5,-0.1) 
      (6,0) -- (6,-0.1)
      (8,0) -- (8,-0.1);

\node[anchor=mid] at (0,-0.35) {$0$};
\node[anchor=mid] at (5,-0.35) {$s$};
\node[anchor=mid] at (6,-0.35) {$s'$};
\node[anchor=mid] at (8,-0.35) {$1$};

\draw[red] (0,0.3) node {$[$} -- (6,0.3) node {$]$};

\draw[green] (0,0.8) node {$[$} to node[auto] {$A_{t_{0}}$} (1,0.8) node {$)$} 
             (0.5,1.7) node {$($} to node[auto] {$A_{t_{1}}$} (3,1.7) node {$)$}
             (2,2.6) node {$($} to node[auto] {$A_{t_{2}}$} (5.5,2.6) node {$)$}
             (4.5,3.5) node {$($} to node[auto] {$A_{t}$} (6.5,3.5) node {$)$};

\end{tikzpicture}

\end{figuretikz} 

The tricky part is to show that by `creeping along' in this manner we can arrive at $1$ after a finite number of steps. Ultimately this is a consequence of the completeness of $\reals$ --- indeed it is possible to give a proof in which one appeals to the completeness of $\reals$ directly. 

We instead gave a proof which builds upon the hard work we already carried out to prove that $(I,\curlyo_{I})$ is connected. The role of the completeness of $\reals$ lay in the proof of Lemma \ref{LemmaCharacterisationOfAnInterval}. \end{scholium}

\begin{rmk} To help us to appreciate why $(I,\curlyo_{I})$ is compact, let us compare it to $(0,1]$. We equip $(0,1]$ with its subspace topology $\curlyo_{(0,1]}$ with respect to $(\reals,\curlyo_{\reals})$. The open covering \[ \Big\{ \big( \frac{1}{n},1 \big] \Big\}_{n \in \mathbb{N}} \] of $(0,1]$ has no finite subcovering.

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small,decoration={markings,
  mark=between positions 0 and 1 step 4pt
  with { \draw [fill,green] (0,0) circle [radius=0.4pt];}}]

\draw[yellow] (0,0) -- (6,0);

\foreach \x in {2,...,10}
  \draw[green] (0 + 6/\x, 6 - \x * 0.5) node {$($} -- (6, 6 - \x * 0.5) node {$]$};

\path[postaction={decorate}] (3,0.75) -- (3,0.25);

\foreach \x in {3}
  \draw (3-\x,0) -- (3-\x,-0.15)
        (3+\x,0) -- (3+\x,-0.15);

\node at (0,-0.5) {$0$};
\node at (6,-0.5) {$1$};

\end{tikzpicture}

\end{figuretikz} %
%
Thus $(0,1]$ is not compact. 

To obtain an open covering of $[0,1]$ we have to add an open set containing $0$. Let us take this open set to be $[0,t)$ for some $0 < t < 1$. Thus our open covering of $[0,1]$ is \[ \Big\{ \big( \frac{1}{n},1 \big] \Big\}_{n \in \mathbb{N}} \cup \left\{ [0,t) \right\} \] %
%
where $\left\{ [0,t) \right\}$ is the singleton set containing $[0,t)$. 

This open covering has a finite subcovering! For instance \[ \Big\{ \big( \frac{1}{n},1 \big] \Big\}_{\text{$n \in \mathbb{N}$ such that $n \leq m$}} \cup \left\{ [0,t) \right\} \] where $m \in \mathbb{N}$ is such that $\frac{1}{m} < t$. 
 
\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (6,0);

\draw[green]  (3,2.5) node {$($} to node[auto] {$(\frac{1}{2},1]$} (6,2.5) node {$]$};
\draw[green]  (2,1.5) node {$($} to node[auto] {$(\frac{1}{3},1]$} (6,1.5) node {$]$};
\draw[purple] (0,0.5) node {$[$} to node[auto] {$[0,t)$} (5/12 * 6,0.5) node {$)$};

\path[postaction={decorate}] (3,0.75) -- (3,0.25);

\foreach \x in {3}
  \draw (3-\x,0) -- (3-\x,-0.15)
        (3+\x,0) -- (3+\x,-0.15);

  \draw (1/3 * 6,0) -- (1/3 * 6,-0.15)
        (5/12 * 6,0) -- (5/12 * 6,-0.15);

\node at (0,-0.5) {$0$};
\node at (1/3 * 6,-0.5) {$\frac{1}{3}$};
\node at (5/12 * 6,-0.5) {$t$};
\node at (6,-0.5) {$1$};

\end{tikzpicture}

\end{figuretikz} 

\end{rmk}

\begin{cor} \label{CorollaryCompactnessClosedIntervals} Let the closed interval $[a,b]$ be equipped with its subspace topology $\curlyo_{[a,b]}$ with respect to $(\reals,\curlyo_{\reals})$. Then $\big( [a,b], \curlyo_{[a,b]} \big)$ is compact. \end{cor}

\begin{proof} Follows immediately by Corollary \ref{CorollaryHomeomorphismPreservesCompactness} from Proposition \ref{PropositionUnitIntervalIsCompact}, since by Examples \ref{ExamplesHomeomorphismsFiniteAndIntervals} (4) we have that $\big( [a,b], \curlyo_{[a,b]} \big)$ is homeomorphic to $(I,\curlyo_{I})$. \end{proof}

\begin{rmk} Corollary \ref{CorollaryCompactnessClosedIntervals} is one of the cornerstones of mathematics. Analysis relies indispensably upon it, and it is at the heart of many constructions in topology. \end{rmk} 

\section{Compact vs Hausdorff vs closed}

\begin{prpn} \label{PropositionClosedSubsetOfACompactSpaceIsCompact} Let $(X,\curlyo_{X})$ be a compact topological space, and let $A$ be a closed subset of $X$. Then $(A,\curlyo_{A})$ is compact. Here $\curlyo_{A}$ is the subspace topology on $A$ with respect to $(X,\curlyo_{X})$.  \end{prpn} 

\begin{proof} Let $\{ U_{j} \}_{j \in J}$ be an open covering of $(A,\curlyo_{A})$. By definition of $\curlyo_{A}$ we have that $U_{j} = A \cap U'_{j}$ for some $U'_{j} \in \curlyo_{X}$. Suppose first that $\{ U'_{j} \}_{j \in J}$ is an open covering of $(X,\curlyo_{X})$. An example is pictured below. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (6,0);

\draw[red] (2,0.3) node {$[$} to node[auto] {$A$} (4,0.3) node {$]$}; 

\draw[green] (0,0.85) node {$[$} to node[auto] {$U'_{0}$} (3.5,0.85) node {$)$};
\draw[green] (2.5,1.4) node {$($} to node[auto] {$U'_{1}$} (6,1.4) node {$]$};

\foreach \x in {3}
  \draw (3-\x,0) -- (3-\x,-0.15)
        (3+\x,0) -- (3+\x,-0.15);

\draw (2,0) -- (2,-0.15)
      (4,0) -- (4,-0.15);

\node at (0,-0.5) {$0$};
\node at (2,-0.5) {$\frac{1}{4}$};
\node at (4,-0.5) {$\frac{3}{4}$};
\node at (6,-0.5) {$1$};

\end{tikzpicture}

\end{figuretikz} %
%
Since $(X,\curlyo_{X})$ is compact there is a finite subset $J'$ of $J$ such that $\{ U'_{j'} \}_{j' \in J'}$ is an open covering of $(X,\curlyo_{X})$. Then: \begin{align*} A &= A \cap X \\ &= A \cap \big( \bigcup_{j' \in J'} U'_{j'}\big) \\ &= \bigcup_{j' \in J'} A \cap U'_{j'} \\ &= \bigcup_{j' \in J'} U_{j'}. \end{align*} %
%
Thus $\{ U_{j'} \}_{j' \in J'}$ is a finite subcovering of $\{ U_{j} \}_{j \in J}$.

Suppose now that $X \setminus \bigcup_{j \in J} U'_{j} \not= \emptyset$. Since $A$ is closed in $(X,\curlyo_{X})$ we have that $X \setminus A$ is open in $(X,\curlyo_{X})$. Hence $\{ U'_{j} \}_{j \in J} \cup \{ X \setminus A \}$ is an open covering of $(X,\curlyo_{X})$. Here $\{ X \setminus A \}$ denotes the set with the single element $X \setminus A$. An example is pictured below. 

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw[yellow] (0,0) -- (6,0);

\draw[red] (2,0.3) node {$[$} to node[auto] {$A$} (4,0.3) node {$]$}; 

\draw[green] (1,1) node {$($} to node[auto] {$U'_{0}$} (3.5,1) node {$)$};
\draw[green] (2.5,1.9) node {$($} to node[auto] {$U'_{1}$} (5,1.9) node {$)$};

\draw[blue] (0,2.8) node {$[$} -- (2,2.8) node {$)$};
\draw[blue] (4,2.8) node {$($} -- (6,2.8) node {$]$};

\node[blue] (label) at (3,4.05) {$X \setminus A$};

\draw[->,blue] (label) to[looseness=1,out=-90,in=90] (1,3.05);
\draw[->,blue] (label) to[looseness=1,out=-90,in=90] (5,3.05);

\foreach \x in {3}
  \draw (3-\x,0) -- (3-\x,-0.15)
        (3+\x,0) -- (3+\x,-0.15);

\draw (2,0) -- (2,-0.15)
      (4,0) -- (4,-0.15);

\node at (0,-0.5) {$0$};
\node at (2,-0.5) {$\frac{1}{4}$};
\node at (4,-0.5) {$\frac{3}{4}$};
\node at (6,-0.5) {$1$};

\end{tikzpicture}

\end{figuretikz} % 
%
Since $(X,\curlyo_{X})$ is compact this open covering admits a finite subcovering. Moreover $X \setminus A$ must belong to this finite subcovering by our assumption that $X \setminus \bigcup_{j \in J} U'_{j} \not= \emptyset$. Thus there is a finite subset $J'$ of $J$ such that \[ \{ U'_{j'} \}_{j' \in J'} \cup \{ X \setminus A \} \] is an open covering of $X$. We now observe that: 

\begin{align*} A &= A \cap X \\ &= A \cap \bigg( \Big( \bigcup_{j' \in J'} U'_{j'} \Big) \cup \{ X \setminus A \} \bigg) \\ &= \Big( A \cap \bigcup_{j' \in J'} U'_{j'} \Big) \cup \Big\{ A \cap (X \setminus A)\Big\} \\ &= \Big( \bigcup_{j' \in J'} A \cap U'_{j'} \Big) \cup \emptyset \\ &= \bigcup_{j' \in J'} U_{j'}. \end{align*} Thus $\{ U_{j'} \}_{j' \in J'}$ is a finite subcovering of $\{ U_{j} \}_{j \in J}$. \end{proof} 

\begin{terminology} Let $(X,\curlyo_{X})$ be a topological space. Let $A$ be a subset of $X$ equipped with its subspace topology $\curlyo_{A}$ with respect to $(X,\curlyo_{X})$. Then $A$ is a {\em compact subset} of $X$ if $(A,\curlyo_{A})$ is compact. \end{terminology}

\begin{lem} \label{LemmaSeparationOfAPointAndACompactSubset} Let $(X,\curlyo_{X})$ be a Hausdorff topological space. Let $A$ be a compact subset of $X$. Suppose that $x \in X \setminus A$. There is a pair of open subsets $U$ and $U'$ of $X$ such that:

\begin{itemize}

\item[(1)] $A \subset U$,

\item[(2)] $U'$ is a neighbourhood of $x$,

\item[(3)] $U \cap U' = \emptyset$.
\end{itemize}
\end{lem} 

\begin{proof} Let $a \in A$. Since $(X,\curlyo_{X})$ is Hausdorff there is a neighbourhood $U_{a}$ of $a$ in $(X,\curlyo_{X})$ and a neighbourhood $U'_{a}$ of $x$ in $(X,\curlyo_{X})$ such that $U_{a} \cap U_{a}' = \emptyset$. 

Let $A$ be equipped with its subspace topology $\curlyo_{A}$ with respect to $(X,\curlyo_{X})$. Then $\{ A \cap U_{a} \}_{a \in A}$ defines an open covering of $(A,\curlyo_{A})$, since \begin{align*} A &= \bigcup_{a \in A} \{ a \} \\  &\subset \bigcup_{a \in A} A \cap U_{a} \end{align*} and thus $A = \bigcup_{a \in A} A \cap U_{a}$. 

Since $(A,\curlyo_{A})$ is compact there is a finite subset $J$ of $A$ such that $\{ A \cap U_{a} \}_{a \in J}$ is an open covering of $A$. Let $U = \bigcup_{a \in J} U_{a}$. Let $U' = \bigcap_{a \in J} U_{a}'$. Then:

\begin{itemize}

\item[(1)] We have that $A \subset U$, since \begin{align*} A &= \bigcup_{a \in A} A \cap U_{a} \\ &= \bigcup_{a \in J} A \cap U_{a} \\ &\subset \bigcup_{a \in J} U_{a} \\ &= U. \end{align*} Moreover we have that $U_{a}$ is open in $(X,\curlyo_{X})$ for all $a \in A$, and in particular for all $a \in J$. Thus we have that $U$ is open in $(X,\curlyo_{X})$.

\item[(2)] Since $x \in U'_{a}$ for all $a \in A$ we have that \[ x \in \bigcap_{a \in A} U'_{a} \subset \bigcap_{a \in J} U'_{a}. \] Moreover we have that $U'_{a}$ is open in $(X,\curlyo_{X})$ for all $a \in A$, and in particular for all $a \in J$. Since $J$ is finite we thus have that $\bigcap_{a \in J} U'_{a}$ is open in $(X,\curlyo_{X})$. 

\item[(3)] Since $U_{a} \cap U_{a'} = \emptyset$ for all $a \in A$ we have that \begin{align*} U \cap U' &=\Big( \bigcup_{a \in J} U_{a} \Big) \cap \Big( \bigcap_{a' \in J} U'_{a'} \Big) \\  &= \bigcup_{a \in J} \Big( U_{a} \cap \bigcap_{a' \in J} U'_{a'} \Big) \\ &\subset \bigcup_{a \in J} (U_{a} \cap U_{a}) \\ &= \bigcup_{a \in J} \emptyset \\ &= \emptyset. \end{align*}

\end{itemize}

\end{proof}

\begin{rmk} \label{RemarkTypicalAppealToCompactness} The proof of Lemma \ref{LemmaSeparationOfAPointAndACompactSubset} is a very typical example of an appeal to compactness in practise. The key step is (2). Our conclusion that $\bigcap_{a \in J} U'_{a}$ is open in $(X,\curlyo_{X})$ relies on the fact that $J$ is finite --- as we know, an arbitrary intersection of open sets in a topological space need not be open.

\end{rmk}


